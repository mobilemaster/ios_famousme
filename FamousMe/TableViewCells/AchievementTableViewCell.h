//
//  AchievementTableViewCell.h
//  FamousMe
//
//  Created by Shine Man on 12/1/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AchievementTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *ivPhoto;

@property (weak, nonatomic) IBOutlet UILabel *lbName;

@property (weak, nonatomic) IBOutlet UILabel *lbActiveTime;

@end
