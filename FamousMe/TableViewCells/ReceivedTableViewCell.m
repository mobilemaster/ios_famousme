//
//  ReceivedTableViewCell.m
//  FamousMe
//
//  Created by Shine Man on 12/15/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "ReceivedTableViewCell.h"

@implementation ReceivedTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(BOOL)canBecomeFirstResponder {
	return YES;
}

- (void)setDate:(NSString*)dateString {
	if (dateString == nil) {
		self.messageTopConstraint.constant = 10.0f;
		self.lbDate.hidden = YES;
		return;
	}
	
	self.lbDate.text = [NSString stringWithFormat:@"  %@  ", dateString ];
	self.lbDate.layer.cornerRadius = self.lbDate.frame.size.height / 2;
	self.lbDate.clipsToBounds = YES;
}
@end
