//
//  ReceivedTableViewCell.h
//  FamousMe
//
//  Created by Shine Man on 12/15/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReceivedTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lbDate;
@property (weak, nonatomic) IBOutlet UIView *textMessageContainer;
@property (weak, nonatomic) IBOutlet UIView *imageMessageContainer;
@property (weak, nonatomic) IBOutlet UIImageView *ivMessageImage;
@property (weak, nonatomic) IBOutlet UILabel *lbMessageText;
@property (weak, nonatomic) IBOutlet UILabel *lbTextMsgTime;
@property (weak, nonatomic) IBOutlet UILabel *lbImageMsgTime;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *messageTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *messageBottomContraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageMsgTopConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageMsgBottomConstraint;

- (void)setDate:(NSString*)dateString;
@end
