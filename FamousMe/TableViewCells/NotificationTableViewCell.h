//
//  NotificationTableViewCell.h
//  FamousMe
//
//  Created by Shine Man on 11/23/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *ivProfilePhoto;
@property (weak, nonatomic) IBOutlet UILabel *lbUserName;
@property (weak, nonatomic) IBOutlet UILabel *lbTime;
@property (weak, nonatomic) IBOutlet UILabel *lbProfession;

@property (weak, nonatomic) IBOutlet UILabel *lbDescription;

@end
