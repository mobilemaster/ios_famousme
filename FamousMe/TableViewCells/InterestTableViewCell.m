//
//  InterestTableViewCell.m
//  FamousMe
//
//  Created by Shine Man on 11/22/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "InterestTableViewCell.h"

@implementation InterestTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
	self.chCheckBox.selected = NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
