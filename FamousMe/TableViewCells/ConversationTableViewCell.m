//
//  DiscoveryTableViewCell.m
//  FamousMe
//
//  Created by Shine Man on 11/23/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "ConversationTableViewCell.h"

@implementation ConversationTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
