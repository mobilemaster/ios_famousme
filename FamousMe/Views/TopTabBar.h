//
//  TopTabBar.h
//  FamousMe
//
//  Created by Shine Man on 11/22/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface TopTabBar : UIView

@property (strong, nonatomic) TopTabBar* contentView;

@property (strong, nonatomic) IBInspectable NSString* countString;
@property (strong, nonatomic) IBInspectable NSString* titleString;
@property (assign, nonatomic) IBInspectable BOOL selected;
@end
