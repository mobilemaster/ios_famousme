//
//  NSLayoutConstraint+NSLayoutConstraint.m
//  FamousMe
//
//  Created by Shine Man on 2/1/18.
//  Copyright © 2018 Shine Man. All rights reserved.
//

#import "NSLayoutConstraint+NSLayoutConstraint.h"

@implementation NSLayoutConstraint (Extensions)
- (CGFloat)iPhone3_5_Constant
{
    return self.constant;
}

- (void)setIPhone3_5_Constant:(CGFloat)iPhone3_5_Constant
{
    if ([UIScreen mainScreen].bounds.size.height < 500) {
        self.constant = iPhone3_5_Constant;
    }
}
@end
