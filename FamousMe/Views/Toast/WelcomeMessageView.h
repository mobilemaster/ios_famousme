//
//  WelcomeMessageView.h
//  FamousMe
//
//  Created by Shine Man on 12/21/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WelcomeMessageView : UIView

@property (weak, nonatomic) IBOutlet UILabel *lbWelcomeTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbNewMessage;
@property (weak, nonatomic) IBOutlet UIView *ivFrame;
@property (weak, nonatomic) IBOutlet UIImageView *ivProfileView;

@end
