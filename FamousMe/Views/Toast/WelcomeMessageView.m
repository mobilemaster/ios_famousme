//
//  WelcomeMessageView.m
//  FamousMe
//
//  Created by Shine Man on 12/21/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "WelcomeMessageView.h"

@implementation WelcomeMessageView

- (void)awakeFromNib {
	[super awakeFromNib];
	
	self.ivFrame.layer.cornerRadius = self.ivFrame.frame.size.height/2;
	[self.ivFrame setClipsToBounds:YES];
	
	self.ivProfileView.layer.cornerRadius = self.ivProfileView.frame.size.height/2;
	[self.ivProfileView setClipsToBounds:YES];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
