//
//  NudgeToastView.h
//  FamousMe
//
//  Created by Shine Man on 12/25/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NudgeToastView : UIView
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbNewMessage;
@property (weak, nonatomic) IBOutlet UIView *ivFrame;
@property (weak, nonatomic) IBOutlet UIImageView *ivProfileView;

@end
