//
//  TopTabBar.m
//  FamousMe
//
//  Created by Shine Man on 11/22/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "TopTabBar.h"

@interface TopTabBar()
@property (strong, nonatomic)  IBOutlet UILabel *lbCount;
@property (strong, nonatomic)  IBOutlet UILabel *lbTitle;
@property (strong, nonatomic)  IBOutlet UIView *vIndicator;
@property (assign, nonatomic) BOOL isCreatedIB;
@end

@implementation TopTabBar
@synthesize contentView;

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
	self = [super initWithCoder:aDecoder];
	
	if (self) {
		//We are in the storyboard code path. Initialize from the xib.
		[self initializeSubviews];
	}
	
	return self;
}

- (id)initWithFrame:(CGRect)frame {
	self = [super initWithFrame:frame];
	if (self) {
		[self initializeSubviews];
	}
	return self;
}

- (void)awakeFromNib {
	[super awakeFromNib];
}

- (void)initializeSubviews {
	if (self.subviews.count == 0) {
		contentView =  [[[NSBundle bundleForClass:self.class] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] firstObject];

		[self addSubview:contentView];
		contentView.frame = self.bounds;
	}
}

- (void)setCountString:(NSString *)countString {
	contentView.lbCount.text = countString;
}

- (void)setTitleString:(NSString *)titleString {
	contentView.lbTitle.text = titleString;
}

- (void)setSelected:(BOOL)selected {
	if (selected) {
		contentView.vIndicator.hidden = NO;
        [ThemeManager setTheme:contentView.vIndicator];
        //contentView._vIndicator.backgroundColor = [ThemeManager getCurrentThemeColor];
		contentView.lbTitle.textColor = [ThemeManager getCurrentThemeColor];
		contentView.lbCount.textColor = [ThemeManager getCurrentThemeColor];
	} else {
		contentView.vIndicator.hidden = YES;		
		contentView.lbTitle.textColor = NORMAL_TEXT_GREY_COLOR;
		contentView.lbCount.textColor = NORMAL_TEXT_GREY_COLOR;
	}
}

@end
