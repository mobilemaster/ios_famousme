//
//  SelectCountryView.h
//  FamousMe
//
//  Created by Shine Man on 11/28/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import <UIKit/UIKit.h>
@import MRCountryPicker;

@interface SelectCountryView : UIView
@property (weak, nonatomic) IBOutlet MRCountryPicker *countryPicker;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;

@end
