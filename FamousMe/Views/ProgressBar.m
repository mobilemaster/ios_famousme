//
//  ProgressBar.m
//  FamousMe
//
//  Created by Shine Man on 12/1/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "ProgressBar.h"

@implementation ProgressBar

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
	self = [super initWithCoder:aDecoder];
	
	if (self) {
		//We are in the storyboard code path. Initialize from the xib.
		[self setUp];
	}
	
	return self;
}

- (id)initWithFrame:(CGRect)frame {
	self = [super initWithFrame:frame];
	if (self) {
		[self setUp];
	}
	return self;
}

- (void)setUp {
	self.backgroundColor = PROGRESS_BACKGROUND_COLOR;
	progress = 0.0f;
}

- (void)drawRect:(CGRect)rect {
    // Drawing code
	[self setProgress:progress];
}

- (void)setProgress:(CGFloat)value {
	progress = value;
	CGFloat progressValue = progress > 1.0 ? progress / 100 : progress;
	
	self.layer.cornerRadius = CGRectGetHeight(self.frame) / 2.0;
	self.clipsToBounds = YES;

	CGFloat margin = 0.0;
	CGFloat width = (CGRectGetWidth(self.frame) - margin)  * progressValue;
	CGFloat height = CGRectGetHeight(self.frame) - margin;
	
	if (width < height)
		width = height;
	
	CGRect progressRect = CGRectMake(margin/2.0, margin/2.0, width, height);
	UIBezierPath* pathRef = [UIBezierPath bezierPathWithRoundedRect:progressRect cornerRadius:height/2.0];
	
	[GENERAL_YELLOW_COLOR setFill];
	[pathRef fill];

	[[UIColor clearColor] setStroke];
	[pathRef stroke];
	[pathRef closePath];
	
	//Drawing String
	NSString* progressString = [NSString stringWithFormat:@"%.1f%%", progress];
	UIFont* font = [UIFont fontWithName:@"Karla-Bold" size:14];
	CGFloat fontHeight = font.pointSize;
	CGFloat yOffset = (progressRect.size.height - fontHeight) / 2.0;
	CGRect textRect = CGRectMake(0, yOffset, progressRect.size.width, fontHeight);
	
	/// Make a copy of the default paragraph style
	NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
	paragraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;
	paragraphStyle.alignment = NSTextAlignmentCenter;
	
	NSDictionary *attributes = @{ NSFontAttributeName: font,
								  NSForegroundColorAttributeName: [UIColor whiteColor],
								  NSParagraphStyleAttributeName: paragraphStyle };
	
	[progressString drawInRect:textRect withAttributes:attributes];
	
	[self setNeedsDisplay];
}

@end
