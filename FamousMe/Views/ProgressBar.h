//
//  ProgressBar.h
//  FamousMe
//
//  Created by Shine Man on 12/1/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProgressBar : UIView {
	CGFloat progress;
}

- (void)setProgress:(CGFloat)value;

@end
