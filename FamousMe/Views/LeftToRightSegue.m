//
//  LeftToRightSegue.m
//  FamousMe
//
//  Created by Shine Man on 11/21/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "LeftToRightSegue.h"

@implementation LeftToRightSegue

- (void)perform {
	UIViewController* source = (UIViewController *)self.sourceViewController;	

	CATransition *transition = [CATransition animation];
	transition.duration = 0.3;
	transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
	transition.type = kCATransitionPush;
	transition.subtype = kCATransitionFromLeft;
	[source.view.window.layer addAnimation:transition forKey:nil];
	[source dismissViewControllerAnimated:NO completion:nil];
}

@end
