//
//  PaddingTextField.h
//  FamousMe
//
//  Created by Shine Man on 11/20/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaddingTextField : UITextField

@property (nonatomic) IBInspectable CGFloat padding;

- (BOOL)isEmpty;
@end
