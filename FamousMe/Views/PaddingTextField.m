//
//  PaddingTextField.m
//  FamousMe
//
//  Created by Shine Man on 11/20/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "PaddingTextField.h"

IB_DESIGNABLE
@implementation PaddingTextField

@synthesize padding;

- (CGRect)textRectForBounds:(CGRect)bounds{
	return CGRectInset(bounds, padding, padding);
}

- (CGRect)editingRectForBounds:(CGRect)bounds{
	return [self textRectForBounds:bounds];
}

- (BOOL)isEmpty {
	if (self.text && self.text.length > 0)
		return NO;
	else
		return YES;
}
@end
