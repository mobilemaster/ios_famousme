//
//  NSLayoutConstraint+NSLayoutConstraint.h
//  FamousMe
//
//  Created by Shine Man on 2/1/18.
//  Copyright © 2018 Shine Man. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSLayoutConstraint (Extensions)
@property (nonatomic) IBInspectable CGFloat iPhone3_5_Constant;

@end
