//
//  SegueFromLeft.m
//  FamousMe
//
//  Created by Shine Man on 11/21/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "RightToLeftSegue.h"

@implementation RightToLeftSegue

- (void)perform {
	UIViewController* source = (UIViewController *)self.sourceViewController;
	UIViewController* destination = (UIViewController *)self.destinationViewController;
	
	CATransition *transition = [[CATransition alloc] init];
	transition.duration = 0.3;
	transition.type = kCATransitionPush;
	transition.subtype = kCATransitionFromRight;
	[transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
	[source.view.window.layer addAnimation:transition forKey:kCATransition];
	[source presentViewController:destination animated:false completion:nil];
	

/*	[source.view.superview insertSubview:destination.view aboveSubview:source.view.superview];
	
	destination.view.transform = CGAffineTransformMakeTranslation(source.view.frame.size.width, source.view.frame.origin.x);
	
	[UIView animateWithDuration:3.25 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^(void) {
		destination.view.transform = CGAffineTransformMakeTranslation(0, 0);
	}  completion:^(BOOL finished) {
		[source presentViewController:destination animated:NO completion:nil];
	}];
	
*/
}

@end
