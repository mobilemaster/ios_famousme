//
//  FirebaseManager.m
//  FamousMe
//
//  Created by Shine Man on 12/12/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "FirebaseManager.h"
#import <FirebaseStorageUI/FirebaseStorageUI.h>
#import "ImageUtils.h"

@interface FirebaseManager () {
	
	//The temp refs which have be remove after using
	FIRDatabaseReference* userStatusRef;    
	FIRDatabaseQuery* getMessageQuery;
	FIRDatabaseQuery* getDiscoveryQuery;
	
	FIRDatabaseReference* notificationRef;
	FIRDatabaseReference* blockUserRef;
	NSMutableArray *childObservers;
	GFCircleQuery *geoCircleQuery;
    GFRegionQuery *geoRegionQuery;
}

@property (nonatomic) FIRDatabaseReference *databaseRef;
@property (nonatomic) FIRStorageReference *storageRef;

@end

@implementation FirebaseManager
#pragma mark - Public Methods

+ (instancetype)sharedManager {
	static FirebaseManager *sharedInstance = nil;
	static dispatch_once_t pred;
	
	dispatch_once(&pred, ^{
		sharedInstance = [[FirebaseManager alloc] init];
	});
	
	return sharedInstance;
}

- (instancetype)init {
	self = [super init];
	if (self) {
		self.databaseRef = [[FIRDatabase database] reference];
		self.storageRef  = [[FIRStorage storage] reference];
		childObservers = [NSMutableArray array];
	}
	
	return self;
}

+ (FIRUser *)currentUser {
	return [[FIRAuth auth] currentUser];
}

+ (NSString *)getCurrentUserID {
	return [FirebaseManager currentUser].uid;
}

+ (NSString*)getPhoneNumber {
	return [[[FIRAuth auth] currentUser] phoneNumber];
}

- (FIRDatabaseReference*)getCurrenUserReference {
	return [[self.databaseRef child:@"users"] child:[FirebaseManager getCurrentUserID]];
}

- (FIRDatabaseReference*)getUserReference {
	return [self.databaseRef child:@"users"];
}

- (FIRDatabaseReference*)getUserReference:(NSString*)key {
	return [[self.databaseRef child:@"users"] child:key];
}

- (FIRDatabaseReference*)getNotificationReference {
	if (notificationRef == nil)
		notificationRef = [[self.databaseRef child:@"notifications"] child:[FirebaseManager getCurrentUserID]];
	
	return notificationRef;
}

- (void)stopNotification {
	if (notificationRef) {
		[notificationRef removeAllObservers];
		notificationRef = nil;
	}
}

- (FIRDatabaseReference*)getNotificationReferenceByUserId:(NSString*)userId {
	return [[self.databaseRef child:@"notifications"] child:userId];
}

- (FIRDatabaseReference*)getDiscoveryReference {
	return [[self.databaseRef child:@"discoveries"] child:[FirebaseManager getCurrentUserID]];
}

- (FIRDatabaseReference*)getDiscoveryReferenceByUserId:(NSString*)userId {
	return [[self.databaseRef child:@"discoveries"] child:userId];
}

- (FIRDatabaseReference*)getConversationReference {
	return [[self.databaseRef child:@"conversations"] child:[FirebaseManager getCurrentUserID]];
}

- (FIRDatabaseReference*)getConversationReferenceByUserId:(NSString*)userId {
	return [[self.databaseRef child:@"conversations"] child:userId];
}

- (FIRDatabaseReference*)getContactReference {
	return [[self.databaseRef child:@"contacts"] child:[FirebaseManager getCurrentUserID]];
}

- (FIRDatabaseReference*)getAchievementReference {
	return [[self.databaseRef child:@"achievements"] child:[FirebaseManager getCurrentUserID]];
}

- (FIRDatabaseReference*)getAchievementReferenceByUserId:(NSString*)userId {
	return [[self.databaseRef child:@"achievements"] child:userId];
}

- (FIRDatabaseReference*)getMessageReference:(NSString*)conversationId {
	return [[self.databaseRef child:@"messages"] child:conversationId];
}

- (FIRDatabaseReference*)getPositionsReference {
	return [[self.databaseRef child:@"positions"] child:[FirebaseManager getCurrentUserID]];
}

- (FIRDatabaseReference*)getNudgeReference:(NSString*)userId{
	return [[self.databaseRef child:@"nudge"] child:userId];
}

- (void)saveUserData:(User*)user profileImage:(UIImage*)image
		   completion:(void (^)(NSError *))completion {
	
	[self uploadUserImage:user profileImage:image competition:^(NSError* error) {
		if (error == nil) {
			[self uploadUserInfo:user competition:completion];
		} else {
			if (completion) completion(error);
		}
		
	}];
}

- (void)uploadUserInfo:(User*)user competition:(void (^)(NSError*))completion {
	[[self getCurrenUserReference] setValue:[user dictionaryValue] withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
		if (!error) {
			if (completion) completion(error);
		} else {
			if (completion) completion(nil);
		}
	}];
}

- (void)uploadUserImage:(User*)user profileImage:(UIImage*)image
		   competition:(void (^)(NSError *))completion {
	
	NSData *imgData = UIImageJPEGRepresentation(image, 0.9);
    NSTimeInterval time = [[NSDate date] timeIntervalSince1970] * 1000;
	NSString* userAvatarPath = [NSString stringWithFormat:@"images/%@%lf.jpg", user.key, time];
	
	// Create a reference to the file you want to upload
	FIRStorageReference *avatarRef = [self.storageRef child:userAvatarPath];
	[avatarRef putData:imgData
			  metadata:nil
			completion:^(FIRStorageMetadata *metadata, NSError *error) {
				
				if (error != nil) {
					if (completion)	completion(error);
				} else {
					NSString *downloadURL = [metadata.downloadURL absoluteString];
					user.userProfile.photoUrl = downloadURL;
                    
                    [self uploadAvatarImage:user profileImage:image competition:completion];
				}
			}];
}


- (void)uploadAvatarImage:(User*)user profileImage:(UIImage*)image
            competition:(void (^)(NSError *))completion {
    
    UIImage* avatarImage = [ImageUtils imageWithImage:image scaledToWidth:PROFILE_AVATAR_SIZE];
    NSData *imgData = UIImageJPEGRepresentation(avatarImage, 0.9);
    NSTimeInterval time = [[NSDate date] timeIntervalSince1970] * 1000;
    NSString* userAvatarPath = [NSString stringWithFormat:@"avatars/%@%lf.jpg", user.key, time];
    
    // Create a reference to the file you want to upload
    FIRStorageReference *avatarRef = [self.storageRef child:userAvatarPath];
    [avatarRef putData:imgData
              metadata:nil
            completion:^(FIRStorageMetadata *metadata, NSError *error) {
                
                if (error != nil) {
                    if (completion)    completion(error);
                } else {
                    NSString *downloadURL = [metadata.downloadURL absoluteString];
                    user.userProfile.avatarUrl = downloadURL;
                    
                    if (completion)    completion(nil);
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"completedRegister"];
                }
            }];
}


- (void)updateUserStatus {
	FIRDatabaseReference* statusOnlineRef = [[self getCurrenUserReference] child:@"/status/isOnline"];
	// stores the timestamp of my last disconnect (the last time I was seen online)
	FIRDatabaseReference* lastOnlineTimeRef = [[self getCurrenUserReference] child:@"/status/timestamp"];
	FIRDatabaseReference* connectedRef =  [[FIRDatabase database] referenceWithPath:@".info/connected"];
	
	[connectedRef observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot *snapshot) {
		if([snapshot.value boolValue]) {
			// connection established (or I've reconnected after a loss of connetion)
			[statusOnlineRef setValue:@YES];
			[statusOnlineRef onDisconnectSetValue:@NO];
			
			// when I disconnect, update the last time I was seen online
			[lastOnlineTimeRef setValue:[FIRServerValue timestamp]];
			[lastOnlineTimeRef onDisconnectSetValue:[FIRServerValue timestamp]];
		}
	}];
}

- (void)upgradedUser {
	[[[self getCurrenUserReference] child:@"isUpgraded"] setValue:@YES];
}

- (void)deleteAccount {
	NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
	[dict setValue:@NO forKey:@"isValid"];
	[dict setValue:[FIRServerValue timestamp] forKey:@"deletedTimestamp"];

	[[[self getCurrenUserReference] child:@"userProfile"] updateChildValues:dict];
	[[self getPositionsReference] removeValue];
    [[self getDiscoveryReference] removeValue];
    [[self getContactReference] removeValue];
    [[self getAchievementReference] removeValue];
    [[self getConversationReference] removeValue];
    
    [self removeLocation:[FirebaseManager getCurrentUserID]];
	
	//[[FIRAuth auth] signOut:nil];
}

- (void)updateUserSocial:(NSMutableDictionary*)dict {
	[[[self getCurrenUserReference] child:@"social"] updateChildValues:dict];
	[[[self getCurrenUserReference] child:@"isAddedSocial"] setValue:@YES];
}

- (void)updateService:(NSMutableDictionary*)dict {
	[[self getCurrenUserReference] updateChildValues:dict];
}

- (void)updateUserToken {
    NSString* token = [[FIRInstanceID instanceID] token];
    if (token.length > 0) {
        NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
        [dict setValue:[[FIRInstanceID instanceID] token] forKey:@"token"];
        [[self getCurrenUserReference] updateChildValues:dict];
    }
}

- (void)getCurrentUser:(void (^)(User *, NSError *))completion {
	FIRDatabaseReference* userRef = [self getCurrenUserReference];
	[childObservers addObject:userRef];
	[userRef observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
		 if (![snapshot.value isEqual:[NSNull null]]) {
			 User *user = [[User alloc] initWithDictionary:snapshot.value];
			 user.key = snapshot.key;
			 
			 self.loginedUser = user;
			 
			 if (completion) completion(user, nil);
		 } else
			 if (completion) completion(nil, nil);
		
	 } withCancelBlock:^(NSError * _Nonnull error) {
		 NSLog(@"%@", error.localizedDescription);
		 if (completion) completion(nil, error);
	 }];
}

- (void)getUser:(NSString*)key completion:(void (^)(User *, NSError *))completion {
	[[self getUserReference:key] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
		if (![snapshot.value isEqual:[NSNull null]]) {
			User *user = [[User alloc] initWithDictionary:snapshot.value];
			user.key = snapshot.key;
			
			if (completion) completion(user, nil);
		} else
			if (completion) completion(nil, nil);
		
	} withCancelBlock:^(NSError * _Nonnull error) {
		NSLog(@"%@", error.localizedDescription);
		if (completion) completion(nil, error);
	}];
}

- (void)getUserStatus:(NSString*)userId completion:(void (^)(Status *, NSError *))completion {
    userStatusRef = [[self getUserReference:userId] child:@"status"];
	[userStatusRef observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (![snapshot.value isEqual:[NSNull null]]) {
            Status* status = [[Status alloc] initWithDictionary:snapshot.value];
            
            if (completion)
                completion(status, nil);

        } else {
            if (completion)
                completion(nil, nil);
        }
	}];
}

- (void)removeUserStatusObserve {
	[userStatusRef removeAllObservers];
}

- (void)isExistUser:(NSString*)userId completion:(void (^)(BOOL, NSError *))completion {
    [[self getUserReference] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if ([snapshot hasChild:userId]) {
            if (completion) completion(YES, nil);
        } else {
            if (completion) completion(NO, nil);
        }
    } withCancelBlock:^(NSError * _Nonnull error) {
        NSLog(@"%@", error.localizedDescription);
        if (completion) completion(NO, error);
    }];
}

- (void)getNotifications:(void (^)(Notification *, BOOL, NSError *))completion {
	if (notificationRef != nil)
		return;
	
	[[self getNotificationReference] observeEventType:FIRDataEventTypeChildAdded withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
		if (![snapshot.value isEqual:[NSNull null]]) {
			Notification *notification = [[Notification alloc] initWithDictionary:snapshot.value];
            //NSLog(@"notification = %@", snapshot);
			notification.key = snapshot.key;
			
			if (completion) completion(notification, NO, nil);
		} else
			if (completion) completion(nil, NO, nil);
		
	} withCancelBlock:^(NSError * _Nonnull error) {
		NSLog(@"%@", error.localizedDescription);
		if (completion) completion(nil, NO, error);
	}];
	
	[[self getNotificationReference] observeEventType:FIRDataEventTypeChildChanged withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
		if (![snapshot.value isEqual:[NSNull null]]) {
			Notification *notification = [[Notification alloc] initWithDictionary:snapshot.value];
			
			if (completion) completion(notification, YES, nil);
		} else
			if (completion) completion(nil, NO, nil);
		
	} withCancelBlock:^(NSError * _Nonnull error) {
		NSLog(@"%@", error.localizedDescription);
		if (completion) completion(nil, NO, error);
	}];
}

- (void)removeNotification:(NSString*)userId notificationKey:(NSString*)notificationKey {
	[[[self getNotificationReferenceByUserId:userId] child:notificationKey] removeValue];
}

- (void)removeAllNotification:(NSString*)userId {
	[[self getNotificationReferenceByUserId:userId] removeValue];
}

- (void)updateNotificationReadStatus:(NSString*)notificationKey {
	NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
	
	[dict setValue:@(YES) forKey:@"isRead"];
	[[[self getNotificationReference] child:notificationKey] updateChildValues:dict];
}

- (void)getAllDiscoveries:(void (^)(NSError *))completion {
	[[self getDiscoveryReference] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
		
		if (![snapshot.children isEqual:[NSNull null]]) {
			for (FIRDataSnapshot *discoverySnapshot in snapshot.children) {
				Discovery *discovery = [[Discovery alloc] initWithDictionary:discoverySnapshot.value];
				discovery.userKey = discoverySnapshot.key;
				
				if (discovery.timestamp != 0)
					[[BaseDataManager sharedManager] addDiscovery:discovery];
			}
		}
		
		if (completion) completion(nil);
		
	} withCancelBlock:^(NSError * _Nonnull error) {
		NSLog(@"%@", error.localizedDescription);
		if (completion) completion(error);
	}];
}

- (void)getDiscoveryRules:(void (^)(DiscoveryRules *, NSError *))completion {
	FIRDatabaseReference* reference = [[self getCurrenUserReference] child:@"discoveryRules"];
	[childObservers addObject:reference];
	[reference observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
		DiscoveryRules *discoveryRules = nil;
		if (![snapshot.value isEqual:[NSNull null]]) {
			discoveryRules = [[DiscoveryRules alloc] initWithDictionary:snapshot.value];
		}
		
		if (completion) completion(discoveryRules, nil);
		
	} withCancelBlock:^(NSError * _Nonnull error) {
		NSLog(@"%@", error.localizedDescription);
		if (completion) completion(nil, error);
	}];
}

- (void)getConversations:(void (^)(Conversation *, BOOL, NSError *))completion {
	FIRDatabaseQuery *query = [[self getConversationReference] queryOrderedByChild:@"timestamp"];
	[childObservers addObject:query];
	[query observeEventType:FIRDataEventTypeChildAdded withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
		
		Conversation *conversation = nil;
		if (![snapshot.value isEqual:[NSNull null]]) {
			conversation = [[Conversation alloc] initWithDictionary:snapshot.value];
			conversation.conversationId = snapshot.key;
			[self getUser:conversation.userId completion:^(User* user, NSError* error) {
				if (user)
					conversation.userProfile = user.userProfile;				
				
				if (completion) completion(conversation, NO, error);
			}];
		} else
			if (completion) completion(conversation, NO, nil);
		
	} withCancelBlock:^(NSError * _Nonnull error) {
		NSLog(@"%@", error.localizedDescription);
		if (completion) completion(nil, NO, error);
	}];
	
	[query observeEventType:FIRDataEventTypeChildChanged withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
		Conversation *conversation = nil;
		if (![snapshot.value isEqual:[NSNull null]]) {
			conversation = [[Conversation alloc] initWithDictionary:snapshot.value];
			conversation.conversationId = snapshot.key;
			
			if (completion) completion(conversation, YES, nil);
		} else
			if (completion) completion(nil, YES, nil);
		
	} withCancelBlock:^(NSError * _Nonnull error) {
		NSLog(@"%@", error.localizedDescription);
		if (completion) completion(nil, NO, error);
	}];
}

- (void)updateDiscoveryRules:(NSDictionary*)newRulesDic completion:(void (^)(NSError *))completion {
	FIRDatabaseReference *discoveryRulesRef = [[self getCurrenUserReference] child:@"discoveryRules"];
	[discoveryRulesRef updateChildValues:newRulesDic withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
		if (!error) {
			if (completion) completion(nil);
		} else {
			if (completion) completion(error);
		}
	}];
}

- (void)updateDiscovery:(NSDictionary*)newDic userId:(NSString*)userId completion:(void (^)(NSError *))completion {
	FIRDatabaseReference *discoveryRef = [[self getDiscoveryReference] child:userId];
	[discoveryRef updateChildValues:newDic withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
		if (!error) {
			if (completion) completion(nil);
		} else {
			if (completion) completion(error);
		}
	}];
}

- (void)getDiscoveries:(void (^)(Discovery *, BOOL, NSError *))completion {
	if (getDiscoveryQuery) {
		[getDiscoveryQuery removeAllObservers];
		getDiscoveryQuery = nil;
	}
	
	getDiscoveryQuery = [[self getDiscoveryReference] queryOrderedByChild:@"timestamp"];
	[childObservers addObject:getDiscoveryQuery];
	[getDiscoveryQuery observeEventType:FIRDataEventTypeChildAdded withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
		Discovery *discovery = nil;
		if (![snapshot.value isEqual:[NSNull null]]) {
			discovery = [[Discovery alloc] initWithDictionary:snapshot.value];
			discovery.userKey = snapshot.key;
			[self getUser:discovery.userKey completion:^(User* user, NSError* error) {
				if (user)
					discovery.userProfile = user.userProfile;
				
				if ([BaseDataManager checkValidUser:user]) {
					if (completion) completion(discovery, NO, error);
				} else {
					if (completion) completion(nil, NO, error);
				}
				
			}];
		} else
			if (completion) completion(nil, NO, nil);
		
	} withCancelBlock:^(NSError * _Nonnull error) {
		NSLog(@"%@", error.localizedDescription);
		if (completion) completion(nil, NO, error);
	}];
	
	[getDiscoveryQuery observeEventType:FIRDataEventTypeChildChanged withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
		Discovery *discovery = nil;
		if (![snapshot.value isEqual:[NSNull null]]) {
			discovery = [[Discovery alloc] initWithDictionary:snapshot.value];
			discovery.userKey = snapshot.key;
			
			if (completion) completion(discovery, YES, nil);
		} else
			if (completion) completion(nil, YES, nil);
		
	} withCancelBlock:^(NSError * _Nonnull error) {
		NSLog(@"%@", error.localizedDescription);
		if (completion) completion(nil, NO, error);
	}];
}

- (void)getContacts:(void (^)(Contact *, NSError *))completion {
	FIRDatabaseReference* reference = [self getContactReference];
	[childObservers addObject:reference];
	[reference observeEventType:FIRDataEventTypeChildAdded withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
		Contact *contact = nil;
		if (![snapshot.value isEqual:[NSNull null]]) {
			contact = [[Contact alloc] initWithDictionary:snapshot.value];
			contact.contactKey = snapshot.key;
			[self getUser:contact.userId completion:^(User* user, NSError* error) {
				if (user)
					contact.userProfile = user.userProfile;
				
				if (completion) completion(contact, error);
			}];
		} else
			if (completion) completion(nil, nil);
		
	} withCancelBlock:^(NSError * _Nonnull error) {
		NSLog(@"%@", error.localizedDescription);
		if (completion) completion(nil, error);
	}];
}


- (void)importContact:(NSString*)phoneNumber completion:(void (^)(BOOL, NSError *))completion {
	NSLog(@"%@", phoneNumber);
	FIRDatabaseQuery *query = [[[self getUserReference] queryOrderedByChild:@"phoneNumber"] queryEqualToValue:phoneNumber];
	
	[query observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
		if (snapshot != NULL && ![snapshot.children isEqual:[NSNull null]]) {
			BOOL isImported = NO;
			for (FIRDataSnapshot *userSnapshot in snapshot.children) {
				User* user = [[User alloc] initWithDictionary:userSnapshot.value];
				if (!user.userProfile.isValid)
					continue;
				
				if (![BaseDataManager isDuplicatedContact:userSnapshot.key]) {					
					[self addContact:userSnapshot.key];
					isImported = YES;
				}
			}
			if (completion) completion(isImported, nil);
		} else
            if (completion) completion(NO, nil);
		
	} withCancelBlock:^(NSError * _Nonnull error) {
		NSLog(@"%@", error.localizedDescription);
		if (completion) completion(NO, error);
	}];
}

- (void)addContact:(NSString*)userId {
	NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
	[dict setObject:userId forKey:@"userId"];
	
	[[[[FirebaseManager sharedManager] getContactReference] childByAutoId] updateChildValues:dict];
	[self sendNotification:userId notificationType:NotificationImportedContact];
}

- (void)getMessage:(NSString*)userId completion:(void (^)(Message *, FIRDataEventType, NSError *))completion {
	NSString* conversationId = [FirebaseManager getConversationId:userId];
	getMessageQuery = [[self getMessageReference:conversationId] queryLimitedToLast:READ_MESSAGES_COUNT] ;
	[getMessageQuery observeEventType:FIRDataEventTypeChildAdded withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
		Message *message = nil;
		if (![snapshot.value isEqual:[NSNull null]]) {
			message = [[Message alloc] initWithDictionary:snapshot.value];
			message.messageId = snapshot.key;

			if (!message.isRead && ![message.senderId isEqualToString:[FirebaseManager getCurrentUserID]]) {
				NSMutableDictionary* messageDict = [[NSMutableDictionary alloc] init];
				[messageDict setValue:[FIRServerValue timestamp] forKey:@"readTimestamp"];
				[messageDict setValue:@YES forKey:@"isRead"];
				
				[[[self getMessageReference:conversationId] child:snapshot.key] updateChildValues:messageDict];
				
				NSMutableDictionary* convDict = [[NSMutableDictionary alloc] init];
				[convDict setValue:@YES forKey:@"isRead"];
				
				[[[self getConversationReference] child:conversationId] updateChildValues:convDict];
			}
			
			if (completion) completion(message, FIRDataEventTypeChildAdded, nil);
		} else
			if (completion) completion(nil, FIRDataEventTypeChildAdded, nil);
		
	} withCancelBlock:^(NSError * _Nonnull error) {
		NSLog(@"%@", error.localizedDescription);
		if (completion) completion(nil, FIRDataEventTypeChildAdded, error);
	}];
	
	[getMessageQuery observeEventType:FIRDataEventTypeChildChanged withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
		Message *message = nil;
		if (![snapshot.value isEqual:[NSNull null]]) {
			message = [[Message alloc] initWithDictionary:snapshot.value];
			message.messageId = snapshot.key;
			
			if (completion) completion(message, FIRDataEventTypeChildChanged, nil);
		} else
			if (completion) completion(nil, FIRDataEventTypeChildChanged, nil);
		
	} withCancelBlock:^(NSError * _Nonnull error) {
		NSLog(@"%@", error.localizedDescription);
		if (completion) completion(nil, FIRDataEventTypeChildChanged, error);
	}];
	
	[getMessageQuery observeEventType:FIRDataEventTypeChildRemoved withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
		Message *message = nil;
		if (![snapshot.value isEqual:[NSNull null]]) {
			message = [[Message alloc] initWithDictionary:snapshot.value];
			message.messageId = snapshot.key;
			
			if (completion) completion(message, FIRDataEventTypeChildRemoved, nil);
		} else
			if (completion) completion(nil, FIRDataEventTypeChildRemoved, nil);
		
	} withCancelBlock:^(NSError * _Nonnull error) {
		NSLog(@"%@", error.localizedDescription);
		if (completion) completion(nil, FIRDataEventTypeChildRemoved, error);
	}];
}

- (void)removeMessage:(NSString*)userId messageId:(NSString*)messageId {
	NSString* conversationId = [FirebaseManager getConversationId:userId];
	
	[[[self getMessageReference:conversationId] child:messageId] removeValue];
}

- (void)editMessage:(NSString*)userId message:(NSString*)message messageId:(NSString*)messageId {
	NSString* conversationId = [FirebaseManager getConversationId:userId];
	NSMutableDictionary* msgDict = [[NSMutableDictionary alloc] init];
	[msgDict setValue:message forKey:@"text"];
	
	[[[self getMessageReference:conversationId] child:messageId] updateChildValues:msgDict];
}

- (void)removeGetMessageObserve {
	[getMessageQuery removeAllObservers];
}

- (void)monitoringBlockUser:(NSString*)userId completion:(void (^)(BlockUser *, NSError *))completion {
	NSString* conversationId = [FirebaseManager getConversationId:userId];
	
	blockUserRef = [[[self getConversationReference] child:conversationId] child:@"blockUser"];
	[blockUserRef observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
		if (![snapshot.value isEqual:[NSNull null]]) {
			BlockUser* bUser = [[BlockUser alloc] initWithDictionary:snapshot.value];
			if (completion) completion(bUser, nil);
		} else
			if (completion) completion(nil, nil);
		
	} withCancelBlock:^(NSError * _Nonnull error) {
		NSLog(@"%@", error.localizedDescription);
		if (completion) completion(nil, error);
	}];
}

- (void)removeMonitoringBlockUserObserve {
	[blockUserRef removeAllObservers];
}

- (void)doBlockUser:(NSString*)userId blockIds:(NSString*)blockIds {
	NSString* conversationId = [FirebaseManager getConversationId:userId];
	
	NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
	[dict setValue:blockIds forKey:@"Ids"];
	
	[[[[self getConversationReference] child:conversationId] child:@"blockUser"] updateChildValues:dict];
	[[[[self getConversationReferenceByUserId:userId] child:conversationId] child:@"blockUser"] updateChildValues:dict];
}

- (void)doUnBlockUser:(NSString*)userId blockIds:(NSString*)blockIds {
	NSString* conversationId = [FirebaseManager getConversationId:userId];
	if (blockIds.length == 0) {
		[[[[self getConversationReference] child:conversationId] child:@"blockUser"] removeValue];
		[[[[self getConversationReferenceByUserId:userId] child:conversationId] child:@"blockUser"] removeValue];
		
	} else {
		[self doBlockUser:userId blockIds:blockIds];
	}
}


- (void)sendImageMessage:(User*)user image:(UIImage*)image
             competition:(void (^)(NSString*, NSError *))completion {
    
    NSData *imgData = UIImageJPEGRepresentation(image, 0.9);
    NSTimeInterval time = [[NSDate date] timeIntervalSince1970] * 1000;
    NSString* imagePath = [NSString stringWithFormat:@"messages/%@%lf.jpg", user.key, time];
    
    // Create a reference to the file you want to upload
    FIRStorageReference *avatarRef = [self.storageRef child:imagePath];
    [avatarRef putData:imgData
              metadata:nil
            completion:^(FIRStorageMetadata *metadata, NSError *error) {
                
                if (error != nil) {
                    if (completion)    completion(nil, error);
                } else {
                    NSString *downloadURL = [metadata.downloadURL absoluteString];
                    if (completion)    completion(downloadURL, nil);
                }
            }];
}


- (void)sendMessage:(NSDictionary*)msgDict message:(NSString*)messasge userId:(NSString*)userId completion:(void(^)(NSError*))completion {
	NSString* conversationId = [FirebaseManager getConversationId:userId];
	FIRDatabaseReference *messageRef = [[self getMessageReference:conversationId] childByAutoId];
	[messageRef setValue:msgDict withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
		if (!error) {
            //[self updateConversations:messasge userId:userId];
			[self updateDiscoveries:messasge userId:userId];
		} else {
			if (completion) completion(error);
		}
	}];
    
    //This part should be called as soon as sending message.
    //if not, after updating conversation status on onChildAdded of getMessage on Android, this part would be called.
    //it makes conversation 'unread' status again.
    [self updateConversations:messasge userId:userId];
}

- (void)getNudgeMessage:(void (^)(NudgeMessage *, NSError *))completion {
	NSString *userId = [FirebaseManager getCurrentUserID];
	FIRDatabaseReference *reference = [self getNudgeReference:userId];
	[childObservers addObject:reference];
	[reference observeEventType:FIRDataEventTypeChildAdded withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
		if (![snapshot.value isEqual:[NSNull null]]) {
			NudgeMessage* message = [[NudgeMessage alloc] initWithDictionary:snapshot.value];
			message.key = snapshot.key;
			
			if (completion) completion(message, nil);
		} else
			if (completion) completion(nil, nil);
		
	} withCancelBlock:^(NSError * _Nonnull error) {
		NSLog(@"%@", error.localizedDescription);
		if (completion) completion(nil, error);
	}];
}

- (void)sendNudgeMessage:(Message*)message userId:(NSString*)userId{
	User* me = [FirebaseManager sharedManager].loginedUser;
	
	NSString* title = [NSString stringWithFormat:@"%@ just send you a message", me.userProfile.username];
	NSMutableDictionary *messageDict = [[NSMutableDictionary alloc] init];
	[messageDict setValue:me.userProfile.photoUrl forKey:@"photoUrl"];
	[messageDict setValue:me.key forKey:@"senderId"];
	[messageDict setValue:title forKey:@"title"];
	[messageDict setValue:message.text forKey:@"message"];
	
	[[[self getNudgeReference:userId] childByAutoId] setValue:messageDict];	

	NSString *conversationId = [FirebaseManager getConversationId:userId];
	NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
	[dict setValue:@YES forKey:@"isNotified"];
	
	[[[self getMessageReference:conversationId] child:message.messageId] updateChildValues:dict];
}

- (void)sendPush:(NSDictionary *)params {
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:FCM_SERVER_URL]];
	if (!request) NSLog(@"Error creating the URL Request");
	
	[request setHTTPMethod:@"POST"];
	if (params != nil) {
		NSError *jsonError;
		[request setHTTPBody:[NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:&jsonError]];
		NSLog(@"Request Body -- %@",params);
	}
	
	[request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
	[request setValue:FCM_AUTH_KEY forHTTPHeaderField:@"Authorization"];
	NSLog(@"will create connection");
	
	NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration] delegate:nil delegateQueue:[NSOperationQueue mainQueue]];
	NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
		
	}];
	
	[dataTask resume];
}

- (void)removeNudgeMessage:(NSString*)messageKey {
	NSString* userId = self.loginedUser.key;
	
	[[[self getNudgeReference:userId] child:messageKey] removeValue];
}

/**
 * Send Notification
 * @param userId Receiver Id
 * @param notificationType the kind of notification
 */
- (void)sendNotification:(NSString*)userId notificationType:(int)notificationType {
	User* mine = [FirebaseManager sharedManager].loginedUser;
	NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
	
	[dict setValue:mine.key forKey:@"userId"];
	[dict setValue:[FIRServerValue timestamp] forKey:@"timestamp"];
	[dict setValue:@(NO) forKey:@"isRead"];
	[dict setValue:[NSNumber numberWithInt:notificationType] forKey:@"notificationType"];

	[[[self getNotificationReferenceByUserId:userId] childByAutoId] setValue:dict];
}

/**
 * Update Conversation data.
 */
- (void)updateConversations:(NSString*)lastMessage userId:(NSString*)userId {
	//Update Conversation data.
	NSString* conversationId = [FirebaseManager getConversationId:userId];
	
	NSMutableDictionary* myDict = [[NSMutableDictionary alloc] init];
	[myDict setValue:userId forKey:@"userId"];
	[myDict setValue:[FIRServerValue timestamp] forKey:@"timestamp"];
	[myDict setValue:[NSString stringWithFormat:@"You:%@", lastMessage] forKey:@"lastMessage"];
	[myDict setValue:@YES forKey:@"isRead"];
	
	[[[[FirebaseManager sharedManager] getConversationReference] child:conversationId] updateChildValues:myDict];

	NSMutableDictionary* opponentDict = [[NSMutableDictionary alloc] init];
	[opponentDict setValue:[FirebaseManager getCurrentUserID] forKey:@"userId"];
	[opponentDict setValue:[FIRServerValue timestamp] forKey:@"timestamp"];
	[opponentDict setValue:lastMessage forKey:@"lastMessage"];
	[opponentDict setValue:@NO forKey:@"isRead"];
	
	[[[[FirebaseManager sharedManager] getConversationReferenceByUserId:userId] child:conversationId] updateChildValues:opponentDict];
}

/**
 * Update discoveries after sending a message.
 */
- (void)updateDiscoveries:(NSString*)lastMessage userId:(NSString*)userId {
	
	NSMutableDictionary* myDict = [[NSMutableDictionary alloc] init];
	[myDict setValue:[NSString stringWithFormat:@"You:%@", lastMessage] forKey:@"lastMessage"];
	
	[[[[FirebaseManager sharedManager] getDiscoveryReference] child:userId] updateChildValues:myDict];
	
	NSMutableDictionary* opponentDict = [[NSMutableDictionary alloc] init];
	[opponentDict setValue:lastMessage forKey:@"lastMessage"];

	[[[[FirebaseManager sharedManager] getDiscoveryReferenceByUserId:userId] child:[FirebaseManager getCurrentUserID]] updateChildValues:opponentDict];
}


- (void)getAchievements:(NSString*)userKey completion:(void (^)(Achievement *, NSError *))completion {
	[[self getAchievementReferenceByUserId:userKey] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
		if (![snapshot.value isEqual:[NSNull null]]) {
			
			for (FIRDataSnapshot *achievementSnapshot in snapshot.children) {
				Achievement *achievement = [[Achievement alloc] initWithDictionary:achievementSnapshot.value];
				
				if (completion) completion(achievement, nil);
			}
		} else
			if (completion) completion(nil, nil);
		
	} withCancelBlock:^(NSError * _Nonnull error) {
		NSLog(@"%@", error.localizedDescription);
		if (completion) completion(nil, error);
	}];
}


/**
 * Update achievement!
 */
- (void)updateAchievements:(AchievementType)achievementType {
	NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
	
	NSString* childKey = @"1";
	switch (achievementType) {
		case AchievementStartConverstation:
			[dict setValue:ACHIEVEMENT_START_CONVERSATION forKey:@"type"];
			[dict setValue:@"Start a conversation" forKey:@"name"];
			[dict setValue:[NSNumber numberWithInt:25] forKey:@"value"];
			childKey = @"1";
			break;
		case AchievementUseAppFor2Weeks:
			[dict setValue:ACHIEVEMENT_USE_APP_TWO_WEEKS forKey:@"type"];
			[dict setValue:@"Use the app for 2 weeks" forKey:@"name"];
			[dict setValue:[NSNumber numberWithInt:25] forKey:@"value"];
			childKey = @"2";
			break;
		case AchievementImportContact:
			[dict setValue:ACHIEVEMENT_IMPORT_CONTACTS forKey:@"type"];
			[dict setValue:@"Import your contacts" forKey:@"name"];
			[dict setValue:[NSNumber numberWithInt:25] forKey:@"value"];
			
			childKey = @"3";
			break;
	}
	
	[dict setValue:[FIRServerValue timestamp] forKey:@"timestamp"];
	
	[[[self getAchievementReference] child:childKey] updateChildValues:dict];
}

+ (NSString*)getConversationId:(NSString*)secondUserId {
	NSString* userId = [FirebaseManager getCurrentUserID];
	
	return [userId compare:secondUserId] > 0?
	[NSString stringWithFormat:@"%@_%@", userId, secondUserId]: [NSString stringWithFormat:@"%@_%@", secondUserId, userId];
}

+ (void)showImageWithUrl:(UIImageView*)imageView imagePath:(NSString*)imagePath {
	if (imagePath.length != 0) {
        [imageView sd_setImageWithURL:[NSURL URLWithString:imagePath]];
//        FIRStorageReference *storageRef = [[FIRStorage storage] referenceForURL:imagePath];
//        [imageView sd_setImageWithStorageReference:storageRef
//                                            placeholderImage:nil
//                                                  completion:^(UIImage *image,
//                                                               NSError *error,
//                                                               SDImageCacheType
//                                                               cacheType,
//                                                               FIRStorageReference *ref) {
//                                                      if (error != nil) {
//                                                          NSLog(@"Error loading image: %@", error.localizedDescription);
//                                                      } else {
//                                                          //The cached images should be removed from disk to show updated image.
//                                                          //SDImageCache* cache =  [SDImageCache sharedImageCache];
//                                                          //[cache removeImageForKey:ref.fullPath fromDisk:YES withCompletion:nil];
//                                                      }
//                                                  }];
	}
}

+ (void)showProfilePhoto:(UIImageView*)imageView userProfile:(UserProfile*)userProfile {
    NSString* imagePath = userProfile.photoUrl;
    if (userProfile.avatarUrl.length > 0)
        imagePath = userProfile.avatarUrl;
    
    if (imagePath.length != 0) {
        [imageView sd_setImageWithURL:[NSURL URLWithString:imagePath]];
        
//        FIRStorageReference *storageRef = [[FIRStorage storage] referenceForURL:imagePath];
//        [imageView sd_setImageWithStorageReference:storageRef
//                                  placeholderImage:nil
//                                        completion:^(UIImage *image,
//                                                     NSError *error,
//                                                     SDImageCacheType
//                                                     cacheType,
//                                                     FIRStorageReference *ref) {
//                                            if (error != nil) {
//                                               // NSLog(@"Error loading image: %@", error.localizedDescription);
//                                            } else {
//                                                //The cached images should be removed from disk to show updated image.
//                                                //SDImageCache* cache =  [SDImageCache sharedImageCache];
//                                                //[cache removeImageForKey:ref.fullPath fromDisk:YES withCompletion:nil];
//                                            }
//                                        }];
    }
}


- (void)removeAllObservers {
	for (FIRDatabaseReference *query in childObservers)
		[query removeAllObservers];
	
	[geoRegionQuery removeAllObservers];
	[childObservers removeAllObjects];
}

#pragma mark Geofire
- (GeoFire*)getGeoFire {
	FIRDatabaseReference *geofireRef = [[FIRDatabase database] referenceWithPath:@"positions"];
	GeoFire *geoFire = [[GeoFire alloc] initWithFirebaseRef:geofireRef];
	
	return geoFire;
}

- (void)setUserLocation:(NSString*)userId location:(CLLocation*)location {
	GeoFire* geoFire = [self getGeoFire];
	[geoFire setLocation:location forKey:userId];
    
    //Add position to user data [26/03/2018]
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
    [dict setValue:[NSNumber numberWithDouble:location.coordinate.latitude] forKey:@"latitude"];
    [dict setValue:[NSNumber numberWithDouble:location.coordinate.longitude] forKey:@"longitude"];
    [[[self getCurrenUserReference] child:@"position"] updateChildValues:dict];
}

- (void)removeLocation:(NSString*)userId {
	GeoFire* geoFire = [self getGeoFire];
	[geoFire removeKey:userId];
}


/**
 * Get users near by radius
 */
- (void)getNearUsers:(CLLocation*)center radius:(double)radius
          completion:(void (^)(User *, CLLocation*, NSError *))completion {
    
    if (geoRegionQuery != nil)
        [geoRegionQuery removeAllObservers];    
    
	GeoFire* geoFire = [self getGeoFire];
    //double rangeRadius = (double)[self getDiscoveryRangeValue] * 1000;
    double rangeRadius = (double)radius * 1000;
    MKCoordinateRegion region;
    if (rangeRadius == (DISCOVERY_FINDING_RADIUS * 1000)) { //All of globe
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(0, 0);
        region = MKCoordinateRegionMakeWithDistance(coordinate, 18000 * 1000, 40000 * 1000);
    } else { // radius is 1km 
        region = MKCoordinateRegionMakeWithDistance(center.coordinate, rangeRadius, rangeRadius);
    }
    
    geoRegionQuery = [geoFire queryWithRegion:region];
	//geoCircleQuery = [geoFire queryAtLocation:center withRadius:[self getDiscoveryRangeValue]];
	[geoRegionQuery observeEventType:GFEventTypeKeyEntered withBlock:^(NSString *key, CLLocation *location) {
		if ([key isEqualToString:self.loginedUser.key]) {
			if (completion) completion(nil, nil, nil);
			return;
		}
		
		[self getUser:key completion:^(User* user, NSError* error) {
			if (error) {
				if (completion) completion(nil, nil, error);
			} else
				if (completion) completion(user, location, nil);
		}];
	}];
}

/**
 * Get users in some country according to the country filter of discovery rules.
 */
- (void)getUsersByCountryFilter:(void (^)(FIRDataSnapshot *, NSError *))completion {
    User* me = [FirebaseManager sharedManager].loginedUser;
    FIRDatabaseQuery *query = [[[self getUserReference] queryOrderedByChild:@"countryCode"] queryEqualToValue:me.discoveryRules.countryFilter];
    
    [query observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (snapshot != NULL && ![snapshot.children isEqual:[NSNull null]]) {
            
            if (completion) completion(snapshot, nil);
        } else
            if (completion) completion(nil, nil);
        
    } withCancelBlock:^(NSError * _Nonnull error) {
        NSLog(@"%@", error.localizedDescription);
        if (completion) completion(NO, error);
    }];
}
@end
