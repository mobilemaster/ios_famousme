//
//  BaseDataManager.m
//  FamousMe
//
//  Created by Shine Man on 12/12/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "BaseDataManager.h"
#import "DateUtils.h"

@interface BaseDataManager () {
	int newNotifications;
	
	NSMutableArray* discoveryArray;
	NSMutableArray* contactArray;
	int conversationCount;
}

@end

@implementation BaseDataManager
+ (instancetype)sharedManager {
	static BaseDataManager *sharedInstance = nil;
	static dispatch_once_t pred;
	
	dispatch_once(&pred, ^{
		sharedInstance = [[BaseDataManager alloc] init];
	});
	
	return sharedInstance;
}

- (instancetype)init {
	self = [super init];
	if (self) {
		newNotifications = 0;
		conversationCount = 0;
		discoveryArray = [NSMutableArray array];
		contactArray = [NSMutableArray array];
	}
	
	return self;
}

- (void)setNewNotifications:(int)notifications {
	newNotifications = notifications;
}

- (int)getNewNotifications {
	return newNotifications;
}

- (void)addNewNotification {
	newNotifications++;
}

- (NSMutableArray*)getDiscoveryArray {
	return discoveryArray;
}

- (void)addDiscovery:(Discovery*)discovery {
	[discoveryArray addObject:discovery];
}

- (void)setConversationCount:(int)newCount {
	conversationCount = newCount;
}

- (int)getConversationCount {
	return conversationCount;
}

- (NSMutableArray*)getContactArray {
	return contactArray;
}

- (void)addContact:(Contact*)contact {
	[contactArray insertObject:contact atIndex:0];
}

- (void)removeAllData {
	newNotifications = 0;
	conversationCount = 0;
	[discoveryArray removeAllObjects];
	[contactArray removeAllObjects];
}

/**
 * Check if has conversations or not
 */
+ (BOOL)hasConversations {
	return [[BaseDataManager sharedManager] getConversationCount] > 0? YES: NO;
}

/**
 * Check with user key if the discovery is duplicated or not
 * When the app is launched, get all discoveries at first.
 * and then when the user would be added by Geofire, it should be checked whether it has already added or not.
 * @param userKey user id
 * @return whther duplicated or not
 */
+ (BOOL)isDuplicatedDiscovery:(NSString*)userKey {
	NSMutableArray* discoveryArray = [[BaseDataManager sharedManager] getDiscoveryArray];
	for (Discovery* discovery in discoveryArray) {
		if ([discovery.userKey isEqualToString:userKey])
			return YES;
	}
	
	return NO;
}

/**
 * Check with user key if the contact is duplicated or not.
 */
+ (BOOL)isDuplicatedContact:(NSString*)userKey {
	NSMutableArray* contactArray = [[BaseDataManager sharedManager] getContactArray];
	for (Contact* contact in contactArray) {
		if ([contact.userId isEqualToString:userKey])
			return YES;
	}
	
	return NO;
}

+ (NSArray*)getInterestList {
	NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Interests" ofType:@"plist"]];
	
	NSArray *array = [dictionary objectForKey:@"Interests"];
	
	return array;
}

+ (NSInteger)getInterestIndex:(NSString*)inInterest {
	NSArray* interestList = [BaseDataManager getInterestList];
	for (NSInteger i = 0; i < interestList.count; i++ ) {
		NSString* interest = [interestList objectAtIndex:i];
		if ([interest isEqualToString:inInterest])
			return i;
	}
	
	return -1;
}

/**
 * Get the value array from plist file.
 */
+ (NSArray*)getValueArrayFromPlistFile:(NSString*)fileName valueName:(NSString*)valueName {
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:fileName ofType:@"plist"]];
    
    NSArray *array = [dictionary objectForKey:valueName];
    
    return array;
}

/**
 * Get national list from plist file.
 */
+ (NSArray*)getNationalityList {
    return [BaseDataManager getValueArrayFromPlistFile:@"Nationalities" valueName:@"Nationalities"];
}

/**
 * Get service list from plist file.
 */
+ (NSArray*)getServiceList {
    return [BaseDataManager getValueArrayFromPlistFile:@"Services" valueName:@"Services"];
}

/**
 * Get discovery range list from plist file.
 */
+ (NSArray*)getDiscoveryRangeList {
    return [BaseDataManager getValueArrayFromPlistFile:@"DiscoveryRange" valueName:@"DiscoveryRange"];
}

/**
 * Get online filter list from plist file.
 */
+ (NSArray*)getOnlineFilterList {
    return [BaseDataManager getValueArrayFromPlistFile:@"Filter" valueName:@"Online"];
}

+ (NSDictionary*)getCountryData {   
    NSString *path = [[NSBundle mainBundle] pathForResource:@"countryCodes" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:path];
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    
    return dict;
}

+ (NSString*)getCountryCodeByLocaleCode {
    NSLocale *locale = [NSLocale currentLocale];
    NSString *localeCountryCode = [locale objectForKey: NSLocaleCountryCode];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"countryCodes" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:path];
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    
    for (NSDictionary *country in dict) {
        NSString *code = [country objectForKey:@"code"];
        if ([localeCountryCode isEqualToString:code]) {
            NSString *dialCode = [country objectForKey:@"dial_code"];
            return dialCode;
        }
    }
    
    return nil;
}

/**
 * Get the country name by country dial code
 * For example, + 971
 */
+ (NSString*)getCountryNameByCode:(NSString*)code {
    if ([code isEqualToString:@"+1"])
        return @"Canada, United States";
    
    NSDictionary* dict = [BaseDataManager getCountryData];
    NSString* countryName = nil;
    for (NSDictionary *country in dict) {
        NSString *dialCode = [country objectForKey:@"dial_code"];
        if ([code isEqualToString:dialCode]) {
            countryName = [country objectForKey:@"name"];
            break;
        }
    }
    
    return countryName;
}

+ (NSInteger)getNationalityIndex:(NSString*)inNationality {
	NSArray* nationalityList = [BaseDataManager getNationalityList];
	for (NSInteger i = 0; i < nationalityList.count; i++ ) {
		NSString* nationality = [nationalityList objectAtIndex:i];
		if ([nationality isEqualToString:inNationality])
			return i;
	}
	
	return -1;
}

/**
 * Check if the user is valid or not by discovery rules.
 */
+ (BOOL)checkValidUser:(User*)user {
	User* myProfile = [FirebaseManager sharedManager].loginedUser;
    if (myProfile == nil || myProfile.discoveryRules == nil)
        return false;
	
	if (myProfile.discoveryRules.discoverGender != 0 &&
		(myProfile.discoveryRules.discoverGender - 1) != user.gender) {
		return false;
	}
    
    if (!user.userProfile.isValid)
        false;
    
    //Online Filter
    if (myProfile.discoveryRules.onlineFilter != NO_TIME_LIMIT) { //0 is No Limit
        int timeGap = [DateUtils getGapOfDate:user.status.timestamp];
        NSUInteger onlineFilter = 0;
        switch (myProfile.discoveryRules.onlineFilter) {
            case A_DAY:
                onlineFilter = 1;
                break;
            case A_WEEK:
                onlineFilter = 15;
                break;
            case A_MONTH:
                onlineFilter = 31;
                break;
            default:
                break;
        }
        
        if (timeGap > onlineFilter)
            return false;
    }
    
    //Service Filter
    if (myProfile.discoveryRules.serviceFilter != 0) { // 0 is No Service Filter.
        NSString* serviceFilter = [[BaseDataManager getServiceList] objectAtIndex:myProfile.discoveryRules.serviceFilter];

        if (![user.serviceTitle isEqualToString:serviceFilter])
            return false;
    }    
	
	//Age Range
	if (myProfile.discoveryRules.ageRange != 0) {
		
		NSInteger userAge = [DateUtils getAge:user.dateOfBirth];
		NSArray* rangeArray = AGE_RANGE_ARRAY;
		
		NSString* range = [rangeArray objectAtIndex:myProfile.discoveryRules.ageRange];
		range = [range stringByReplacingOccurrencesOfString:@" " withString:@""];
		
		NSArray* rangeStringArray = [range componentsSeparatedByString:@"~"];
		int startRange = [[rangeStringArray objectAtIndex:0] intValue];
		
		int endRange = 200;
		if (startRange < 60)
			endRange = [[rangeArray objectAtIndex:1] intValue];
		
		if (userAge < startRange || userAge > endRange)
			return false;
	}
	
    if (myProfile.discoveryRules.discoverNationality.length == 0) {
        return false;
    }
        
	//Nationality
	if (![myProfile.discoveryRules.discoverNationality isEqualToString:@"All"]) {
		if (![myProfile.discoveryRules.discoverNationality isEqualToString:user.nationality])
			return NO;
	}
	
	NSArray* userInterests = [user.interests componentsSeparatedByString:@"/"];
	//Interests
	if (![myProfile.discoveryRules.discoveryInterest isEqualToString:@"All"]) {
		for (NSString* userInterest in userInterests) {
			if ([myProfile.discoveryRules.discoveryInterest containsString:userInterest]) {
				return YES;
			}
		}
	}
	
	return true;
}

/**
 * Get Discovery Range value
 * @return discovery range by km
 */
+ (int)getDiscoveryRangeValue {
    User* myProfile = [FirebaseManager sharedManager].loginedUser;
    if (myProfile == nil)
        return 0;
    
    NSArray* rangeArrary = [BaseDataManager getDiscoveryRangeList];
    NSString* range = [rangeArrary objectAtIndex:myProfile.discoveryRules.discoveryRange];
    
    if ([range isEqualToString:@"No Limit"])
        return DISCOVERY_FINDING_RADIUS;
    
    range = [range stringByReplacingOccurrencesOfString:@"km" withString:@""];
    return [range integerValue];
}


/**
 * Get Distance string.
 */
+ (NSString*)getDistanceString:(int)distance {
    if (distance < 0)
        return @"";
    
	if (distance < 150)
		return @"Near you";
    else {
        if (distance >= 1000) {
            float fValue = ((float) distance) / 1000;
            return [NSString stringWithFormat:@"%.02f %@", fValue, @"km away"];
        }
        
        return [NSString stringWithFormat:@"%d %@", distance, @"m away"];
    }    
}

/**
 * Get the resized image.
 */
+ (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

@end
