//
//  AlertViewManager.m
//  FamousMe
//
//  Created by abc on 11/10/16.
//  Copyright © 2016 Shine Man. All rights reserved.
//


#import "AlertViewManager.h"

@implementation AlertViewManager

+ (UIAlertController*)getAlertView:(NSString*)title msg:(NSString*)msg
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:msg
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                          }];
    [alert addAction:defaultAction];
    
    return alert;
}

+ (void)showAlertView:(NSString*)title msg:(NSString*)msg parent:(UIViewController*)parent
{
    UIAlertController* alert = [AlertViewManager getAlertView:title msg:msg];
    [parent presentViewController:alert animated:YES completion:nil];
}
@end
