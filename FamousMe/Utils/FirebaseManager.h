//
//  FirebaseManager.h
//  FamousMe
//
//  Created by Shine Man on 12/12/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GeoFire.h"
#import "User.h"
#import "Notification.h"
#import "Discovery.h"
#import "DiscoveryRules.h"
#import "Conversation.h"
#import "Message.h"
#import "NudgeMessage.h"
#import "Contact.h"
#import "Achievement.h"
#import "Status.h"

@import Firebase;

@interface FirebaseManager : NSObject
@property (nonatomic) User *loginedUser;
@property (nonatomic) CLLocation *currentLocation;

+ (instancetype)sharedManager;
+ (FIRUser*)currentUser;
+ (NSString *)getCurrentUserID;
+ (NSString*)getPhoneNumber;
+ (void)showImageWithUrl:(UIImageView*)imageView imagePath:(NSString*)imagePath;
+ (void)showProfilePhoto:(UIImageView*)imageView userProfile:(UserProfile*)userProfile;
+ (NSString*)getConversationId:(NSString*)secondUserId;

- (void)getCurrentUser:(void (^)(User *, NSError *))completion;
- (void)getUser:(NSString*)key completion:(void (^)(User *, NSError *))completion;
- (void)uploadUserInfo:(User*)user competition:(void (^)(NSError*))completion;
- (void)saveUserData:(User*)user profileImage:(UIImage*)image
		  completion:(void (^)(NSError *))completion;
- (void)updateUserStatus;
- (void)upgradedUser;
- (void)deleteAccount;

- (void)updateUserToken;
- (void)updateUserSocial:(NSMutableDictionary*)dict;
- (void)updateService:(NSMutableDictionary*)dict;

- (void)getUserStatus:(NSString*)userId completion:(void (^)(Status *, NSError *))completion;
- (void)removeUserStatusObserve;
- (void)isExistUser:(NSString*)userId completion:(void (^)(BOOL, NSError *))completion;

- (void)getNotifications:(void (^)(Notification *, BOOL, NSError *))completion;
- (void)stopNotification;
- (void)removeNotification:(NSString*)userId notificationKey:(NSString*)notificationKey;
- (void)removeAllNotification:(NSString*)userId;

- (void)updateNotificationReadStatus:(NSString*)notificationKey;

- (void)getAllDiscoveries:(void (^)(NSError *))completion;
- (void)getDiscoveryRules:(void (^)(DiscoveryRules *, NSError *))completion;

- (void)getConversations:(void (^)(Conversation *, BOOL, NSError *))completion;
- (void)getDiscoveries:(void (^)(Discovery *, BOOL, NSError *))completion;
- (void)getContacts:(void (^)(Contact *, NSError *))completion;

- (void)monitoringBlockUser:(NSString*)userId completion:(void (^)(BlockUser *, NSError *))completion;
- (void)getMessage:(NSString*)userId completion:(void (^)(Message *, FIRDataEventType, NSError *))completion;
- (void)sendMessage:(NSDictionary*)msgDict message:(NSString*)messasge userId:(NSString*)userId completion:(void(^)(NSError*))completion;
- (void)sendNudgeMessage:(Message*)message userId:(NSString*)userId;
- (void)getNudgeMessage:(void (^)(NudgeMessage *, NSError *))completion;
- (void)sendPush:(NSDictionary *)params;
- (void)removeNudgeMessage:(NSString*)messageKey;
- (void)sendImageMessage:(User*)user image:(UIImage*)image
             competition:(void (^)(NSString*, NSError *))completion;

- (void)removeMessage:(NSString*)userId messageId:(NSString*)messageId;
- (void)editMessage:(NSString*)userId message:(NSString*)message messageId:(NSString*)messageId;
- (void)importContact:(NSString*)phoneNumber completion:(void (^)(BOOL, NSError *))completion;
- (void)addContact:(NSString*)userId;
- (void)doBlockUser:(NSString*)userId blockIds:(NSString*)blockIds;
- (void)doUnBlockUser:(NSString*)userId blockIds:(NSString*)blockIds;

- (void)removeGetMessageObserve;
- (void)removeMonitoringBlockUserObserve;

- (void)sendNotification:(NSString*)userId notificationType:(int)notificationType;

- (void)getAchievements:(NSString*)userKey completion:(void (^)(Achievement *, NSError *))completion;

- (void)updateConversations:(NSString*)lastMessage userId:(NSString*)userId;
- (void)updateDiscoveries:(NSString*)lastMessage userId:(NSString*)userId;

- (void)updateDiscoveryRules:(NSDictionary*)newRulesDic completion:(void (^)(NSError *))completion;
- (void)updateDiscovery:(NSDictionary*)newDic userId:(NSString*)userId completion:(void (^)(NSError *))completion;
- (void)updateAchievements:(AchievementType)achievementType;
- (void)removeAllObservers;

//GeoFire
- (void)setUserLocation:(NSString*)userId location:(CLLocation*)location;
- (void)removeLocation:(NSString*)userId;
//- (void)getNearUsers:(CLLocation*)center completion:(void (^)(User *, CLLocation*, NSError *))completion;
- (void)getNearUsers:(CLLocation*)center radius:(double) radius completion:(void (^)(User *, CLLocation*, NSError *))completion;
- (void)getUsersByCountryFilter:(void (^)(FIRDataSnapshot *, NSError *))completion;
@end
