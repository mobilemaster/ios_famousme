//
//  DateUtils.h
//  FamousMe
//
//  Created by Shine Man on 12/15/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateUtils : NSObject

+ (NSInteger)getAge:(NSString*)dateString;
+ (NSString*)getGapOfTimesWithString:(double)timeStamp;
+ (int)getGapOfDate:(double)timestamp;
+ (NSString*)getTimeFromTimestamp:(double)timestamp;
+ (NSString*)getGapOfDateWithString:(double)timestamp;
+ (NSString*)getDateFromTimestamp:(double)timestamp;
+ (NSString*)getMonthAndDayFromTimestamp:(double)timestamp;
@end
