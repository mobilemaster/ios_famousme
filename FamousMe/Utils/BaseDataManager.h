//
//  BaseDataManager.h
//  FamousMe
//
//  Created by Shine Man on 12/12/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Discovery.h"
#import "User.h"
#import "Contact.h"

@interface BaseDataManager : NSObject

+ (instancetype)sharedManager;

- (NSMutableArray*)getDiscoveryArray;
- (void)addDiscovery:(Discovery*)discovery;
- (void)setNewNotifications:(int)notifications;
- (int)getNewNotifications;
- (void)addNewNotification;
- (void)setConversationCount:(int)newCount;
- (int)getConversationCount;
- (NSMutableArray*)getContactArray;
- (void)addContact:(Contact*)contact;
- (void)removeAllData;

+ (BOOL)hasConversations;
+ (BOOL)isDuplicatedDiscovery:(NSString*)userKey;
+ (BOOL)isDuplicatedContact:(NSString*)userKey;
+ (NSArray*)getInterestList;
+ (NSArray*)getNationalityList;
+ (NSArray*)getServiceList;
+ (NSArray*)getDiscoveryRangeList;
+ (NSArray*)getOnlineFilterList;
+ (NSDictionary*)getCountryData;
+ (NSString*)getCountryCodeByLocaleCode;
+ (NSString*)getCountryNameByCode:(NSString*)code;
+ (NSInteger)getInterestIndex:(NSString*)inInterest;
+ (NSInteger)getNationalityIndex:(NSString*)inNationality;
+ (int)getDiscoveryRangeValue;
+ (BOOL)checkValidUser:(User*)user;
+ (NSString*)getDistanceString:(int)distance;

+ (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize;
@end
