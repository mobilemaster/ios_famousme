//
//  AlertViewManager.h
//  FamousMe
//
//  Created by Shine Man on 16/07/18.
//  Copyright © 2016 Shine Man. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ThemeManager : NSObject

+ (instancetype)sharedManager;

+ (void)setThemeType:(ThemeType)themeType;
+ (ThemeType)getThemeType;
+ (UIColor*)getCurrentThemeColor;
+ (UIColor*)getThemeColor:(ThemeType)themeType;

+ (void)setThemeWithChildViews:(UIView*)view;
+ (void)setTheme:(UIView*)view;

+ (UIImage*)getMapLoadingIcon;
@end
