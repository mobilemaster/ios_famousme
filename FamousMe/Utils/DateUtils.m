//
//  DateUtils.m
//  FamousMe
//
//  Created by Shine Man on 12/15/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "DateUtils.h"

@implementation DateUtils

/**
 * Get Age with string.
 * @return int
 */
+ (NSInteger)getAge:(NSString*)dateString {
    if (dateString.length == 0)
        return 0;
    
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"MM/dd/yyyy"];
	NSDate *dateFromString = [[NSDate alloc] init];
	dateFromString = [dateFormatter dateFromString:dateString];
	
	NSDate* now = [NSDate date];
	NSDateComponents* ageComponents = [[NSCalendar currentCalendar]
									   components:NSCalendarUnitYear
									   fromDate:dateFromString
									   toDate:now
									   options:0];
	NSInteger age = [ageComponents year];
	
	return age;
}


/**
 * Get gap of between twe times with string.
 * @param timeStamp The time stamp which have to be compared.
 * @return String
 */
+ (NSString*)getGapOfTimesWithString:(double)timeStamp {
	// Calculate difference in milliseconds
	double diff = fabs(([[NSDate date] timeIntervalSince1970] * 1000) - timeStamp);
	
	NSString* gapString = @"";
	if (diff < 24 * 60 * 60 * 1000) {
		if (diff < 60 * 60 * 1000) {// minutes
			if (diff < 60 * 1000)
				gapString = @"now";
			else
				gapString = [NSString stringWithFormat:@"%d %@", (int) (diff / (60 * 1000)), @"minitues ago"];
		}
		else //hours
			gapString = [NSString stringWithFormat:@"%d %@", (int) (diff / (60 * 60 * 1000)), @"hours ago"];
		
	} else { //Day
		gapString = [NSString stringWithFormat:@"%d %@", (int)(diff / (24 * 60 * 60 * 1000)), @"days ago"];
	}
	
	return gapString;
}

/**
 * Get gap of date
 */
+ (int)getGapOfDate:(double)timestamp {
	double diff = fabs(([[NSDate date] timeIntervalSince1970] * 1000) - timestamp);
	
	int gap = 0;
	if (diff > 24 * 60 * 60 * 1000)
		gap = (int)(diff / (24 * 60 * 60 * 1000));
	
	return gap;
}

/**
 * Get gap of date with string
 */
+ (NSString*)getGapOfDateWithString:(double)timestamp {
	double diff = fabs(([[NSDate date] timeIntervalSince1970] * 1000) - timestamp);
	
	NSString* gapString = @"";
	if (diff < 24 * 60 * 60 * 1000) {
		gapString = @"TODAY";
	} else {
		int days = (int)(diff / (24 * 60 * 60 * 1000));
		if (days == 1)
			gapString = @"Yesterday";
		else if(days > 1 && days < 5)
			gapString = [NSString stringWithFormat:@"%d days ago", days];
		else
			gapString = [DateUtils getDateFromTimestamp:timestamp];
	}
	
	return gapString;
}

/**
 * Get Time string from timestamp
 * @param timestamp which have to be converted.
 * @return String
 */
+ (NSString*)getTimeFromTimestamp:(double)timestamp {
	NSString* timeStampString = [NSString stringWithFormat:@"%f", timestamp];
	NSDate *date = [NSDate dateWithTimeIntervalSince1970:[timeStampString doubleValue]/1000.0];
	NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
	[formatter setDateFormat:@"hh:mm"];
	
	return [formatter stringFromDate:date];
}

/**
 * Timestamp to String.
 */
+ (NSString*)getDateFromTimestamp:(double)timestamp {
	NSString* timeStampString = [NSString stringWithFormat:@"%f", timestamp];
	NSDate *date = [NSDate dateWithTimeIntervalSince1970:[timeStampString doubleValue]/1000.0];
	NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
	[formatter setDateFormat:@"MM/dd/yyyy"];
	
	return [formatter stringFromDate:date];
}

/**
 * Get date string from timestamp like Aug 29
 */
+ (NSString*)getMonthAndDayFromTimestamp:(double)timestamp {
	NSString* timeStampString = [NSString stringWithFormat:@"%f", timestamp];
	
	NSDate *date = [NSDate dateWithTimeIntervalSince1970:[timeStampString doubleValue]/1000.0];
	NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
	[formatter setDateFormat:@"MMM dd"];
	
	return [formatter stringFromDate:date];
}
@end
