//
//  AlertViewManager.h
//  FamousMe
//
//  Created by abc on 11/10/16.
//  Copyright © 2016 Shine Man. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface AlertViewManager : NSObject

+ (UIAlertController*)getAlertView:(NSString*)title msg:(NSString*)msg;
+ (void)showAlertView:(NSString*)title msg:(NSString*)msg parent:(UIViewController*)parent;
@end
