//
//  AlertViewManager.m
//  FamousMe
//
//  Created by Shine Man on 16/07/18.
//  Copyright © 2016 Shine Man. All rights reserved.
//


#import "ThemeManager.h"

@interface ThemeManager() {
    //ThemeType currentThemeType;
}
@end

@implementation ThemeManager

+ (instancetype)sharedManager {
    static ThemeManager *sharedInstance = nil;
    static dispatch_once_t pred;
    
    dispatch_once(&pred, ^{
        sharedInstance = [[ThemeManager alloc] init];
    });
    
    return sharedInstance;
}

- (instancetype)init {
    self = [super init];
    if (self) {
    }
    
    return self;
}

+ (void)setThemeType:(ThemeType)themeType {
    [[NSUserDefaults standardUserDefaults] setInteger:themeType forKey:@"ThemeType"];
}

+ (ThemeType)getThemeType {
    id themeValue = [[NSUserDefaults standardUserDefaults] valueForKey:@"ThemeType"];    
    return themeValue != nil? [themeValue intValue]: SalmonColor;
}

+ (UIColor*)getCurrentThemeColor {
    return [ThemeManager getThemeColor:[ThemeManager getThemeType]];
}

+ (UIColor*)getThemeColor:(ThemeType)themeType {
    switch (themeType) {
        case SalmonColor:
            return SALMON_COLOR;
        case MediumGreenColor:
            return MEDIUM_GREEN_COLOR;
        case HotPinkColor:
            return HOT_PINK_COLOR;
        case BlueColor:
            return BLUE_COLOR;
        case DarkIndigoColor:
            return DARK_INDIGO_COLOR;
        case DarkSalmonColor:
            return DARK_SALMON_COLOR;
        case BlueVioletColor:
            return BLUE_VIOLET_COLOR;
        case SlateGrayColor:
            return SLATE_GRAY_COLOR;
        case RoyalBlueColor:
            return ROYAL_BLUE_COLOR;
        default:
            break;
    }
    
    return SALMON_COLOR;
}

+ (UIImage*)getMapLoadingIcon {
    ThemeType themeType = [ThemeManager getThemeType];
    switch (themeType) {
        case SalmonColor:
            return [UIImage imageNamed:@"map-loading-salmon"];
        case MediumGreenColor:
            return [UIImage imageNamed:@"map-loading"];
        case HotPinkColor:
            return [UIImage imageNamed:@"map-loading-pink"];
        case BlueColor:
            return [UIImage imageNamed:@"map-loading-blue"];
        case DarkIndigoColor:
            return [UIImage imageNamed:@"map-loading-indigo"];
        case DarkSalmonColor:
            return [UIImage imageNamed:@"map-loading-dark-salmon"];
        case BlueVioletColor:
            return [UIImage imageNamed:@"map-loading-blue-violet"];
        case SlateGrayColor:
            return [UIImage imageNamed:@"map-loading-slate-gray"];
        case RoyalBlueColor:
            return [UIImage imageNamed:@"map-loading-royal-blue"];
        default:
            break;
    }
    
    return [UIImage imageNamed:@"map-loading-salmon"];
}

+ (void)setThemeWithChildViews:(UIView*)view {
    view.backgroundColor = [ThemeManager getCurrentThemeColor];
    NSArray *subviews = view.subviews;
    for (UIView *childView in subviews) {
        childView.backgroundColor = [ThemeManager getCurrentThemeColor];
    }
}

+ (void)setTheme:(UIView*)view {
    view.backgroundColor = [ThemeManager getCurrentThemeColor];
}
@end
