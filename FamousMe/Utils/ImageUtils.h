//
//  ImageUtils.h
//  FamousMe
//
//  Created by Shine Man on 1/15/18.
//  Copyright © 2018 Shine Man. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageUtils : NSObject
+ (UIImage*)imageWithImage:(UIImage*)sourceImage scaledToWidth:(float)i_width;
@end
