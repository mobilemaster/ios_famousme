//
//  AddSocialViewController.h
//  FamousMe
//
//  Created by Shine Man on 11/24/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaddingTextField.h"
@interface AddSocialViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIScrollView *scrScrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *headerView;

@property (weak, nonatomic) IBOutlet PaddingTextField *tfWebsite;
@property (weak, nonatomic) IBOutlet PaddingTextField *tfFacebook;
@property (weak, nonatomic) IBOutlet PaddingTextField *tfSnapchat;
@property (weak, nonatomic) IBOutlet PaddingTextField *tfLinkedin;
@property (weak, nonatomic) IBOutlet PaddingTextField *tfTwitter;
@property (weak, nonatomic) IBOutlet PaddingTextField *tfInstagram;
@property (weak, nonatomic) IBOutlet PaddingTextField *tfYoutube;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;

@end
