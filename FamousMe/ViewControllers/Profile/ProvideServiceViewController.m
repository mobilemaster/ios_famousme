//
//  ProvideServiceViewController.m
//  FamousMe
//
//  Created by Shine Man on 11/24/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "ProvideServiceViewController.h"

@interface ProvideServiceViewController () <UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate>{

	UIPickerView* servicePicker;
	
	NSArray* serviceArray;
}
@end

@implementation ProvideServiceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	
    [self applyTheme];
	serviceArray = [BaseDataManager getServiceList];
	[self setUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)applyTheme {
    [ThemeManager setThemeWithChildViews:_headerView];
    [ThemeManager setTheme:_btnSubmit];
}

- (void)setUI {
	User* user = [FirebaseManager sharedManager].loginedUser;
	
	servicePicker = [[UIPickerView alloc] init];
	servicePicker.delegate = self;
	servicePicker.dataSource = self;
	self.tfSelectService.inputView = servicePicker;
	self.tfSelectService.delegate = self;

	self.tfSelectService.text = user.serviceTitle;
	self.tfDescription.text = user.serviceDetails;
}

/**
 * Save service data
 */
- (void)submitServiceData {
	NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
	[dict setValue:self.tfSelectService.text forKey:@"serviceTitle"];
	[dict setValue:self.tfDescription.text forKey:@"serviceDetails"];
	
	[[FirebaseManager sharedManager] updateService:dict];
}

#pragma mark - UITextFieldDelegate
- (void)textFieldDidEndEditing:(UITextField *)textField {
	NSInteger row = [servicePicker selectedRowInComponent:0];
	self.tfSelectService.text = [serviceArray objectAtIndex:row];
	[self.tfSelectService resignFirstResponder];
}


#pragma mark - UIPickerViewDelegate & UIPickerViewDataSource
// The number of columns of data
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
	return 1;
}

// The number of rows of data
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
	
	return serviceArray.count;
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
	return serviceArray[row];
}

#pragma mark IBAction
- (IBAction)onBack:(id)sender {
	CATransition *transition = [CATransition animation];
	transition.duration = 0.3;
	transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
	transition.type = kCATransitionPush;
	transition.subtype = kCATransitionFromLeft;
	[self.view.window.layer addAnimation:transition forKey:nil];
	[self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)onSubmit:(id)sender {
	[self submitServiceData];
	[self onBack:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
