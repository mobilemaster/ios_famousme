//
//  UserProfileViewController.m
//  FamousMe
//
//  Created by Shine Man on 11/22/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "UserProfileViewController.h"
#import "ChatViewController.h"
#import "PhotoViewController.h"
#import "DateUtils.h"
#import "MBProgressHUD.h"

@import Toast;

@interface UserProfileViewController () {
	NSString* currentUserId;
	User* currentUser;
	
	BOOL isFromChat;
	BOOL shouldCheckUpdate;
	NSString* status;
	NSString* orgUsername;
	NSString* orgPhotoUrl;
	NSString* orgProfession;
    
	BOOL isUpdated;
	
	int achievementPercent;
}

@end

@implementation UserProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	
	achievementPercent = 0;
	_ivProfilePhoto.layer.cornerRadius = _ivProfilePhoto.frame.size.height / 2;
	[_ivProfilePhoto setClipsToBounds:YES];
	_vAchievementFrame.layer.cornerRadius = _vAchievementFrame.frame.size.height / 2;
	[_vAchievementFrame setClipsToBounds:YES];
	_btnAchievement.layer.cornerRadius = _btnAchievement.frame.size.height / 2;
	[_btnAchievement setClipsToBounds:YES];
	
	[self.ivProfilePhoto setUserInteractionEnabled:YES];
	UITapGestureRecognizer* photoTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleProfilePhotoTap:)];
	[self.ivProfilePhoto addGestureRecognizer:photoTap];
	
	isUpdated = NO;
    
    [self applyTheme];    
	[self getUserInfo];
}

- (void)viewDidDisappear:(BOOL)animated {
	[super viewDidDisappear:animated];
	
	NSMutableDictionary* userInfo = [[NSMutableDictionary alloc] init];
	[userInfo setValue:[NSNumber numberWithBool:isUpdated] forKey:@"isUpdated"];
	
	if (isUpdated) {
		[userInfo setValue:currentUser.key forKey:@"userKey"];
		[userInfo setValue:currentUser.userProfile.photoUrl forKey:@"photoUrl"];
        [userInfo setValue:currentUser.userProfile.avatarUrl forKey:@"avatarUrl"];
		[userInfo setValue:currentUser.userProfile.profession forKey:@"profession"];
		[userInfo setValue:currentUser.userProfile.username forKey:@"username"];
	}
	
	[[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_USERINFO_CHANGED object:userInfo];
}

- (void)applyTheme {
    [ThemeManager setThemeWithChildViews:_headerView];
    [ThemeManager setTheme:_btnMessage];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setBaseData:(NSDictionary*)baseDict {	
	if ([baseDict objectForKey:@"shouldCheckUpdate"])
		shouldCheckUpdate = [[baseDict objectForKey:@"shouldCheckUpdate"] boolValue];
	
	if ([baseDict objectForKey:@"isFromChat"])
		isFromChat = [[baseDict objectForKey:@"isFromChat"] boolValue];
	
	currentUserId = [baseDict objectForKey:@"userId"];
    if ([baseDict objectForKey:@"profession"])
        orgProfession = [baseDict objectForKey:@"profession"];
    
    if ([baseDict objectForKey:@"username"])
        orgUsername = [baseDict objectForKey:@"username"];
    
    if ([baseDict objectForKey:@"photoUrl"])
        orgPhotoUrl = [baseDict objectForKey:@"photoUrl"];
    
    if ([baseDict objectForKey:@"status"])
        status = [baseDict objectForKey:@"status"];
}

/**
 * Set UI with current user info
 */
- (void)setUI {
	
	[FirebaseManager showImageWithUrl:self.ivProfilePhoto imagePath:currentUser.userProfile.photoUrl];
	
	self.lbTitle.text = currentUser.userProfile.username;
	self.lbProfessionTitle.text = currentUser.userProfile.profession;
	self.lbProfessionValue.text = currentUser.userProfile.profession;
	self.lbEmail.text = currentUser.emailAddress;
	
	if (currentUser.discoveryRules.canSeeMyAge)
		self.lbDateOfBirth.text = currentUser.dateOfBirth;
	else
		self.lbDateOfBirth.text = @"";

	self.lbNatonality.text = currentUser.nationality;
	if (currentUser.discoveryRules.showPhoneNumber)
		self.lbPhoneNumber.text = currentUser.phoneNumber;
	else
		self.lbPhoneNumber.text = @"";

	if (currentUser.status.isOnline) {
		self.lbOnlineStatus.text = @"online now";
		self.lbOnlineStatus.textColor = MAIN_COLOR;
	} else {
		self.lbOnlineStatus.text = [DateUtils getGapOfTimesWithString:currentUser.status.timestamp];
		self.lbOnlineStatus.textColor = CONTENT_TEXT_COLOR;
	}
	
	if (currentUser.serviceTitle.length == 0) {
		self.lbServiceName.text = @"No added service!";
		self.lbServiceDetails.text = @"";
	} else {
		self.lbServiceName.text = currentUser.serviceTitle;
		self.lbServiceDetails.text = currentUser.serviceDetails;
	}

    if (status.length > 0)
        self.lbDistanceStatus.text = status;
    
	[currentUser.interests stringByReplacingOccurrencesOfString:@"/" withString:@"\n"];
	NSString* interestString = [currentUser.interests stringByReplacingOccurrencesOfString:@"/" withString:@"\n"];
	self.lbInterests.text = interestString;
}

- (void)getUserInfo {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
	[[FirebaseManager sharedManager] getUser:currentUserId completion:^(User* user, NSError* error) {
		if (error || !user)
			return;
		
		if (user) {
			currentUser = user;
            [self setUI];
			[self getAchievements];			
			[MBProgressHUD hideHUDForView:self.view animated:YES];
			if ([self isUpdatedProfile]) {
				isUpdated = YES;
				[self.view makeToast:@"The profile was already updated by the user."];
				
			}
		} else
			[AlertViewManager showAlertView:@"Error" msg:@"can't get the user's info" parent:self];
	}];
}

/**
 * Check whether the user's profile was updated or not.
 * @return result.
 */
- (BOOL)isUpdatedProfile {
	if (orgUsername.length > 0 &&
		![orgUsername isEqualToString:currentUser.userProfile.username]) {
		return YES;
	}
	
	if (orgPhotoUrl.length > 0 && ![orgPhotoUrl isEqualToString:currentUser.userProfile.photoUrl]) {
		return YES;
	}

	if (orgProfession.length > 0 && ![orgProfession isEqualToString:currentUser.userProfile.profession]) {
		return YES;
	}
	
	return NO;
}

- (void)getAchievements {
	[[FirebaseManager sharedManager] getAchievements:currentUser.key completion:^(Achievement* achievement, NSError* error) {
		achievementPercent = achievementPercent + achievement.value;
		[self.btnAchievement setTitle:[NSString stringWithFormat:@"%d%@", achievementPercent, @"%"] forState:UIControlStateNormal];
	}];
}

- (void)handleProfilePhotoTap:(UITapGestureRecognizer *)recognizer {
	PhotoViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PhotoViewController"];
	[vc setPhotoUrl:currentUser.userProfile.photoUrl];
	[self presentViewController:vc animated:YES completion:nil];
}

#pragma mark IBAction
- (IBAction)onBack:(id)sender {
	/*CATransition *transition = [CATransition animation];
	transition.duration = 0.3;
	transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
	transition.type = kCATransitionPush;
	transition.subtype = kCATransitionFromLeft;
	[self.view.window.layer addAnimation:transition forKey:nil];*/
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)onShowMessage:(id)sender {
    if (isFromChat) {
        [self onBack:nil];
    } else {
        ChatViewController* svc = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatViewController"];
        
        NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
        [dict setValue:currentUser.key forKey:@"userId"];
        [dict setValue:[NSNumber numberWithBool:currentUser.userProfile.isValid] forKey:@"isValid"];
        [dict setValue:currentUser.userProfile.username forKey:@"username"];
        [dict setValue:@YES forKey:@"isFromProfile"];
        [svc setBaseData:dict];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goToMap:) name:NOTIFICATION_GOTO_MAP object:nil];
        
        [self presentViewController:svc animated:NO completion:nil];
    }
}

- (void)goToMap:(NSNotification*)data {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_GOTO_MAP object:nil];
    dispatch_async(dispatch_get_main_queue(), ^{
        // Your code to run on the main queue/thread
        [self dismissViewControllerAnimated:YES completion:nil];
    });
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
