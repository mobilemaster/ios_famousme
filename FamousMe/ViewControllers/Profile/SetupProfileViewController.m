//
//  SetupProfileViewController.m
//  FamousMe
//
//  Created by Shine Man on 11/20/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "SetupProfileViewController.h"
#import "InterestTableViewCell.h"
#import "User.h"
#import "FirebaseManager.h"

//Filtering
#import "IFInkwellFilter.h"
#import "IF1977Filter.h"
#import "IFXproIIFilter.h"
#import "IFEarlybirdFilter.h"
#import "IFToasterFilter.h"
#import "IFRiseFilter.h"
#import "IFBrannanFilter.h"
#import "IFSierraFilter.h"

#import "GPUImage.h"

@import Toast;

@interface SetupProfileViewController () <UITextFieldDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource> {
	UIDatePicker* datePicker;
	UIPickerView* genderPicker;
	UIPickerView* nationalityPicker;
	NSUInteger currentStep;
	UIImage* userImage;
    UIImage* avatarImage;
	
	NSArray* genderArray;
	NSArray* nationalityArray;
	NSArray* interestArray;
	NSMutableArray* selectedInterestArray;
    
    //Filters
    NSMutableArray* filterViewArray;
    NSMutableArray* filterArray;
    NSMutableArray* filterImageArray;
}

@property (strong, nonatomic) NSMutableArray* interestItems;
@property (strong, nonatomic) NSMutableArray* currentInterestItems;

@property (nonatomic) User* user;
@end

@implementation SetupProfileViewController
@synthesize user;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	
	self.interestItems = [NSMutableArray array];
	self.currentInterestItems = [NSMutableArray array];
	self.tbInterests.delegate = self;
	self.tbInterests.dataSource = self;

	genderPicker = [[UIPickerView alloc] init];
	genderPicker.delegate = self;
	genderPicker.dataSource = self;
	genderPicker.tag = 2001;
	self.tfGender.inputView = genderPicker;
	self.tfGender.delegate = self;
	self.tfGender.tag = 1001;
	genderArray = @[@"Male", @"Female"];
	
	datePicker = [[UIDatePicker alloc] init];
	datePicker.datePickerMode = UIDatePickerModeDate;
	self.tfDateOfBirth.inputView = datePicker;
	self.tfDateOfBirth.delegate = self;
	self.tfDateOfBirth.tag = 1002;

	nationalityPicker = [[UIPickerView alloc] init];
	nationalityPicker.delegate = self;
	nationalityPicker.dataSource = self;
	nationalityPicker.tag = 2003;
	self.tfNationality.inputView = nationalityPicker;
	self.tfNationality.delegate = self;
	self.tfNationality.tag = 1003;
	nationalityArray = [BaseDataManager getNationalityList];

	interestArray = [BaseDataManager getInterestList];
	selectedInterestArray = [NSMutableArray array];
	currentStep = 1;
	userImage = nil;
    
    //Filtering
    filterViewArray = [NSMutableArray array];
    filterArray = [NSMutableArray array];
    filterImageArray = [NSMutableArray array];
    
    [filterViewArray addObject:self.givFilter1];
    [filterViewArray addObject:self.givFilter2];
    [filterViewArray addObject:self.givFilter3];
    [filterViewArray addObject:self.givFilter4];
    [filterViewArray addObject:self.givFilter5];
    [filterViewArray addObject:self.givFilter6];
    [filterViewArray addObject:self.givFilter7];
    [filterViewArray addObject:self.givFilter8];
    
	if (!self.user)
		self.user = [[User alloc] init];
    
    if ([UIScreen mainScreen].bounds.size.height < 500) {

    }
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	[self setUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setUI {
	self.vPhotoFrame.layer.cornerRadius = self.vPhotoFrame.frame.size.height/2;
	self.ivProfilePhoto.layer.cornerRadius = self.ivProfilePhoto.frame.size.height/2;
	self.ivProfilePhoto.clipsToBounds = YES;
	
    //Filtering
    for (UIView* view in filterViewArray) {
        UITapGestureRecognizer* photoTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleFilterImage:)];
        [view addGestureRecognizer:photoTap];
        
        view.layer.cornerRadius = view.frame.size.height/2;
        view.clipsToBounds = YES;
    }
    
	UITapGestureRecognizer* photoTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleProfilePhotoTap:)];
	[self.vPhotoFrame addGestureRecognizer:photoTap];
}

- (void)setCountryCode:(NSString*)countryCode {
	if (!self.user)
		self.user = [[User alloc] init];
	
	self.user.countryCode = countryCode;
}

//-------------------------- START First Step ----------------------------/
- (void)handleProfilePhotoTap:(UITapGestureRecognizer *)tap {
	UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:@"Take a Photo" preferredStyle:UIAlertControllerStyleActionSheet];
	
	//Cancel
	[actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
		[actionSheet dismissViewControllerAnimated:YES completion:nil];
	}]];
	
	[actionSheet addAction:[UIAlertAction actionWithTitle:@"Use Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
		[self takePhoto];
	}]];
	
	[actionSheet addAction:[UIAlertAction actionWithTitle:@"Photo Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
		[self selectPhoto];
	}]];
	
	// Present action sheet.
	[self presentViewController:actionSheet animated:YES completion:nil];
}

- (void)takePhoto {
	UIImagePickerController* picker = [[UIImagePickerController alloc] init];
	picker.delegate = self;
	picker.allowsEditing = YES;
	picker.sourceType = UIImagePickerControllerSourceTypeCamera;
	
	[self presentViewController:picker animated:YES completion:NULL];
}

- (void)selectPhoto {
	UIImagePickerController *picker = [[UIImagePickerController alloc] init];
	picker.delegate = self;
	picker.allowsEditing = YES;
	picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
	
	[self presentViewController:picker animated:YES completion:NULL];
}

- (BOOL)isValidFirstStep {
	if ([self.tfFullName isEmpty]) {
		[self.view makeToast:@"The full name field should be not empty."];
		return NO;
	}
	
	if (userImage == nil) {
		[self.view makeToast:@"You should add an image"];
		return NO;
	}
	
	return YES;
}

- (void)handleFilterImage:(UITapGestureRecognizer *)recognizer {
    NSInteger seletedIndex = recognizer.view.tag;
    for (UIView* view in filterViewArray) {
        if (view.tag == seletedIndex) {
            view.layer.borderColor = MAIN_COLOR.CGColor;
            view.layer.borderWidth = 3.0f;
            
            UIImage* filterImage = [filterImageArray objectAtIndex:seletedIndex];
            self.ivProfilePhoto.image = filterImage;
        } else {
            view.layer.borderColor = [UIColor clearColor].CGColor;
            view.layer.borderWidth = 0.0f;
        }
    }
}

- (void)addFilter {
    self.vgAddFilterViewGroup.hidden = YES;
    userImage = self.ivProfilePhoto.image;
    for (UIView* view in filterViewArray) {
        view.layer.borderColor = [UIColor clearColor].CGColor;
        view.layer.borderWidth = 0.0f;
    }
}

- (void)closeFilter {
    self.ivProfilePhoto.image = userImage;
    self.vgAddFilterViewGroup.hidden = YES;
    for (UIView* view in filterViewArray) {
        view.layer.borderColor = [UIColor clearColor].CGColor;
        view.layer.borderWidth = 0.0f;
    }
}

- (void)setFilterImages {
    self.vgAddFilterViewGroup.hidden = NO;
    
    [filterArray removeAllObjects];
    [filterImageArray removeAllObjects];
    
    GPUImagePicture *stillImageSource;
    
    stillImageSource = [[GPUImagePicture alloc] initWithImage:userImage];
    NSLog(@"original Image = %f:%f", userImage.size.width, userImage.size.height);
    
    IFInkwellFilter *imageFilter1 = [[IFInkwellFilter alloc] init];
    IFToasterFilter *imageFilter2 = [[IFToasterFilter alloc] init];
    IFXproIIFilter *imageFilter3 = [[IFXproIIFilter alloc] init];
    IFEarlybirdFilter *imageFilter4 = [[IFEarlybirdFilter alloc] init];
    IF1977Filter *imageFilter5 = [[IF1977Filter alloc] init];
    IFBrannanFilter* imageFilter6 = [[IFBrannanFilter alloc] init];
    IFSierraFilter* imageFilter7 = [[IFSierraFilter alloc] init];
    IFRiseFilter* imageFilter8 = [[IFRiseFilter alloc] init];
    
    [filterArray addObject:imageFilter1];
    [filterArray addObject:imageFilter2];
    [filterArray addObject:imageFilter3];
    [filterArray addObject:imageFilter4];
    [filterArray addObject:imageFilter5];
    [filterArray addObject:imageFilter6];
    [filterArray addObject:imageFilter7];
    [filterArray addObject:imageFilter8];
    
    for (NSInteger index = 0; index < filterArray.count; index++) {
        [stillImageSource removeAllTargets]; //Clear current frame buffer.
        IFImageFilter* imageFilter = [filterArray objectAtIndex:index];
        GPUImageView* imageView = [filterViewArray objectAtIndex:index];
        [imageFilter addTarget:imageView];
        [stillImageSource addTarget:imageFilter];
        [imageFilter useNextFrameForImageCapture];
        [stillImageSource processImage];
        UIImage* filterImage = [imageFilter imageFromCurrentFramebuffer];
        
        NSLog(@"filtered Image = %f:%f", filterImage.size.width, filterImage.size.height);
        [filterImageArray addObject:filterImage];
    }
}

//-------------------------- END First Step ---------------------------/

//-------------------------- START Second Step -------------------------/
- (BOOL)isValidSecondStep {
	if ([self.tfDateOfBirth isEmpty]) {
		[self.view makeToast:@"The Date of Birth field should be not empty."];
		return NO;
	}

	if ([self.tfProfession isEmpty]) {
		[self.view makeToast:@"The Profession field should be not empty."];
		return NO;
	}

	if ([self.tfEmailAddress isEmpty]) {
		[self.view makeToast:@"The Email Address field should be not empty."];
		return NO;
	}
	
	return YES;
}

//-------------------------- END Second Step ---------------------------/

//-------------------------- START Third Step -------------------------/
- (BOOL)isValidThirdStep {
	if (selectedInterestArray > 0)
		return YES;
	
	return NO;
}
//-------------------------- END Third Step ---------------------------/

- (void)setCurrentStep {
	switch (currentStep) {
		case 1:
			self.btnBack.hidden = YES;
			self.vgSecondStepViewGroup.hidden = YES;
			self.vgSecondStepViewGroup.hidden = YES;
			[self.lbTitle setText:@"Set up profile (1/3)"];
			break;
		case 2:
			self.btnBack.hidden = NO;
			self.vgSecondStepViewGroup.hidden = NO;
			self.vgThirdStepViewGroup.hidden = YES;
			[self.lbTitle setText:@"Set up profile (2/3)"];
			break;
		case 3:
			self.btnBack.hidden = NO;
			self.vgThirdStepViewGroup.hidden = NO;
			[self.lbTitle setText:@"Set up profile (3/3)"];
			break;

		default:
			break;
	}
}

- (void)saveUserData {
	[[FirebaseManager sharedManager] saveUserData:self.user profileImage:userImage completion:^(NSError* error) {
		if (error)
			[AlertViewManager showAlertView:@"Sorry." msg:error.localizedDescription parent:self];
		else {
			[[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"completedRegister"];
			[self performSegueWithIdentifier:@"SetupToMain" sender:self];
		}
	}];
}

#pragma mark - UIPickerViewDelegate & UIPickerViewDataSource
// The number of columns of data
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
	return 1;
}

// The number of rows of data
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
	if (pickerView.tag == 2001)
		return genderArray.count;
	else if (pickerView.tag == 2003)
		return nationalityArray.count;
	
	return 0;
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
	if (pickerView.tag == 2001)
		return genderArray[row];
	else if (pickerView.tag == 2003)
		return nationalityArray[row];
	
	return nil;
}


#pragma mark - UITableViewDelegate & DataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 50;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex {
	return interestArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"InterestTableViewCell";
    InterestTableViewCell *cell = (InterestTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    NSString* interest = [interestArray objectAtIndex:indexPath.row];
    cell.lbName.text = [interestArray objectAtIndex:indexPath.row];;
    BOOL isOn = NO;
    for (NSString* selectedItem in selectedInterestArray) {
        if ([interest isEqualToString:selectedItem]) {
            isOn = YES;
            break;
        }
    }
    
    [cell.chCheckBox setTag:indexPath.row];
    [cell.chCheckBox setOn:isOn];
    cell.chCheckBox.delegate = self;
    
    return cell;
}

#pragma mark - BEMCheckBoxDelegate
- (void)didTapCheckBox:(BEMCheckBox*)checkBox {
	NSInteger index = checkBox.tag;
	NSString* interestName = [interestArray objectAtIndex:index];
	if (checkBox.on)
		[selectedInterestArray addObject:interestName];
	else
		[selectedInterestArray removeObject:interestName];
}

#pragma mark - UITextFieldDelegate
- (void)textFieldDidEndEditing:(UITextField *)textField {
	switch (textField.tag) {
		case 1001: {
			NSInteger row = [genderPicker selectedRowInComponent:0];
			self.tfGender.text = [genderArray objectAtIndex:row];
			self.user.gender = (int)row;
		}
			break;
		case 1002: {
			NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
			[dateFormatter setDateFormat:@"MM/dd/yyyy"];
			
			self.tfDateOfBirth.text = [NSString stringWithFormat:@"%@", [dateFormatter stringFromDate:datePicker.date]];
			[self.tfDateOfBirth resignFirstResponder];
		}
			break;
		case 1003:{
			NSInteger row = [nationalityPicker selectedRowInComponent:0];
			self.tfNationality.text = [nationalityArray objectAtIndex:row];
		}
			
			break;
		default:
			break;
	}
}

#pragma mark - Image Picker Controller delegate methods
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
	UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
	self.ivProfilePhoto.image = chosenImage;
	userImage = chosenImage;
    //Filtering
    [self setFilterImages];
	[picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
	
	[picker dismissViewControllerAnimated:YES completion:NULL];
}


#pragma mark - IBAction
- (IBAction)onTouchBack:(id)sender {
	if (currentStep == 1)
		return;
	
	currentStep--;
	[self setCurrentStep];
}

- (IBAction)onAddFilter:(id)sender {
    //[self addFilter];
}

- (IBAction)onCloseFilter:(id)sender {
    [self closeFilter];
}

- (IBAction)onFirstStepContinue:(id)sender {
	if ([self isValidFirstStep]) {
		if (!self.user.userProfile)
			self.user.userProfile = [[UserProfile alloc] init];
        
        [self addFilter];
        self.user.userProfile.isValid = YES;
        self.user.userProfile.deletedTimestamp = 0;
		self.user.userProfile.username = self.tfFullName.text;
		currentStep = 2;
		[self setCurrentStep];
	}
}

- (IBAction)onSecondStepContinue:(id)sender {
	if ([self isValidSecondStep]) {
		currentStep = 3;
		self.user.dateOfBirth = self.tfDateOfBirth.text;
		self.user.phoneNumber = [FirebaseManager getPhoneNumber];
		self.user.emailAddress = self.tfEmailAddress.text;
		self.user.userProfile.profession = self.tfProfession.text;
		self.user.nationality = self.tfNationality.text;
		self.user.isUpgraded = NO;
		self.user.isAddedSocial = NO;
		self.user.key = [FirebaseManager getCurrentUserID];
		
		self.user.serviceTitle = @"";
		self.user.serviceDetails = @"";
		
		if (self.user.discoveryRules == nil)
			self.user.discoveryRules = [[DiscoveryRules alloc] init];

		//Initialize Discovery rules
		self.user.discoveryRules.discoverNationality = @"All";
		self.user.discoveryRules.discoveryInterest = @"All";
		
		if (self.user.social == nil)
			self.user.social = [[Social alloc] init];
		
		self.user.token = @"";
		
		[self setCurrentStep];
	}
}

- (IBAction)onSubmit:(id)sender {
	if ([self isValidThirdStep]) {
		NSString* interestData = nil;
		for (int i = 0; i < selectedInterestArray.count; i++) {
			NSString* interest = selectedInterestArray[i];
			if (!interestData)
				interestData = interest;
			else
				interestData = [NSString stringWithFormat:@"%@/%@", interestData, interest];
		}
		
		self.user.interests = interestData;
		[self saveUserData];
	}
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
