//
//  PhotoViewController.h
//  FamousMe
//
//  Created by Shine Man on 12/20/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *ivPhotoView;

- (void)setPhotoUrl:(NSString *)photoUrl;

@end
