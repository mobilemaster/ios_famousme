//
//  EditProfileViewController.h
//  FamousMe
//
//  Created by Shine Man on 11/22/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaddingTextField.h"
#import "BEMCheckBox.h"
#import "GPUImageView.h"

@interface EditProfileViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, BEMCheckBoxDelegate>

@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;

//First Step
@property (weak, nonatomic) IBOutlet UIView *vgFirstStepViewGroup;

@property (weak, nonatomic) IBOutlet UIView *vPhotoFrame;
@property (weak, nonatomic) IBOutlet UIImageView *ivProfilePhoto;
@property (weak, nonatomic) IBOutlet UIImageView *ivCloseView;
@property (weak, nonatomic) IBOutlet PaddingTextField *tfFullName;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIButton *btnContinue;

//Second Step
@property (weak, nonatomic) IBOutlet UIView *vgSecondStepViewGroup;
@property (weak, nonatomic) IBOutlet UIScrollView *scScrollView;

@property (weak, nonatomic) IBOutlet PaddingTextField *tfGender;

@property (weak, nonatomic) IBOutlet PaddingTextField *tfDateOfBirth;

@property (weak, nonatomic) IBOutlet PaddingTextField *tfNationality;


@property (weak, nonatomic) IBOutlet PaddingTextField *tfProfession;

@property (weak, nonatomic) IBOutlet PaddingTextField *tfEmailAddress;
@property (weak, nonatomic) IBOutlet UIButton *btnSecondContinue;

//Third Step
@property (weak, nonatomic) IBOutlet UIView *vgThirdStepViewGroup;

@property (weak, nonatomic) IBOutlet UITableView *tbInterests;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;

//Filters
@property (weak, nonatomic) IBOutlet UIView *vgAddFilterViewGroup;
@property (weak, nonatomic) IBOutlet GPUImageView *givFilter1;
@property (weak, nonatomic) IBOutlet GPUImageView *givFilter2;
@property (weak, nonatomic) IBOutlet GPUImageView *givFilter3;
@property (weak, nonatomic) IBOutlet GPUImageView *givFilter4;
@property (weak, nonatomic) IBOutlet GPUImageView *givFilter5;
@property (weak, nonatomic) IBOutlet GPUImageView *givFilter6;
@property (weak, nonatomic) IBOutlet GPUImageView *givFilter7;
@property (weak, nonatomic) IBOutlet GPUImageView *givFilter8;

@end
