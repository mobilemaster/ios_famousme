//
//  AddSocialViewController.m
//  FamousMe
//
//  Created by Shine Man on 11/24/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "AddSocialViewController.h"

@interface AddSocialViewController ()

@end

@implementation AddSocialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self applyTheme];
	[self setUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews {
	[super viewDidLayoutSubviews];
}

- (void)applyTheme {
    [ThemeManager setThemeWithChildViews:_headerView];
    [ThemeManager setTheme:_btnSave];
}

- (void)setUI {
	User* user = [FirebaseManager sharedManager].loginedUser;
	if (user.social == nil)
		return;
	
	if (user.social.website.length > 0)
		self.tfWebsite.text = user.social.website;
	
	if (user.social.facebook.length > 0)
		[self.tfFacebook setText:user.social.facebook];
	
	if (user.social.snapchat.length > 0)
		[self.tfSnapchat setText:user.social.snapchat];
	
	if (user.social.linkedin.length > 0)
		[self.tfLinkedin setText:user.social.linkedin];
	
	if (user.social.twitter.length > 0)
		[self.tfTwitter setText:user.social.twitter];
	
	if (user.social.instagram.length > 0)
		[self.tfInstagram setText:user.social.instagram];
	
	if (user.social.youtube.length > 0)
		[self.tfYoutube setText:user.social.youtube];
}

/**
 * Save data to server.
 */
- (void)submitSocialData {
	NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
	[dict setValue:self.tfWebsite.text forKey:@"website"];
	[dict setValue:self.tfFacebook.text forKey:@"facebook"];
	[dict setValue:self.tfSnapchat.text forKey:@"snapchat"];
	[dict setValue:self.tfLinkedin.text forKey:@"linkedin"];
	[dict setValue:self.tfTwitter.text forKey:@"twitter"];
	[dict setValue:self.tfInstagram.text forKey:@"instagram"];
	[dict setValue:self.tfYoutube.text forKey:@"youtube"];
	
	[[FirebaseManager sharedManager] updateUserSocial:dict];
}

- (IBAction)onBack:(id)sender {
	CATransition *transition = [CATransition animation];
	transition.duration = 0.3;
	transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
	transition.type = kCATransitionPush;
	transition.subtype = kCATransitionFromLeft;
	[self.view.window.layer addAnimation:transition forKey:nil];
	[self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)onSave:(id)sender {
	[self submitSocialData];
	[self onBack:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
