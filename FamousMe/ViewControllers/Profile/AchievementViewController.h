//
//  AchievementViewController.h
//  FamousMe
//
//  Created by Shine Man on 12/1/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProgressBar.h"

@interface AchievementViewController : UIViewController
@property (weak, nonatomic) IBOutlet ProgressBar *prProgress;
@property (weak, nonatomic) IBOutlet UIView *headerView;

@property (weak, nonatomic) IBOutlet UITableView *tbAchievements;
@end
