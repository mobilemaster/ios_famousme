//
//  MyProfileViewController.h
//  FamousMe
//
//  Created by Shine Man on 11/22/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyProfileViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIScrollView *scrScrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIImageView *ivProfilePhoto;
@property (weak, nonatomic) IBOutlet UIView *headerView;

@property (weak, nonatomic) IBOutlet UIView *vAchievementFrame;
@property (weak, nonatomic) IBOutlet UIButton *btnAchievement;
@property (weak, nonatomic) IBOutlet UIButton *btnEditService;

@property (weak, nonatomic) IBOutlet UILabel *lbUserName;
@property (weak, nonatomic) IBOutlet UILabel *lbProfessionTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbServiceName;
@property (weak, nonatomic) IBOutlet UILabel *lbServiceDetails;

@property (weak, nonatomic) IBOutlet UILabel *lbProfessionValue;
@property (weak, nonatomic) IBOutlet UILabel *lbPhoneNumber;
@property (weak, nonatomic) IBOutlet UILabel *lbEmail;
@property (weak, nonatomic) IBOutlet UILabel *lbDateOfBirth;
@property (weak, nonatomic) IBOutlet UILabel *lbNatonality;
@property (weak, nonatomic) IBOutlet UILabel *lbInterests;

@property (weak, nonatomic) IBOutlet UILabel *lbUpgradeTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbStatusDetails;

@property (weak, nonatomic) IBOutlet UIButton *btnUpgradeAction;

@property (weak, nonatomic) IBOutlet UIView *vgUpgradeView;
@property (weak, nonatomic) IBOutlet UIView *vgUpgradeMark;

@property (weak, nonatomic) IBOutlet UIView *vgSocial;
@property (weak, nonatomic) IBOutlet UIButton *btnEditProfile;

@end
