//
//  UserProfileViewController.h
//  FamousMe
//
//  Created by Shine Man on 11/22/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserProfileViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIScrollView *scrScrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIImageView *ivProfilePhoto;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UIView *headerView;

@property (weak, nonatomic) IBOutlet UIView *vAchievementFrame;
@property (weak, nonatomic) IBOutlet UIButton *btnAchievement;

@property (weak, nonatomic) IBOutlet UILabel *lbProfessionTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbDistanceStatus;
@property (weak, nonatomic) IBOutlet UILabel *lbOnlineStatus;

@property (weak, nonatomic) IBOutlet UILabel *lbServiceName;
@property (weak, nonatomic) IBOutlet UILabel *lbServiceDetails;

@property (weak, nonatomic) IBOutlet UILabel *lbProfessionValue;
@property (weak, nonatomic) IBOutlet UILabel *lbPhoneNumber;
@property (weak, nonatomic) IBOutlet UILabel *lbEmail;
@property (weak, nonatomic) IBOutlet UILabel *lbDateOfBirth;
@property (weak, nonatomic) IBOutlet UILabel *lbNatonality;
@property (weak, nonatomic) IBOutlet UILabel *lbInterests;
@property (weak, nonatomic) IBOutlet UIButton *btnMessage;

- (void)setBaseData:(NSDictionary*)baseData;
@end
