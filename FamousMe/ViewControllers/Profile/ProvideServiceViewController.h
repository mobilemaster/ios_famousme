//
//  ProvideServiceViewController.h
//  FamousMe
//
//  Created by Shine Man on 11/24/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaddingTextField.h"

@interface ProvideServiceViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet PaddingTextField *tfSelectService;

@property (weak, nonatomic) IBOutlet PaddingTextField *tfDescription;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;

@end
