//
//  PhotoViewController.m
//  FamousMe
//
//  Created by Shine Man on 12/20/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "PhotoViewController.h"

@interface PhotoViewController () {
	NSString* photoUrl;
}

@end

@implementation PhotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	
	[self.ivPhotoView setUserInteractionEnabled:YES];
	UITapGestureRecognizer* contactsTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapPhotoView:)];
	[self.ivPhotoView addGestureRecognizer:contactsTap];
	
	self.ivPhotoView.contentMode = UIViewContentModeScaleAspectFit;
	[FirebaseManager showImageWithUrl:self.ivPhotoView imagePath:photoUrl];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setPhotoUrl:(NSString *)url {
	photoUrl = url;
}

- (void)handleTapPhotoView:(id)sender {
	[self dismissViewControllerAnimated:YES completion:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
