//
//  MyProfileViewController.m
//  FamousMe
//
//  Created by Shine Man on 11/22/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "MyProfileViewController.h"
#import "ProvideServiceViewController.h"
#import "PhotoViewController.h"
#import "AddSocialViewController.h"
#import "AchievementViewController.h"
#import "EditProfileViewController.h"
#import "User.h"
#import "CopyRightViewController.h"

#import <FirebaseStorageUI/FirebaseStorageUI.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@import TwitterKit;
@import GoogleSignIn;

@interface MyProfileViewController () <GIDSignInDelegate, GIDSignInUIDelegate>{
	int upgradedState;
	int achievementPercent;
	
	NSString *verificationId;
}

@end

@implementation MyProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	
	_ivProfilePhoto.layer.cornerRadius = _ivProfilePhoto.frame.size.height / 2;
	_ivProfilePhoto.clipsToBounds = YES;
	_vAchievementFrame.layer.cornerRadius = _vAchievementFrame.frame.size.height / 2;
	_btnAchievement.layer.cornerRadius = _btnAchievement.frame.size.height / 2;
	
	_vgUpgradeMark.layer.cornerRadius = _vgUpgradeMark.frame.size.height / 2;
	_vgUpgradeMark.clipsToBounds = YES;
	
	[self.ivProfilePhoto setUserInteractionEnabled:YES];
	UITapGestureRecognizer* photoTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleProfilePhotoTap:)];
	[self.ivProfilePhoto addGestureRecognizer:photoTap];
	
	upgradedState = 0;
	achievementPercent = 0;   
	
    [self applyTheme];
	[self getAchievements];
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	
	[self setUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews {
	[super viewDidLayoutSubviews];
}

- (void)applyTheme {
    [ThemeManager setThemeWithChildViews:_headerView];
    [ThemeManager setTheme:_btnEditProfile];
    
    [_btnEditService setTitleColor:[ThemeManager getCurrentThemeColor] forState:UIControlStateNormal];
    [_btnUpgradeAction setTitleColor:[ThemeManager getCurrentThemeColor] forState:UIControlStateNormal];
}

- (void)setUI {
	User* user = [FirebaseManager sharedManager].loginedUser;
	
	if (![user.userProfile.photoUrl isEqualToString:@""]) {
        [FirebaseManager showImageWithUrl:self.ivProfilePhoto imagePath:user.userProfile.photoUrl];
	}
	
	[self.lbUserName setText:user.userProfile.username];
	self.lbProfessionTitle.text = user.userProfile.profession;
	self.lbProfessionValue.text = user.userProfile.profession;
	self.lbEmail.text = user.emailAddress;
	self.lbDateOfBirth.text = user.dateOfBirth;
	self.lbNatonality.text = user.nationality;
	self.lbPhoneNumber.text = user.phoneNumber;
	
	if ([user.serviceTitle length] == 0) {
		self.lbServiceName.text = @"No added service!";
		self.lbServiceDetails.text = @"";
	} else {
		self.lbServiceName.text = user.serviceTitle;
		self.lbServiceDetails.text = user.serviceDetails;
	}
	
	if (user.isUpgraded) {
		upgradedState = 1;
		[self setUpdateStatus];
	}
	
	if (user.isAddedSocial) {
		self.vgSocial.hidden = NO;
	}
	
	[user.interests stringByReplacingOccurrencesOfString:@"/" withString:@"\n"];
	NSString* interestString = [user.interests stringByReplacingOccurrencesOfString:@"/" withString:@"\n"];
	self.lbInterests.text = interestString;	
}

- (void)getAchievements {
	User* user = [FirebaseManager sharedManager].loginedUser;
	[[FirebaseManager sharedManager] getAchievements:user.key completion:^(Achievement* achievement, NSError* error) {
		achievementPercent = achievementPercent + achievement.value;
		[self.btnAchievement setTitle:[NSString stringWithFormat:@"%d%@", achievementPercent, @"%"] forState:UIControlStateNormal];
	}];
}

- (void)setUpdateStatus {
	if (upgradedState == 0) {
		
	} else if (upgradedState == 1) {
		self.lbUpgradeTitle.text = @"Upgraded!";
		self.lbStatusDetails.text = @"Add your social details to your profile";
		[self.btnUpgradeAction setTitle:@"Add" forState:UIControlStateNormal];
		
	} else {
		
	}
}

- (void)logout {
	NSError *signOutError;
	BOOL status = [[FIRAuth auth] signOut:&signOutError];
	if (!status) {
		NSLog(@"Error signing out: %@", signOutError);
		[AlertViewManager showAlertView:@"Sign out Error!" msg:signOutError.localizedDescription parent:self];
		return;
	}
	
	[[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"completedRegister"];
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_LOG_OUT object:self];
	[self dismissViewControllerAnimated:NO completion:^(void) {
		
	}];
}

- (void)deleteMyAccount {
	UIAlertController * alert = [UIAlertController
								 alertControllerWithTitle:@"Confirm delete account"
								 message:@"Enter in DELETE to delete your account"
								 preferredStyle:UIAlertControllerStyleAlert];
	
	UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
											   handler:^(UIAlertAction * action) {
												   //Do Some action here
												   NSArray * textfields = alert.textFields;
												   UITextField * tfCode = textfields[0];
												   NSString* deleteCode = [tfCode.text lowercaseString];
												   if ([deleteCode isEqualToString:@"delete"]) {
                                                       User* me = [FirebaseManager sharedManager].loginedUser;
                                                       if (me.logInSocialWith.length > 0) { //Social logged in.
                                                           if ([me.logInSocialWith isEqualToString:@"facebook"]) {
                                                               [self deleteAccountByAuthenticatedFacebook];
                                                           } else if ([me.logInSocialWith isEqualToString:@"twitter"]) {
                                                               [self deleteAccountByAuthenticatedTwitter];
                                                           } else if ([me.logInSocialWith isEqualToString:@"google"]) {
                                                               [GIDSignIn sharedInstance].uiDelegate = self;
                                                               [GIDSignIn sharedInstance].delegate = self;
                                                               [[GIDSignIn sharedInstance] signIn];                                                               
                                                           } else {
                                                               [AlertViewManager showAlertView:@"Unknown Error!" msg:@"Deleting social account" parent:self];
                                                           }
                                                           
                                                       } else { //Phone logged in.
                                                           [self resendPhoneNumberVerification];
                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                       }
												   }
												   
											   }];
	UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
												   handler:^(UIAlertAction * action) {
													   [alert dismissViewControllerAnimated:YES completion:nil];
												   }];
	
	[alert addAction:ok];
	[alert addAction:cancel];
	
	[alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {

	}];
	
	[self presentViewController:alert animated:YES completion:nil];
}

/**
 * Request phone number verification.
 */
- (void)resendPhoneNumberVerification {
	NSString* phoneNumber = [FirebaseManager sharedManager].loginedUser.phoneNumber;
	[[FIRPhoneAuthProvider provider] verifyPhoneNumber:phoneNumber
											completion:^(NSString * _Nullable verificationID, NSError * _Nullable error) {
												if (error) {
													[AlertViewManager showAlertView:nil msg:error.localizedDescription parent:self];
													return;
												}
												// Sign in using the verificationID and the code sent to the user
												verificationId = verificationID;
												[self verifyPhoneNumberWithCode];

											}];
}

- (void)verifyPhoneNumberWithCode {
	UIAlertController * alert = [UIAlertController
								 alertControllerWithTitle:@"Enter the 6 digit code"
								 message:nil
								 preferredStyle:UIAlertControllerStyleAlert];
	
	UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Verify" style:UIAlertActionStyleDefault
											   handler:^(UIAlertAction * action) {
												   //Do Some action here
												   NSArray * textfields = alert.textFields;
												   UITextField * tfCode = textfields[0];
												   
												   [self signInWithVerifyCode:tfCode.text];
												   
											   }];
	UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
												   handler:^(UIAlertAction * action) {
													   [alert dismissViewControllerAnimated:YES completion:nil];
												   }];
	
	[alert addAction:ok];
	[alert addAction:cancel];
	
	[alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
		textField.keyboardType = UIKeyboardTypeNumberPad;
		
	}];
	
	[self presentViewController:alert animated:YES completion:nil];
}

- (void)signInWithVerifyCode:(NSString*)verifyCode {
	FIRAuthCredential *credential = [[FIRPhoneAuthProvider provider]
									 credentialWithVerificationID:verificationId
									 verificationCode:verifyCode];
	
	[[FIRAuth auth] signInWithCredential:credential
							  completion:^(FIRUser *user, NSError *error) {
								  if (error) {
									  // ...
									  [AlertViewManager showAlertView:nil msg:error.localizedDescription parent:self];
									  return;
								  }
								  
								  // User successfully signed in. Get user data from the FIRUser object
								  [self deleteAccountFromServer];
							  }];
}

- (void)deleteAccountByAuthenticatedFacebook {
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions: @[@"email", @"public_profile"]
                 fromViewController:self
                            handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                if (error) {
                                    [AlertViewManager showAlertView:@"Facebook Login" msg:error.localizedDescription parent:self];
                                } else if (result.isCancelled) {
                                    [AlertViewManager showAlertView:@"Facebook Login" msg:@"Login cancelled" parent:self];
                                } else {
                                    NSLog(@"Logged in");
                                    FIRAuthCredential *credential = [FIRFacebookAuthProvider
                                                                     credentialWithAccessToken:[FBSDKAccessToken currentAccessToken].tokenString];
                                    
                                    [[FIRAuth auth] signInWithCredential:credential
                                                              completion:^(FIRUser *user, NSError *error) {
                                                                  if (error) {
                                                                      // ...
                                                                      [AlertViewManager showAlertView:nil msg:error.localizedDescription parent:self];
                                                                      return;
                                                                  }
                                                                  
                                                                  // User successfully signed in. Get user data from the FIRUser object
                                                                  [self deleteAccountFromServer];
                                                              }];
                                }
                            }];
}

- (void)deleteAccountByAuthenticatedTwitter {
    [[Twitter sharedInstance] logInWithCompletion:^(TWTRSession *session, NSError *error) {
        if (session) {
            //Saving Session
            TWTRSessionStore *store = [[Twitter sharedInstance] sessionStore];
            [store saveSessionWithAuthToken:session.authToken authTokenSecret:session.authTokenSecret completion:^(id<TWTRAuthSession> session, NSError* error) {
                if (error) {
                    [AlertViewManager showAlertView:@"Twitter Sign in Error!" msg:error.localizedDescription parent:self];
                } else {
                    FIRAuthCredential *credential = [FIRTwitterAuthProvider credentialWithToken:session.authToken
                                                                                         secret:session.authTokenSecret];
                    [[FIRAuth auth] signInWithCredential:credential
                                              completion:^(FIRUser *user, NSError *error) {
                                                  if (error) {
                                                      [AlertViewManager showAlertView:@"Twitter Sign up" msg:error.localizedDescription parent:self];
                                                      return;
                                                  }
                                                  
                                                  // User successfully signed in. Get user data from the FIRUser object
                                                  [self deleteAccountFromServer];
                                              }];
                }
            }];
            
            NSLog(@"signed in as %@", [session userName]);
        } else {
            [AlertViewManager showAlertView:@"Error!" msg:error.localizedDescription parent:self];
        }
    }];
}

//Google Login
- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)googleUser withError:(NSError *)error {
    // ...
    if (error == nil) {
        GIDAuthentication *authentication = googleUser.authentication;
        FIRAuthCredential *credential =
        [FIRGoogleAuthProvider credentialWithIDToken:authentication.idToken
                                         accessToken:authentication.accessToken];
        
        [[FIRAuth auth] signInWithCredential:credential
                                  completion:^(FIRUser *user, NSError *error) {
                                      if (error) {
                                          [AlertViewManager showAlertView:@"Google Sign up" msg:error.localizedDescription parent:self];
                                          return;
                                      }
                                      
                                      [self deleteAccountFromServer];

                                  }];
    } else {
        [AlertViewManager showAlertView:@"Error!" msg:error.localizedDescription parent:self];
    }
}

- (void)deleteAccountFromServer {
    [[FirebaseManager sharedManager] deleteAccount];
	[[FirebaseManager currentUser] deleteWithCompletion:^(NSError* error) {
		if (error) {
			[AlertViewManager showAlertView:@"Error" msg:error.localizedDescription parent:self];
		} else {
			dispatch_async(dispatch_get_main_queue(), ^{
				[[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"completedRegister"];
				[self dismissViewControllerAnimated:NO completion:^(void) {
					[[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_LOG_OUT object:self];
				}];
			});
		}
	}];
}

- (void)handleProfilePhotoTap:(UITapGestureRecognizer *)recognizer {
	User* user = [FirebaseManager sharedManager].loginedUser;
	PhotoViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PhotoViewController"];
	[vc setPhotoUrl:user.userProfile.photoUrl];
	[self presentViewController:vc animated:YES completion:nil];
}

#pragma mark - IBAction
- (IBAction)onBack:(id)sender {
	CATransition *transition = [CATransition animation];
	transition.duration = 0.3;
	transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
	transition.type = kCATransitionPush;
	transition.subtype = kCATransitionFromLeft;
	[self.view.window.layer addAnimation:transition forKey:nil];
	[self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)onUpgrade:(id)sender {
	if (upgradedState == 0) {
		self.vgUpgradeView.hidden = NO;
	} else if (upgradedState == 1) {
		AddSocialViewController* svc = [self.storyboard instantiateViewControllerWithIdentifier:@"AddSocialViewController"];
		CATransition *transition = [CATransition animation];
		transition.duration = 0.3;
		transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
		transition.type = kCATransitionPush;
		transition.subtype = kCATransitionFromRight;
		[self.view.window.layer addAnimation:transition forKey:nil];
		[self presentViewController:svc animated:NO completion:nil];
	} else {
		
	}
}

- (IBAction)onMenu:(id)sender {
	UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
	
	//Cancel
	[actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
		[actionSheet dismissViewControllerAnimated:YES completion:nil];
	}]];
	
	[actionSheet addAction:[UIAlertAction actionWithTitle:@"Terms and Privacy Policy" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {		
		[self performSegueWithIdentifier:@"MyProfileToTerms" sender:self];
		
	}]];
	
	[actionSheet addAction:[UIAlertAction actionWithTitle:@"Edit Your Service" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
		[self onEditService:nil];
		
	}]];

	[actionSheet addAction:[UIAlertAction actionWithTitle:@"Edit Profile" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
		[self onEditProfile:nil];
	}]];
	
	[actionSheet addAction:[UIAlertAction actionWithTitle:@"Log out" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
		[self logout];
	}]];
	
	[actionSheet addAction:[UIAlertAction actionWithTitle:@"Delete My account" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
		[self deleteMyAccount];
	}]];
	// Present action sheet.
	[self presentViewController:actionSheet animated:YES completion:nil];
}

- (IBAction)onShowAchievement:(id)sender {
	AchievementViewController* svc = [self.storyboard instantiateViewControllerWithIdentifier:@"AchievementViewController"];
	CATransition *transition = [CATransition animation];
	transition.duration = 0.3;
	transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
	transition.type = kCATransitionPush;
	transition.subtype = kCATransitionFromRight;
	[self.view.window.layer addAnimation:transition forKey:nil];
	[self presentViewController:svc animated:NO completion:nil];
}

- (IBAction)onEditProfile:(id)sender {
	EditProfileViewController* svc = [self.storyboard instantiateViewControllerWithIdentifier:@"EditProfileViewController"];
	CATransition *transition = [CATransition animation];
	transition.duration = 0.3;
	transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
	transition.type = kCATransitionPush;
	transition.subtype = kCATransitionFromRight;
	[self.view.window.layer addAnimation:transition forKey:nil];
	[self presentViewController:svc animated:NO completion:nil];
}

- (IBAction)onEditService:(id)sender {
	ProvideServiceViewController* svc = [self.storyboard instantiateViewControllerWithIdentifier:@"ProvideServiceViewController"];
	CATransition *transition = [CATransition animation];
	transition.duration = 0.3;
	transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
	transition.type = kCATransitionPush;
	transition.subtype = kCATransitionFromRight;
	[self.view.window.layer addAnimation:transition forKey:nil];
	[self presentViewController:svc animated:NO completion:nil];
}

- (IBAction)onInAppPurchase:(id)sender {
	self.vgUpgradeView.hidden = YES;
	upgradedState = 1;
	[self setUpdateStatus];
	[[FirebaseManager sharedManager] upgradedUser];
}

- (IBAction)onNotInterested:(id)sender {
	self.vgUpgradeView.hidden = YES;
}

- (IBAction)onTouchSnapShot:(id)sender {
}

- (IBAction)onTouchLinkIn:(id)sender {
}

- (IBAction)onFacebook:(id)sender {
}

- (IBAction)onTwitter:(id)sender {
	// Check if current session has users logged in
	if ([[Twitter sharedInstance].sessionStore hasLoggedInUsers]) {
		TWTRComposerViewController *composer = [TWTRComposerViewController emptyComposer];
		[self presentViewController:composer animated:YES completion:nil];
	} else {
		[[Twitter sharedInstance] logInWithCompletion:^(TWTRSession *session, NSError *error) {
			if (session) {
				TWTRComposerViewController *composer = [TWTRComposerViewController emptyComposer];
				[self presentViewController:composer animated:YES completion:nil];
			} else {
				UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"No Twitter Accounts Available" message:@"You must log in before presenting a composer." preferredStyle:UIAlertControllerStyleAlert];
				[self presentViewController:alert animated:YES completion:nil];
			}
		}];
	}
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
