//
//  AchievementViewController.m
//  FamousMe
//
//  Created by Shine Man on 12/1/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "AchievementViewController.h"
#import "AchievementTableViewCell.h"
#import "DateUtils.h"

@interface AchievementViewController () <UITableViewDelegate, UITableViewDataSource> {
	NSMutableArray* achievementArray;
	float achievementValue;
}

@end

@implementation AchievementViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	
	achievementArray = [[NSMutableArray alloc] init];
	self.tbAchievements.delegate = self;
	self.tbAchievements.dataSource = self;
	
	achievementValue = 0.0f;
	[self.prProgress setProgress:achievementValue];
	
	[self getAchievements];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self applyTheme];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)applyTheme {
    [ThemeManager setThemeWithChildViews:_headerView];
}

- (void)getAchievements {
	User* user = [FirebaseManager sharedManager].loginedUser;
	[[FirebaseManager sharedManager] getAchievements:user.key completion:^(Achievement* achievement, NSError* error) {
		if (error) {
			[AlertViewManager showAlertView:@"Error!" msg:error.localizedDescription parent:self];
			return;
		}
		
		if (achievement) {
			achievementValue = achievementValue + achievement.value;
			[self.prProgress setProgress:achievementValue];
			[achievementArray insertObject:achievement atIndex:0];
			[self.tbAchievements reloadData];
		}		
	}];
}

- (IBAction)onBack:(id)sender {
	CATransition *transition = [CATransition animation];
	transition.duration = 0.3;
	transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
	transition.type = kCATransitionPush;
	transition.subtype = kCATransitionFromLeft;
	[self.view.window.layer addAnimation:transition forKey:nil];
	[self dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark - UITableViewDelegate & DataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 70;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex {
	return achievementArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *cellIdentifier = @"AchievementTableViewCell";
	AchievementTableViewCell *cell = (AchievementTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	
	Achievement* achievement = [achievementArray objectAtIndex:indexPath.row];
	
	cell.lbName.text = achievement.name;
	if ([achievement.type isEqualToString:ACHIEVEMENT_START_CONVERSATION])
		cell.ivPhoto.image = [UIImage imageNamed:@"iconChatsInactive"];
	else if ([achievement.type isEqualToString:ACHIEVEMENT_USE_APP_TWO_WEEKS])
		cell.ivPhoto.image = [UIImage imageNamed:@"adjust"];
	else if ([achievement.type isEqualToString:ACHIEVEMENT_IMPORT_CONTACTS])
		cell.ivPhoto.image = [UIImage imageNamed:@"addressBook"];
	
	cell.lbActiveTime.text = [DateUtils getMonthAndDayFromTimestamp:achievement.timeStamp];
	
	return cell;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
