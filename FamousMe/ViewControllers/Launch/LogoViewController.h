//
//  LogoViewController.h
//  FamousMe
//
//  Created by Shine Man on 11/14/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LogoViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *btnGetStarted;
@property (weak, nonatomic) IBOutlet UIButton *btnHaveAccount;
@property (weak, nonatomic) IBOutlet UILabel *lbMagicTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbMagicContent;

@end
