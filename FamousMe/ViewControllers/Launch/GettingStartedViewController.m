//
//  GettingStartedViewController.m
//  FamousMe
//
//  Created by Shine Man on 12/11/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "GettingStartedViewController.h"
#import "BoardingPageViewController.h"


@interface GettingStartedViewController () <CLLocationManagerDelegate>{
	NSArray* pageTitles;
	NSArray* pageDetails;
	
	NSMutableArray* imageViewArray;
	NSMutableArray* nearUserArray;
	NSMutableArray* animationArray;
	CLLocationManager *locationManager;
	CLLocation *currentLocation;
	BOOL isStoppedAnimation;
	BOOL isStoppedLocationUpdate;
	
	CLGeocoder *geocoder;
	NSString* currentCity;
	
}

@end

@implementation GettingStartedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	
	nearUserArray = [NSMutableArray array];
	animationArray = [NSMutableArray array];
	isStoppedAnimation = YES;
	isStoppedLocationUpdate = YES;
	
    if ([UIScreen mainScreen].bounds.size.height < 500) {
        [self.lbEnableLocationTitle setFont: [self.lbEnableLocationTitle.font fontWithSize: 16]];
        [self.lbEnableLocationContent setFont: [self.lbEnableLocationContent.font fontWithSize: 14]];
        
        [self.lbCityName setFont: [self.lbCityName.font fontWithSize: 38]];
        [self.btnInviteFriends.titleLabel setFont: [self.btnInviteFriends.titleLabel.font fontWithSize: 14]];
        
        [self.lbNumberOfUsers setFont: [self.lbNumberOfUsers.font fontWithSize: 80]];
    }
    
	[self prepareBoardingPage];
	[self prepareControls];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareBoardingPage {
	// Do any additional setup after loading the view.
	pageTitles = @[@"A network beyond digital", @"Everyone has their own profile", @"Use this the way you like it", @"Set your privacy settings"];
	pageDetails = @[@"You will have the opportunity to connect with every new member in the area. \n Invite your friends and ask them to invite theirs!",
					@"Build your own profile, add the things that matter to you, and add things you want to talk about!",
					@"Build your own profile, add the things that matter to you, and add things you want to talk about!",
					@"We respect you have your own preferences in whom to meet, so you can fully customise your privacy settings!"];
	
	// Create page view controller
	self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
	self.pageViewController.dataSource = self;
	self.pageViewController.delegate = self;
	
	BoardingPageViewController *startingViewController = [self viewControllerAtIndex:0];
	NSArray *viewControllers = @[startingViewController];
	[self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
	
	CGRect bounds = self.vgBoardingPageContainer.frame;
	// Change the size of page view controller
	self.pageViewController.view.frame = CGRectMake(0, 0, bounds.size.width, bounds.size.height);
	
	[self addChildViewController:_pageViewController];
	[self.vgBoardingPageContainer addSubview:_pageViewController.view];
	[self.pageViewController didMoveToParentViewController:self];
}

- (void)prepareControls {
	self.btnInviteFriends.layer.cornerRadius = 8.0f;
	[self.btnInviteFriends setClipsToBounds:YES];
	
	imageViewArray = [NSMutableArray array];
	
	[imageViewArray addObject:self.ivProfile1];
	[imageViewArray addObject:self.ivProfile2];
	[imageViewArray addObject:self.ivProfile3];
	[imageViewArray addObject:self.ivProfile4];
	[imageViewArray addObject:self.ivProfile5];
	[imageViewArray addObject:self.ivProfile6];
	[imageViewArray addObject:self.ivProfile7];
	[imageViewArray addObject:self.ivProfile8];
	
	for (UIImageView* imageView in imageViewArray) {
		imageView.layer.cornerRadius = imageView.frame.size.height/2;
		[imageView setClipsToBounds:YES];
	}
}

- (BoardingPageViewController *)viewControllerAtIndex:(NSUInteger)index {
	if (([pageTitles count] == 0) || (index >= [pageDetails count])) {
		return nil;
	}
	
	// Create a new view controller and pass suitable data.
	BoardingPageViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BoardingPageViewController"];
	
	if (index == 0) {
		if (nearUserArray.count > 0) {
			pageContentViewController.detailText = @"These people around you have interests, hobbies, side projects and loads of other things you night have in common with them.";
		} else
			pageContentViewController.detailText = pageDetails[0];
	} else
		pageContentViewController.detailText = pageDetails[index];
	
	pageContentViewController.titleText = pageTitles[index];
	pageContentViewController.pageIndex = index;

	return pageContentViewController;
}

- (void)setUIbyPageIndex:(NSUInteger)index {
	if (index == 0) {
		if (nearUserArray.count > 0) {
			self.vgProfilesNearByContainer.hidden = YES;
			self.vgInviteContainer.hidden = YES;
			self.vgNumberOfUsersContainer.hidden = NO;
		} else {
			self.vgProfilesNearByContainer.hidden = YES;
			self.vgInviteContainer.hidden = NO;
			self.vgNumberOfUsersContainer.hidden = YES;
		}
		
		for (UIImageView* imageView in imageViewArray)
			[imageView.layer removeAllAnimations];
		
		isStoppedAnimation = YES;
	} else {
		if (nearUserArray > 0) {
			self.vgProfilesNearByContainer.hidden = NO;
			self.vgInviteContainer.hidden = YES;
			self.vgNumberOfUsersContainer.hidden = YES;
			if (isStoppedAnimation)
				[self animationImageViews];
			
			isStoppedAnimation = NO;
		} else {
			self.vgProfilesNearByContainer.hidden = YES;
			self.vgInviteContainer.hidden = NO;
			self.vgNumberOfUsersContainer.hidden = YES;
		}
	}
}

/**
 * Moving profile images.
 */
- (void)animationImageViews {
	NSInteger imageCount;
	if (nearUserArray.count < imageViewArray.count)
		imageCount = nearUserArray.count;
	else
		imageCount = imageViewArray.count;
	
	for (NSInteger i = 0; i < imageCount; i++) {
		UIImageView* imageView = [imageViewArray objectAtIndex:i];
		User* user = [nearUserArray objectAtIndex:i];
		[FirebaseManager showImageWithUrl:imageView imagePath:user.userProfile.photoUrl];
		[self animateImage:i view:imageView];
	}
}


/**
 * Move image according to path.
 * @param indexOfPosition The index of image
 * @param view  The image view.
 */
- (void)animateImage:(NSInteger)indexOfPosition view:(UIImageView*)view {
	if (animationArray.count >= imageViewArray.count)
		return;
	
	UIBezierPath *path = [UIBezierPath bezierPath];
	
	CGFloat x = view.frame.origin.x;
	CGFloat y = view.frame.origin.y;
	switch (indexOfPosition) {
		case 0:
		case 5:
			[path moveToPoint:CGPointMake(x + 0, y + 0)];
			[path addLineToPoint:CGPointMake(x - 30, y + 100)];
			[path addLineToPoint:CGPointMake(x + 20, y + 20)];
			[path addLineToPoint:CGPointMake(x + 30, y - 30)];
			[path addLineToPoint:CGPointMake(x + 0, y + 0)];
			break;
		case 1:
		case 6:
			[path moveToPoint:CGPointMake(x + 0, y + 0)];
			[path addLineToPoint:CGPointMake(x + 10, y + 110)];
			[path addLineToPoint:CGPointMake(x + 0, y + 120)];
			[path addLineToPoint:CGPointMake(x + 20, y + 10)];
			[path addLineToPoint:CGPointMake(x + 0, y + 0)];
			break;
		case 2:
			[path moveToPoint:CGPointMake(x + 0, y + 0)];
			[path addLineToPoint:CGPointMake(x + 60, y + 70)];
			[path addLineToPoint:CGPointMake(x + 0, y + 0)];
			break;
		case 3:
			[path moveToPoint:CGPointMake(x + 0, y + 0)];
			[path addLineToPoint:CGPointMake(x + 70, y + 90)];
			[path addLineToPoint:CGPointMake(x + 20, y + 50)];
			[path addLineToPoint:CGPointMake(x + 0, y + 0)];
			break;
		case 4:
			[path moveToPoint:CGPointMake(x + 0, y + 0)];
			[path addLineToPoint:CGPointMake(x + 30, y + 50)];
			[path addLineToPoint:CGPointMake(x - 10, y + 50)];
			[path addLineToPoint:CGPointMake(x + 0, y + 0)];
			break;
		case 7:
			[path moveToPoint:CGPointMake(x + 0, y + 0)];
			[path addLineToPoint:CGPointMake(x + 40, y + 100)];
			[path addLineToPoint:CGPointMake(x + 0, y + 0)];
			break;
	}
	
	CAKeyframeAnimation *animatePosition = [CAKeyframeAnimation animationWithKeyPath:@"position"];
	animatePosition.path = [path CGPath];
	animatePosition.duration = 8.0;
	animatePosition.autoreverses = YES;
	animatePosition.repeatCount = HUGE_VALF;
	[view.layer addAnimation:animatePosition forKey:@"position"];
}

- (void)enableMyLocation {
	
	locationManager = [[CLLocationManager alloc] init];
	
	locationManager.delegate = self;
	locationManager.desiredAccuracy = kCLLocationAccuracyBest;
	//locationManager.distanceFilter = 50;
	geocoder = [[CLGeocoder alloc] init];
	
	isStoppedLocationUpdate = NO;
	[locationManager requestAlwaysAuthorization];
	[locationManager startUpdatingLocation];
}

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
	NSLog(@"didFailWithError: %@", error);
   
    [AlertViewManager showAlertView:@"Error" msg:@"Failed to Get Your Location" parent:self];
}

- (void)locationManager:(CLLocationManager *)manager
	didUpdateToLocation:(CLLocation *)newLocation
		   fromLocation:(CLLocation *)oldLocation {
	
	if (isStoppedLocationUpdate)
		return;

	currentLocation = newLocation;
	isStoppedLocationUpdate = YES;
	[geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
		CLPlacemark *placemark = [placemarks lastObject];

		currentCity= placemark.locality;
		if (currentCity == nil) {
			NSDictionary* addressDict = placemark.addressDictionary;
			currentCity = [addressDict objectForKey:@"State"];
		}
		
		NSLog(@"We live in %@", currentCity);
		self.lbCityName.text = currentCity;
		
		// stopping locationManager from fetching again
		[locationManager stopUpdatingLocation];
		locationManager = nil;
		geocoder = nil;
	}];
	
	self.vgEnableLocation.hidden = YES;
	[self getNearUsers];
}

- (void)getNearUsers {
	
    [[FirebaseManager sharedManager] getNearUsers:currentLocation radius:DISCOVERY_FINDING_RADIUS
									   completion:^(User* user, CLLocation* userLocation, NSError* error)
	 {
		 if (error) {
			 [AlertViewManager showAlertView:@"Error!" msg:error.localizedDescription parent:self];
			 return;
		 }
		 
		 if (!user) return;
		 [nearUserArray addObject:user];
		 self.lbNumberOfUsers.text = [NSString stringWithFormat:@"%ld", (unsigned long)nearUserArray.count];
	 }];
}


#pragma mark - Page View Controller Data Source
- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
	NSUInteger index = ((BoardingPageViewController*) viewController).pageIndex;

	if ((index == 0) || (index == NSNotFound))
		return nil;
	
	index--;
	
	return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
	NSUInteger index = ((BoardingPageViewController*) viewController).pageIndex;
	
	if (index == NSNotFound)
		return nil;
	
	index++;
	if (index == [pageTitles count])
		return nil;

	return [self viewControllerAtIndex:index];
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
	return [pageTitles count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
	return 0;
}

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray<UIViewController *> *)previousViewControllers transitionCompleted:(BOOL)completed {
	if (completed) {
		
		NSUInteger currentIndex = ((BoardingPageViewController *)self.pageViewController.viewControllers.firstObject).pageIndex;
		
		[self setUIbyPageIndex:currentIndex];
	}
}

#pragma mark IBAction
- (IBAction)onTouchEnableLocation:(id)sender {
	[self enableMyLocation];
}

- (IBAction)onTouchSetupProfile:(id)sender {
	[self performSegueWithIdentifier:@"gettingStartedToVerify" sender:self];
}

- (IBAction)onInviteFriends:(id)sender {
	[self performSegueWithIdentifier:@"GettingStartedToInvite" sender:nil];
}

- (IBAction)onBack:(id)sender {
	CATransition *transition = [CATransition animation];
	transition.duration = 0.3;
	transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
	transition.type = kCATransitionPush;
	transition.subtype = kCATransitionFromLeft;
	[self.view.window.layer addAnimation:transition forKey:nil];
	[self dismissViewControllerAnimated:NO completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
