//
//  EnableLocationViewController.h
//  FamousMe
//
//  Created by Shine Man on 12/20/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BoardingPageViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbDetail;

@property NSUInteger pageIndex;
@property NSString *titleText;
@property NSString *detailText;
@end
