//
//  LogoViewController.m
//  FamousMe
//
//  Created by Shine Man on 11/14/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "LogoViewController.h"
#import "VerifyPhoneViewController.h"

@interface LogoViewController ()

@end

@implementation LogoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	
	self.btnGetStarted.layer.cornerRadius = 7.0f;
	self.btnHaveAccount.layer.cornerRadius = 7.0f;
	
	self.btnHaveAccount.layer.borderWidth = 1.0f;
	self.btnHaveAccount.layer.borderColor = [MAIN_COLOR CGColor];
    
    if ([UIScreen mainScreen].bounds.size.height < 500) {
        [self.lbMagicTitle setFont: [self.lbMagicTitle.font fontWithSize: 24]];
        [self.lbMagicContent setFont: [self.lbMagicContent.font fontWithSize: 14]];
        [self.btnHaveAccount.titleLabel setFont: [self.btnHaveAccount.titleLabel.font fontWithSize: 14]];
        [self.btnGetStarted.titleLabel setFont: [self.btnGetStarted.titleLabel.font fontWithSize: 14]];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onGetStarted:(id)sender {
	[[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isHaveAccount"];
	[self performSegueWithIdentifier:@"logoToGettingStarted" sender:self];
}

- (IBAction)onHaveAccount:(id)sender {
	[[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isHaveAccount"];
	[self performSegueWithIdentifier:@"logoToVerify" sender:self];
}

 #pragma mark - Navigation

//// In a storyboard-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//	// Get the new view controller using [segue destinationViewController].
//	// Pass the selected object to the new view controller.
//	if ([[segue identifier] isEqualToString:@"logoToVerify"]) {
//		VerifyPhoneViewController* viewController = [segue destinationViewController];
//		[viewController setHaveAccount:NO];
//	}
//}

@end
