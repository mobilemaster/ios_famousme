//
//  GettingStartedViewController.h
//  FamousMe
//
//  Created by Shine Man on 12/11/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GettingStartedViewController : UIViewController <UIPageViewControllerDataSource, UIPageViewControllerDelegate>

@property (strong, nonatomic) UIPageViewController *pageViewController;

@property (weak, nonatomic) IBOutlet UIView *vgEnableLocation;
@property (weak, nonatomic) IBOutlet UIView *vgBoarding;
@property (weak, nonatomic) IBOutlet UIView *vgBoardingPageContainer;
@property (weak, nonatomic) IBOutlet UIView *vgNumberOfUsersContainer;
@property (weak, nonatomic) IBOutlet UILabel *lbNumberOfUsers;

@property (weak, nonatomic) IBOutlet UIView *vgInviteContainer;
@property (weak, nonatomic) IBOutlet UIButton *btnInviteFriends;
@property (weak, nonatomic) IBOutlet UILabel *lbCityName;
@property (weak, nonatomic) IBOutlet UIView *vgProfilesNearByContainer;
@property (weak, nonatomic) IBOutlet UILabel *lbEnableLocationTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbEnableLocationContent;

@property (weak, nonatomic) IBOutlet UIImageView *ivProfile1;
@property (weak, nonatomic) IBOutlet UIImageView *ivProfile2;
@property (weak, nonatomic) IBOutlet UIImageView *ivProfile3;
@property (weak, nonatomic) IBOutlet UIImageView *ivProfile4;
@property (weak, nonatomic) IBOutlet UIImageView *ivProfile5;
@property (weak, nonatomic) IBOutlet UIImageView *ivProfile6;
@property (weak, nonatomic) IBOutlet UIImageView *ivProfile7;
@property (weak, nonatomic) IBOutlet UIImageView *ivProfile8;

@end
