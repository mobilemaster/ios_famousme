//
//  EnableLocationViewController.m
//  FamousMe
//
//  Created by Shine Man on 12/20/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "BoardingPageViewController.h"

@interface BoardingPageViewController ()

@end

@implementation BoardingPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	self.lbTitle.text = self.titleText;
	self.lbDetail.text = self.detailText;
    
    if ([UIScreen mainScreen].bounds.size.height < 500) {
        [self.lbTitle setFont: [self.lbTitle.font fontWithSize: 16]];
        [self.lbDetail setFont: [self.lbDetail.font fontWithSize: 14]];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
