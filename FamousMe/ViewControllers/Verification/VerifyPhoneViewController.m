//
//  VerifyPhoneViewController.m
//  FamousMe
//
//  Created by Shine Man on 11/17/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "VerifyPhoneViewController.h"
#import "SelectCountryView.h"
#import "SetupProfileViewController.h"
#import "MBProgressHUD.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@import MRCountryPicker;
@import TwitterKit;
@import GoogleSignIn;

@interface VerifyPhoneViewController ()<MRCountryPickerDelegate, GIDSignInDelegate, GIDSignInUIDelegate> {
	NSString* countryCode;
	NSString* countryName;
	NSString* dialCode;
	NSString* phoneNumber;
	NSString* verificationId;
	
	BOOL 	isHaveAccount;
}

@property (weak, nonatomic) SelectCountryView* selectCountryView;

@property (weak, nonatomic) NSString* tempCountryCode;
@property (weak, nonatomic) NSString* tempCountryName;
@property (weak, nonatomic) NSString* tempDialCode;
@end

@implementation VerifyPhoneViewController
@synthesize selectCountryView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	
	selectCountryView = nil;
	[self setCountryCodeByLocale];
	
	isHaveAccount = [[NSUserDefaults standardUserDefaults] boolForKey:@"isHaveAccount"];

    //social login
    self.btnFacebook.layer.cornerRadius = 8.0f;
    [self.btnFacebook setClipsToBounds:YES];
    
    self.btnTwitter.layer.cornerRadius = 8.0f;
    [self.btnTwitter setClipsToBounds:YES];
    
    self.btnGoogle.layer.cornerRadius = 8.0f;
    [self.btnGoogle setClipsToBounds:YES];
    
    self.btnHaveAccount.layer.cornerRadius = 8.0f;
    self.btnHaveAccount.layer.borderWidth = 1.0f;
    self.btnHaveAccount.layer.borderColor = MAIN_COLOR.CGColor;
    [self.btnHaveAccount setClipsToBounds:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setCountryCodeByLocale {
	NSLocale *locale = [NSLocale currentLocale];
	NSString *localeCountryCode = [locale objectForKey: NSLocaleCountryCode];
	
	NSString *path = [[NSBundle mainBundle] pathForResource:@"countryCodes" ofType:@"json"];
	NSData *data = [NSData dataWithContentsOfFile:path];	
	NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];

	for (NSDictionary *country in dict) {
		NSString *code = [country objectForKey:@"code"];
		if ([localeCountryCode isEqualToString:code]) {
			countryCode = code;
			dialCode = [country objectForKey:@"dial_code"];
			countryName = [country objectForKey:@"name"];
		
			[self setSelectCountyCodeButton];
			return;
		}
	}
}

/**
 * Request phone number verification.
 */
- (void)startPhoneNumberVerification {
    phoneNumber = [NSString stringWithFormat:@"%@%@", dialCode, self.tfPhoneNumber.text];
	//phoneNumber = @"+8562055800876";
    [[FIRPhoneAuthProvider provider] verifyPhoneNumber:phoneNumber
                                            completion:^(NSString * _Nullable verificationID, NSError * _Nullable error) {
                                                if (error) {
                                                    [AlertViewManager showAlertView:nil msg:error.localizedDescription parent:self];
                                                    return;
                                                }
                                                // Sign in using the verificationID and the code sent to the user                                                
                                                verificationId = verificationID;
                                                self.vgDigitCodeViewGroup.hidden = NO;
                                                self.btnBack.hidden = YES;
                                                self.btnNext.hidden = YES;
												self.lbTitle.text = @"Verify your phone number";
												self.lbPhoneNumber.text = phoneNumber;
                                            }];
}

/**
 * Sign in with verify code.
 */
- (void)signInWithVerifyCode:(NSString*)verifyCode {
	FIRAuthCredential *credential = [[FIRPhoneAuthProvider provider]
									 credentialWithVerificationID:verificationId
									 verificationCode:verifyCode];
	
	[[FIRAuth auth] signInWithCredential:credential
							  completion:^(FIRUser *user, NSError *error) {
								  if (error) {
									  // ...
									  [AlertViewManager showAlertView:nil msg:error.localizedDescription parent:self];
									  return;
								  }
								  // User successfully signed in. Get user data from the FIRUser object
								  if (isHaveAccount) {
                                      [[FirebaseManager sharedManager] isExistUser:[FirebaseManager getCurrentUserID] completion:^(BOOL isExist, NSError* error) {
                                          if (isExist) {
                                              [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"completedRegister"];
                                              [self performSegueWithIdentifier:@"verifyToMain" sender:self];
                                          } else {
                                              [self performSegueWithIdentifier:@"verifyToSetup" sender:self];
                                          }
                                          
                                      }];
								  }
                                  else
									  [self performSegueWithIdentifier:@"verifyToSetup" sender:self];

							  }];
}

- (void)setSelectCountyCodeButton {
	NSString* fullCountryString = [NSString stringWithFormat:@"%@ (%@)", countryName, dialCode];
	[self.btnSelectCountry setTitle:fullCountryString forState:UIControlStateNormal];
}

/**
 * Show country phone code picker.
 */
- (void)showCountryPhoneCodePicker {
	if (!selectCountryView)
		return;
	
	CGRect boundRect = [[UIScreen mainScreen] bounds];
	selectCountryView.frame = CGRectMake(0, boundRect.size.height, boundRect.size.width, boundRect.size.height);
	
	[UIView animateWithDuration:0.5
						  delay:0.1
						options: UIViewAnimationOptionCurveEaseIn
					 animations:^{
						 selectCountryView.frame = CGRectMake(0, 0, boundRect.size.width, boundRect.size.height);
					 }
					 completion:^(BOOL finished){
						 if (finished) {
							 selectCountryView.layer.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.25].CGColor;
						 }
					 }];
	
	selectCountryView.countryPicker.countryPickerDelegate = self;
	[selectCountryView.countryPicker setCountry:countryCode];
	[self.view addSubview:selectCountryView];
}

- (void)hideCountryPhoneCodePicker {
	if (!selectCountryView)
		return;
	
	selectCountryView.backgroundColor = [UIColor clearColor];
	CGRect boundRect = [[UIScreen mainScreen] bounds];
	[UIView animateWithDuration:0.5
						  delay:0.1
						options: UIViewAnimationOptionCurveEaseIn
					 animations:^{
						 selectCountryView.frame = CGRectMake(0, boundRect.size.height, boundRect.size.width, boundRect.size.height);
					 }
					 completion:^(BOOL finished){
						 if (finished) {
							 [selectCountryView removeFromSuperview];
							 selectCountryView = nil;
						 }
					 }];
}

- (void)setCountryPhoneCode {
	countryName = self.tempCountryName;
	countryCode = self.tempCountryCode;
	dialCode = self.tempDialCode;
	
	[self setSelectCountyCodeButton];
	[self hideCountryPhoneCodePicker];
}

- (void)cancelCountryPhoneCode {
	[self hideCountryPhoneCodePicker];
}

- (void)setHaveAccount:(BOOL)haveAccount {
	isHaveAccount = haveAccount;
}

- (BOOL)validatePhoneNumber {
	if (!_tfPhoneNumber.text || _tfPhoneNumber.text.length < 7) {
		[AlertViewManager showAlertView:@"The phone number you entered is not a valid phone number." msg:@"Please do make sure you entered a correct phone number." parent:self];
		return false;
	}
	
	return YES;
}

- (User*)getNewUserForSocial {
    User* user = [[User alloc] init];
    
    user.userProfile.isValid = YES;
    user.userProfile.deletedTimestamp = 0;
    user.userProfile.username = @"";
    
    user.countryCode = [BaseDataManager getCountryCodeByLocaleCode];
    
    user.dateOfBirth = @"";
    user.phoneNumber = @"";

    user.userProfile.profession = @"";
    user.nationality = @"";
    user.isUpgraded = NO;
    user.isAddedSocial = NO;
    
    user.serviceTitle = @"";
    user.serviceDetails = @"";
    
    if (user.discoveryRules == nil)
        user.discoveryRules = [[DiscoveryRules alloc] init];
    
    //Initialize Discovery rules
    user.discoveryRules.discoverNationality = @"All";
    user.discoveryRules.discoveryInterest = @"All";
    
    if (user.social == nil)
        user.social = [[Social alloc] init];
    
    user.token = @"";
    
    NSArray* interestArray = [BaseDataManager getInterestList];
    NSString* interestData = nil;
    for (int i = 0; i < interestArray.count; i++) {
        NSString* interest = interestArray[i];
        if (!interestData)
            interestData = interest;
        else
            interestData = [NSString stringWithFormat:@"%@/%@", interestData, interest];
    }
    
    user.interests = interestData;
    return user;
}

//Social Login
- (void)saveUserWithTwitter {
    TWTRSession* session = [[Twitter sharedInstance] sessionStore].session;
    if (!session) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [AlertViewManager showAlertView:@"Error!" msg:@"Twitter session is expired!" parent:self];
        return;
    }
    
    TWTRAPIClient* apiClient = [[TWTRAPIClient alloc] initWithUserID:session.userID];
    [apiClient loadUserWithID:[session userID] completion:^(TWTRUser *user,
                                                            NSError *error) {
        // handle the response or error
        if (![error isEqual:nil]) {
            NSLog(@"Twitter info -> user = %@ ", user);
            
            User* me = [self getNewUserForSocial];
            
            NSString *photoUrl = [[NSString alloc]initWithString:user.profileImageLargeURL];

            me.gender = 0; //default
            me.logInSocialWith = @"twitter";
            me.userProfile.username = user.screenName;
            me.userProfile.photoUrl = photoUrl;
            me.userProfile.avatarUrl = photoUrl;
            me.emailAddress = @"";
            
            [[FirebaseManager sharedManager] uploadUserInfo:me competition:^(NSError* error) {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                if (!error) {
                    // User successfully signed in. Get user data from the FIRUser object
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"completedRegister"];
                    [self performSegueWithIdentifier:@"verifyToMain" sender:self];
                } else
                    [AlertViewManager showAlertView:@"Error!" msg:error.localizedDescription parent:self];
            }];

        } else {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [AlertViewManager showAlertView:@"Twitter error getting profile!" msg:error.localizedDescription parent:self];
        }
    }];
}

- (void)saveUserWithFaceBook {
    if ([FBSDKAccessToken currentAccessToken]) {
        NSLog(@"Token is available : %@", [[FBSDKAccessToken currentAccessToken]tokenString]);
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, cover, gender, picture.type(large), email, birthday, location"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error) {
                 NSLog(@"resultisfetchFbUserInfo:%@", result);
                 
                 User* user = [self getNewUserForSocial];
                 NSDictionary *dictionary = (NSDictionary *)result;
                 NSDictionary *picture = [dictionary objectForKey:@"picture"];
                 NSDictionary *data = [picture objectForKey:@"data"];
                 NSString *photoUrl = (NSString *)[data objectForKey:@"url"];
                 if (!photoUrl)
                     photoUrl = @"";
                 
                 if ([[dictionary objectForKey:@"gender"] isEqualToString:@"male"])
                     user.gender = 0;
                 else
                     user.gender = 1;
                 
                 user.logInSocialWith = @"facebook";
                 user.userProfile.username = [dictionary objectForKey:@"name"];
                 user.userProfile.photoUrl = photoUrl;
                 user.userProfile.avatarUrl = photoUrl;
                 user.emailAddress = [dictionary objectForKey:@"email"];
                 
                 [[FirebaseManager sharedManager] uploadUserInfo:user competition:^(NSError* error) {
                     [MBProgressHUD hideHUDForView:self.view animated:YES];
                     if (!error) {
                         // User successfully signed in. Get user data from the FIRUser object
                         [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"completedRegister"];
                         [self performSegueWithIdentifier:@"verifyToMain" sender:self];
                     } else
                         [AlertViewManager showAlertView:@"Error!" msg:error.localizedDescription parent:self];                     
                 }];
                 
             } else {
                 [MBProgressHUD hideHUDForView:self.view animated:YES];
                 [AlertViewManager showAlertView:@"Error!" msg:@"Please try again or use phone number verification" parent:self];
             }
         }];
    }
}

- (void)saveUserWithGoogle:(GIDGoogleUser *)googleUser {
    User* me = [self getNewUserForSocial];
    
    NSString *photoUrl = [[NSString alloc] initWithString:[googleUser.profile imageURLWithDimension:100].absoluteString];
    if (!photoUrl)
        photoUrl = @"";
    
    me.gender = 0; //default
    me.logInSocialWith = @"google";
    me.userProfile.username = googleUser.profile.name;
    me.userProfile.photoUrl = photoUrl;
    me.userProfile.avatarUrl = photoUrl;
    me.emailAddress = googleUser.profile.email;
    
    [[FirebaseManager sharedManager] uploadUserInfo:me competition:^(NSError* error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (!error) {
            // User successfully signed in. Get user data from the FIRUser object
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"completedRegister"];
            [self performSegueWithIdentifier:@"verifyToMain" sender:self];
        } else
            [AlertViewManager showAlertView:@"Error!" msg:error.localizedDescription parent:self];
    }];
}

//Google Login
- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)googleUser withError:(NSError *)error {
    // ...
    if (error == nil) {
        GIDAuthentication *authentication = googleUser.authentication;
        FIRAuthCredential *credential =
        [FIRGoogleAuthProvider credentialWithIDToken:authentication.idToken
                                         accessToken:authentication.accessToken];

        [[FIRAuth auth] signInWithCredential:credential
                                  completion:^(FIRUser *user, NSError *error) {
                                      if (error) {
                                          [AlertViewManager showAlertView:@"Google Sign up" msg:error.localizedDescription parent:self];
                                          return;
                                      }
                                      
                                      if (isHaveAccount) {
                                          [[FirebaseManager sharedManager] isExistUser:[FirebaseManager getCurrentUserID] completion:^(BOOL isExist, NSError* error) {
                                              if (isExist) {
                                                  [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"completedRegister"];
                                                  [self performSegueWithIdentifier:@"verifyToMain" sender:self];
                                              } else {
                                                  [self saveUserWithGoogle:googleUser];
                                              }
                                          }];
                                      }
                                      else
                                          [self saveUserWithGoogle:googleUser];
                                      
                                  }];
    } else {
        [AlertViewManager showAlertView:@"Error!" msg:error.localizedDescription parent:self];
    }
}

#pragma mark - MRCountryPickerDelegate
- (void)countryPhoneCodePicker:(MRCountryPicker *)picker didSelectCountryWithName:(NSString *)name
				   countryCode:(NSString *)countryCode
					 phoneCode:(NSString *)phoneCode flag:(UIImage *)flag {
	
	self.tempCountryCode = countryCode;
	self.tempCountryName = name;
	self.tempDialCode = phoneCode;
}

#pragma mark - IBAction

- (IBAction)onSendVerificationCode:(id)sender {
	if (![self validatePhoneNumber])
		return;

	[self startPhoneNumberVerification];
}

- (IBAction)onTouchNext:(id)sender {
    if (![self validatePhoneNumber])
        return;
    
    [self startPhoneNumberVerification];
}

- (IBAction)onVerify:(id)sender {
	NSString* verifyCode = self.tfDigitCode.text;
	if (verifyCode == nil || verifyCode.length != 6) {
		[AlertViewManager showAlertView:@"Invalid verify code" msg:@"Please do make sure you entered a correct code." parent:self];
		
		return;
	}
	
	[self signInWithVerifyCode:verifyCode];
}

- (IBAction)onNotGetCode:(id)sender {
	[self startPhoneNumberVerification];
}

- (IBAction)onSendNewCode:(id)sender {
	[self startPhoneNumberVerification];
}

- (IBAction)onSelectCountryCode:(id)sender {
	if (selectCountryView)
		return;
	
	selectCountryView = [[[NSBundle mainBundle] loadNibNamed:@"SelectCountryView" owner:self options:nil] firstObject];

	[selectCountryView.cancelButton addTarget:self action:@selector(cancelCountryPhoneCode) forControlEvents:UIControlEventTouchUpInside];

	[selectCountryView.doneButton addTarget:self action:@selector(setCountryPhoneCode) forControlEvents:UIControlEventTouchUpInside];
	
	[self showCountryPhoneCodePicker];
}

- (IBAction)onTouchTwitterLogin:(id)sender {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[Twitter sharedInstance] logInWithCompletion:^(TWTRSession *session, NSError *error) {
        if (session) {
            //Saving Session
            TWTRSessionStore *store = [[Twitter sharedInstance] sessionStore];
            [store saveSessionWithAuthToken:session.authToken authTokenSecret:session.authTokenSecret completion:^(id<TWTRAuthSession> session, NSError* error) {
                if (error) {
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                   [AlertViewManager showAlertView:@"Twitter Sign in Error!" msg:error.localizedDescription parent:self];
                } else {
                    FIRAuthCredential *credential = [FIRTwitterAuthProvider credentialWithToken:session.authToken
                                                         secret:session.authTokenSecret];
                    [[FIRAuth auth] signInWithCredential:credential
                                              completion:^(FIRUser *user, NSError *error) {
                                                  if (error) {
                                                      [MBProgressHUD hideHUDForView:self.view animated:YES];
                                                      [AlertViewManager showAlertView:@"Twitter Sign up" msg:error.localizedDescription parent:self];
                                                      return;
                                                  }
                                                  // User successfully signed in. Get user data from the FIRUser object
                                                  if (isHaveAccount) {
                                                      [[FirebaseManager sharedManager] isExistUser:[FirebaseManager getCurrentUserID] completion:^(BOOL isExist, NSError* error) {
                                                          if (isExist) {
                                                              [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"completedRegister"];
                                                              [self performSegueWithIdentifier:@"verifyToMain" sender:self];
                                                          } else {
                                                              [self saveUserWithTwitter];
                                                          }
                                                          
                                                      }];
                                                  }
                                                  else
                                                      [self saveUserWithTwitter];
                                              }];
                }
            }];
            
            NSLog(@"signed in as %@", [session userName]);
        } else {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [AlertViewManager showAlertView:@"Error!" msg:error.localizedDescription parent:self];
        }
    }];
}

- (IBAction)onTouchGoogleLogin:(id)sender {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [GIDSignIn sharedInstance].uiDelegate = self;
    [GIDSignIn sharedInstance].delegate = self;
    [[GIDSignIn sharedInstance] signIn];
}

- (IBAction)onTouchHaveAccount:(id)sender {
    
}

- (IBAction)onTouchFacebookLogin:(id)sender {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions: @[@"email", @"public_profile"]
                                    fromViewController:self
                                    handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             [MBProgressHUD hideHUDForView:self.view animated:YES];
             [AlertViewManager showAlertView:@"Facebook Login" msg:error.localizedDescription parent:self];
         } else if (result.isCancelled) {
             [MBProgressHUD hideHUDForView:self.view animated:YES];
             [AlertViewManager showAlertView:@"Facebook Login" msg:@"Login cancelled" parent:self];
         } else {
             NSLog(@"Logged in");
             FIRAuthCredential *credential = [FIRFacebookAuthProvider
                                              credentialWithAccessToken:[FBSDKAccessToken currentAccessToken].tokenString];
            
             [[FIRAuth auth] signInWithCredential:credential
                                       completion:^(FIRUser *user, NSError *error) {
                                           if (error) {
                                               [MBProgressHUD hideHUDForView:self.view animated:YES];
                                               [AlertViewManager showAlertView:@"Facebook Sign up" msg:error.localizedDescription parent:self];
                                               return;
                                           }
                                           
                                           if (isHaveAccount) {
                                               [[FirebaseManager sharedManager] isExistUser:[FirebaseManager getCurrentUserID] completion:^(BOOL isExist, NSError* error) {
                                                   if (isExist) {
                                                       [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"completedRegister"];
                                                       [self performSegueWithIdentifier:@"verifyToMain" sender:self];
                                                   } else {
                                                       [self saveUserWithFaceBook];
                                                   }
                                               }];
                                           }
                                           else
                                               [self saveUserWithFaceBook];
                                       }];
         }
     }];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
	if ([[segue identifier] isEqualToString:@"verifyToSetup"]) {
		SetupProfileViewController* setupViewController = [segue destinationViewController];
		[setupViewController setCountryCode:dialCode];
	}
}


@end
