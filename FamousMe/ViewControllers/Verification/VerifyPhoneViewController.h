//
//  VerifyPhoneViewController.h
//  FamousMe
//
//  Created by Shine Man on 11/17/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VerifyPhoneViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;

@property (weak, nonatomic) IBOutlet UITextField *tfPhoneNumber;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectCountry;
@property (weak, nonatomic) IBOutlet UIButton *btnSendVerifyCode;
@property (weak, nonatomic) IBOutlet UIView *vgSocialRegister;

@property (weak, nonatomic) IBOutlet UIButton *btnHaveAccount;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet UIButton *btnFacebook;
@property (weak, nonatomic) IBOutlet UIButton *btnTwitter;
@property (weak, nonatomic) IBOutlet UIButton *btnGoogle;

//Verify
@property (weak, nonatomic) IBOutlet UIView *vgDigitCodeViewGroup;
@property (weak, nonatomic) IBOutlet UITextField *tfDigitCode;

@property (weak, nonatomic) IBOutlet UILabel *lbPhoneNumber;

- (void)setHaveAccount:(BOOL)haveAccount;
@end
