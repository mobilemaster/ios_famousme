//
//  ChatViewController.h
//  FamousMe
//
//  Created by Shine Man on 11/23/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import <UIKit/UIKit.h>
@import GrowingTextView;

@interface ChatViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *lbUserName;
@property (weak, nonatomic) IBOutlet UIButton *lbStatus;
@property (weak, nonatomic) IBOutlet UIView *headerView;

@property (weak, nonatomic) IBOutlet UITableView *tbMessages;
@property (weak, nonatomic) IBOutlet UIView *vgInputContainer;

@property (weak, nonatomic) IBOutlet GrowingTextView *tvInputMessage;
@property (weak, nonatomic) IBOutlet UIButton *btnSend;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomContraint;

- (void)setBaseData:(NSDictionary*)baseData;
@end
