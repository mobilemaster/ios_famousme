//
//  ChatViewController.m
//  FamousMe
//
//  Created by Shine Man on 11/23/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "ChatViewController.h"
#import "KxMenu.h"
#import "Message.h"
#import "SentTableViewCell.h"
#import "DateUtils.h"
#import "BlockUser.h"
#import "ReceivedTableViewCell.h"
#import "UserProfileViewController.h"
#import "PhotoViewController.h"
#import "MBProgressHUD.h"

#import "Reachability.h"
#import <SystemConfiguration/SystemConfiguration.h>

@import Toast;

@import IQKeyboardManager;

@interface ChatViewController () <UITableViewDelegate, UITableViewDataSource, UITextViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate>{
	NSMutableArray* messageArray;
	
	User* currentUser;
	NSString* currentUserId;
	
	BOOL isValid;
	BlockUser* blockUser;
    BOOL isFromProfile;
	
	BOOL shouldCheckUpdate;
	
	NSString* orgUsername;
	NSString* orgPhotoUrl;
	NSString* orgProfession;
	BOOL isUpdated;
	
	NSInteger showedMenuIndex;
    
    BOOL isGoToProfile;
    BOOL isTakingPhoto;
}

@end

@implementation ChatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	[[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
	[[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:YES];
	[[IQKeyboardManager sharedManager] setEnable:NO];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
	
	messageArray = [NSMutableArray array];
	self.tbMessages.delegate = self;
	self.tbMessages.dataSource = self;
	self.tbMessages.estimatedRowHeight = 147.0f;
	self.tbMessages.rowHeight = UITableViewAutomaticDimension;
	self.tvInputMessage.delegate = self;
	
	shouldCheckUpdate = NO;
	isUpdated = NO;
    isFromProfile = NO;
    isGoToProfile = NO;
    isTakingPhoto = NO;
    self.lbUserName.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(onUserProfile:)];
    [self.lbUserName addGestureRecognizer:tapGesture];
    
    [self getUserInfo];
	[self getUserStatus];
    if (![self connected]) {
        // Not connected
        [AlertViewManager showAlertView:@"Error" msg:@"Network Error" parent:self];
    } else {
        // Connected. Do some Internet stuff
    }
    [self applyTheme];
	[self getMesssages];
}

- (void)viewDidLayoutSubviews {
	[self updateContentInsetForTableView:self.tbMessages animated:NO];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    isGoToProfile = NO;
}

- (void)viewDidDisappear:(BOOL)animated {
	[super viewDidDisappear:animated];
    
    if (isGoToProfile || isTakingPhoto)
        return;
    
	[[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
	[[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:NO];
	[[IQKeyboardManager sharedManager] setEnable:YES];
		
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	
	[[FirebaseManager sharedManager] removeGetMessageObserve];
	[[FirebaseManager sharedManager] removeMonitoringBlockUserObserve];
    
    NSMutableDictionary* userInfo = [[NSMutableDictionary alloc] init];
    [userInfo setValue:[NSNumber numberWithBool:isUpdated] forKey:@"isUpdated"];
    
    if (isUpdated) {
        [userInfo setValue:currentUser.key forKey:@"userKey"];
        [userInfo setValue:currentUser.userProfile.photoUrl forKey:@"photoUrl"];
        [userInfo setValue:currentUser.userProfile.avatarUrl forKey:@"avatarUrl"];
        [userInfo setValue:currentUser.userProfile.profession forKey:@"profession"];
        [userInfo setValue:currentUser.userProfile.username forKey:@"username"];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_USERINFO_CHANGED object:userInfo];
}

- (void)applyTheme {
    [ThemeManager setThemeWithChildViews:_headerView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)connected {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

- (void)setBaseData:(NSDictionary*)baseDict {
	if ([baseDict objectForKey:@"shouldCheckUpdate"])
		shouldCheckUpdate = [[baseDict objectForKey:@"shouldCheckUpdate"] boolValue];
	
	currentUserId = [baseDict objectForKey:@"userId"];
	isValid = [[baseDict objectForKey:@"isValid"] boolValue];
	orgProfession = [baseDict objectForKey:@"profession"];
	orgUsername = [baseDict objectForKey:@"username"];
	orgPhotoUrl = [baseDict objectForKey:@"photoUrl"];
    if ([baseDict objectForKey:@"isFromProfile"])
        isFromProfile = [[baseDict objectForKey:@"isFromProfile"] boolValue];
}

- (void)getUserInfo {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
	[[FirebaseManager sharedManager] getUser:currentUserId completion:^(User* user, NSError* error) {
		if (error || !user) 
			return;
		
		if (user) {
			currentUser = user;
			self.lbUserName.text = currentUser.userProfile.username;
            
            if (user.userProfile.isValid)
                [self monitoringBlockUser];
            
            if ([self isUpdatedProfile]) {
                isUpdated = YES;
                [self.view makeToast:@"The profile was already updated by the user."];                
            }
            
		} else
			[AlertViewManager showAlertView:@"Error" msg:@"can't get the user's info" parent:self];
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
	}];
}

- (void)getUserStatus {
	[[FirebaseManager sharedManager] getUserStatus:currentUserId completion:^(Status* status, NSError* error) {
        if (error) {
            [AlertViewManager showAlertView:@"Error!" msg:error.localizedDescription parent:self];
            return;
        }
        
        if (status.isOnline)
            [self.lbStatus setTitle:@"online now" forState:UIControlStateNormal];
        else {
            NSString* onlineStatus = [NSString stringWithFormat:@"online %@", [DateUtils getGapOfTimesWithString:status.timestamp]];
            [self.lbStatus setTitle:onlineStatus forState:UIControlStateNormal];
        }
	}];
}

/**
 * Check whether the user's profile was updated or not.
 * @return result.
 */
- (BOOL)isUpdatedProfile {
    if (orgUsername.length > 0 &&
        ![orgUsername isEqualToString:currentUser.userProfile.username])
        return true;
    
    if (orgPhotoUrl.length > 0 &&
        ![orgPhotoUrl isEqualToString:currentUser.userProfile.photoUrl])
        return true;
    
    if (orgProfession.length > 0 &&
        ![orgProfession isEqualToString:currentUser.userProfile.profession])
        return true;
    
    return false;
}

- (void)getMesssages {
	[[FirebaseManager sharedManager] getMessage:currentUserId completion:^(Message* message, FIRDataEventType eventType, NSError* error) {
		if (!message)
			return;
		
        if (eventType == FIRDataEventTypeChildAdded) {
            if (messageArray.count > 0) {
                Message* firstMessage = [messageArray objectAtIndex:0];
                if (message.sentTimestamp < firstMessage.sentTimestamp )
                    [messageArray insertObject:message atIndex:0];
                else
                    [messageArray addObject:message];
            } else
                [messageArray addObject:message];
            
            //[self sortMessageArray];
        } else if (eventType == FIRDataEventTypeChildChanged){
			for (Message* item in messageArray) {
				if ([item.messageId isEqualToString:message.messageId]) {
					[item setData:message];
					item.messageId = message.messageId;
					break;
				}
			}
		} else if (eventType == FIRDataEventTypeChildRemoved) {
			for (Message* item in messageArray) {
				if ([item.messageId isEqualToString:message.messageId]) {
					[messageArray removeObject:item];
					break;
				}
			}
		}
		
		[self reloadMessages];
	}];
}


- (void)sortMessageArray {
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sentTimestamp"
                                                 ascending:YES];
    NSArray *sortedArray = [messageArray sortedArrayUsingDescriptors:@[sortDescriptor]];
    
    [messageArray removeAllObjects];
    [messageArray addObjectsFromArray:sortedArray];
}

/**
 * Check if the conversation is blocked or not.
 */
- (void)monitoringBlockUser {
	[[FirebaseManager sharedManager] monitoringBlockUser:currentUserId completion:^(BlockUser* bUser, NSError* error) {
		if (error) {
			[AlertViewManager showAlertView:@"Error!" msg:error.localizedDescription parent:self];
			return;
		}
			
		if (bUser == nil) {
			blockUser = nil;
			self.vgInputContainer.hidden = NO;
		} else  {
			if (bUser.Ids.length == 0) {
				blockUser = nil;
				self.vgInputContainer.hidden = NO;
			} else {
				blockUser = bUser;
				self.vgInputContainer.hidden = YES;
			}
		}
	}];
}

- (void)sendMessage:(MessageType)messageType fileUrl:(NSString*)fileUrl {
    if (messageType == Text && self.tvInputMessage.text.length == 0 )
        return;
    
    if (!currentUser)
        return;    
        
	NSMutableDictionary* msgDict = [[NSMutableDictionary alloc] init];
	[msgDict setValue:[FirebaseManager getCurrentUserID] forKey:@"senderId"];
	[msgDict setValue:currentUserId forKey:@"receiverId"];
	[msgDict setValue:[FIRServerValue timestamp] forKey:@"sentTimestamp"];
	[msgDict setValue:self.tvInputMessage.text forKey:@"text"];
	[msgDict setValue:@NO forKey:@"isRead"];
    [msgDict setValue:[NSNumber numberWithInteger:(NSInteger)messageType] forKey:@"messageType"];
    [msgDict setValue:fileUrl != nil? fileUrl:@"" forKey:@"imageUrl"];
    
	if (messageArray.count > 0) {
		Message* lastMessage = [messageArray objectAtIndex:messageArray.count - 1];
		int gap = [DateUtils getGapOfDate:lastMessage.sentTimestamp];
		if (gap > 0)
			[msgDict setValue:@YES forKey:@"isFirstOfDay"];
		else
			[msgDict setValue:@NO forKey:@"isFirstOfDay"];
	} else
		[msgDict setValue:@YES forKey:@"isFirstOfDay"];
	
    [[FirebaseManager sharedManager] sendMessage:msgDict message:self.tvInputMessage.text userId:currentUserId completion:^(NSError*error) {
        if (error) {
            [AlertViewManager showAlertView:@"Error!" msg:error.localizedDescription parent:self];
        }
    }];
    
    if (currentUser.discoveryRules.pushNotification) {
        [self sendPush:self.tvInputMessage.text];
    }
    
    self.tvInputMessage.text = @"";
	
	if (![BaseDataManager hasConversations]) {
		[[FirebaseManager sharedManager] updateAchievements:AchievementStartConverstation];
	}
}

/**
 * Send nudge message push notification.
 */
- (void)nudgeMessage:(id)sender {
	Message *message = [messageArray objectAtIndex:showedMenuIndex];
	[[FirebaseManager sharedManager] sendNudgeMessage:message userId:currentUserId];

    [self sendPush:message.text];
}

- (void)sendPush:(NSString*)messageText {
    User* me = [FirebaseManager sharedManager].loginedUser;
    
    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
    [params setValue:currentUser.token forKey:@"to"];
    
    NSString* title = [NSString stringWithFormat:@"%@ just send you a message", me.userProfile.username];
    NSDictionary *dataDict = @{@"title": title,
                               @"message": messageText
                               };
    
    [params setValue:dataDict forKey:@"data"];
    
    NSDictionary *notificationDict = @{@"body": messageText,
                                       @"title": title,
                                       @"content_available": @YES,
                                       @"user_key":currentUser.key
                                       };
    
    [params setValue:notificationDict forKey:@"notification"];

    
    [[FirebaseManager sharedManager] sendPush:params];
}

- (void)editMessage:(id)sender {

	UIAlertController * alert = [UIAlertController
								  alertControllerWithTitle:@"Edit Message"
								  message:nil
								  preferredStyle:UIAlertControllerStyleAlert];
	
	UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
											   handler:^(UIAlertAction * action) {
												   //Do Some action here
												   NSArray * textfields = alert.textFields;
												   UITextField * tfEditMessage = textfields[0];
												   NSString* editMessage = tfEditMessage.text;
												   
												   Message* message = [messageArray objectAtIndex:showedMenuIndex];
												   message.text = editMessage;
												   
												   [[FirebaseManager sharedManager] editMessage:currentUserId message:editMessage messageId:message.messageId];
												   
												   [self reloadMessages];
												   
												   if (showedMenuIndex == messageArray.count) {
													   Message *lastMessage = [messageArray objectAtIndex:messageArray.count - 1];
													   [[FirebaseManager sharedManager] updateConversations:lastMessage.text userId:currentUserId];
													   [[FirebaseManager sharedManager] updateDiscoveries:lastMessage.text userId:currentUserId];
												   }
											   }];
	UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
												   handler:^(UIAlertAction * action) {
													   [alert dismissViewControllerAnimated:YES completion:nil];
												   }];
	
	[alert addAction:ok];
	[alert addAction:cancel];
	
	[alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
		Message* message = [messageArray objectAtIndex:showedMenuIndex];
		textField.text = message.text;
	}];
	
	[self presentViewController:alert animated:YES completion:nil];
}

- (void)deleteMessage:(id)sender {
	Message* message = [messageArray objectAtIndex:showedMenuIndex];
	[[FirebaseManager sharedManager] removeMessage:currentUserId messageId:message.messageId];
	
	//After deleting a message, update last messages!
	if (showedMenuIndex == messageArray.count - 1) {
		Message *lastMessage = [messageArray objectAtIndex:messageArray.count - 1];
		[[FirebaseManager sharedManager] updateConversations:lastMessage.text userId:currentUserId];
		[[FirebaseManager sharedManager] updateDiscoveries:lastMessage.text userId:currentUserId];
	}
}

- (void)reloadMessages {
	[self.tbMessages reloadData];
	if (messageArray.count == 0)
		return;	
		
	NSIndexPath *indexPath = [NSIndexPath indexPathForItem:(messageArray.count - 1) inSection:0];
	[self.tbMessages scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
}

//Image Message
- (void)takePhoto {
    UIImagePickerController* picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    isTakingPhoto = YES;
    
    [self presentViewController:picker animated:YES completion:NULL];
}

- (void)selectPhoto {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    isTakingPhoto = YES;
    
    [self presentViewController:picker animated:YES completion:NULL];
}

#pragma mark - Image Picker Controller delegate methods
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];

    User* me = [FirebaseManager sharedManager].loginedUser;
    [[FirebaseManager sharedManager] sendImageMessage:me image:chosenImage competition:^(NSString *imageUrl, NSError *error) {
        if (error) {
            [AlertViewManager showAlertView:@"Error" msg:error.localizedDescription parent:self];
        } else {
            [self sendMessage:Image fileUrl:imageUrl];
        }
        
    }];

    isTakingPhoto = NO;
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    isTakingPhoto = NO;
    [picker dismissViewControllerAnimated:YES completion:NULL];
}


#pragma mark - UITableViewDelegate & DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex {
	return messageArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	Message* message = [messageArray objectAtIndex:indexPath.row];
	if ([message.senderId isEqualToString:currentUserId]) {
		static NSString *cellIdentifier = @"ReceivedTableViewCell";
		ReceivedTableViewCell *cell = (ReceivedTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		cell.tag = indexPath.row;
        if (message.messageType == Text) {
            cell.textMessageContainer.hidden = NO;
            cell.imageMessageContainer.hidden = YES;
            cell.imageMsgBottomConstraint.active = NO;
            cell.lbMessageText.text = message.text;
            cell.lbTextMsgTime.text = [DateUtils getTimeFromTimestamp:message.sentTimestamp];
        } else if (message.messageType == Image) {
            cell.textMessageContainer.hidden = YES;
            cell.imageMessageContainer.hidden = NO;
            cell.imageMsgBottomConstraint.active = YES;
            if (!cell.imageMsgBottomConstraint.active) {
                cell.imageMsgBottomConstraint = [NSLayoutConstraint
                                             constraintWithItem:cell.imageMessageContainer
                                             attribute:NSLayoutAttributeBottom
                                             relatedBy:NSLayoutRelationEqual
                                             toItem:cell.contentView
                                             attribute:NSLayoutAttributeBottom
                                             multiplier:1.0f
                                             constant:0.f];
                
                [cell.contentView addConstraint:cell.imageMsgBottomConstraint];
            }
            
            [FirebaseManager showImageWithUrl:cell.ivMessageImage imagePath:message.imageUrl];
            [cell.imageMessageContainer setBackgroundColor:[UIColor clearColor]];
            cell.lbImageMsgTime.textColor = [ThemeManager getCurrentThemeColor];
            
            cell.ivMessageImage.tag = indexPath.row;
            cell.ivMessageImage.userInteractionEnabled = YES;
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showImageMessage:)];
            [cell.ivMessageImage addGestureRecognizer:recognizer];
        }
		
		if (message.isFirstOfDay || indexPath.row == 0) {
			cell.lbDate.layer.cornerRadius = cell.lbDate.frame.size.height / 2;
			cell.lbDate.clipsToBounds = YES;
			cell.lbDate.text = [NSString stringWithFormat:@"   %@   ", [DateUtils getGapOfDateWithString:message.sentTimestamp]];
            
            if (message.messageType == Text)
                cell.messageTopConstraint.constant = 70.0f;
            else if (message.messageType == Image)
                cell.imageMsgTopConstraint.constant = 70.0f;
            
			[cell.lbDate setHidden:NO];
            [ThemeManager setTheme:cell.lbDate];
		} else {
            if (message.messageType == Text)
                cell.messageTopConstraint.constant = 10.0f;
            else if (message.messageType == Image)
                cell.imageMsgTopConstraint.constant = 10.0f;
            
			[cell.lbDate setHidden:YES];
		}
		
		UILongPressGestureRecognizer *recognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
		[cell addGestureRecognizer:recognizer];

		return cell;
	} else {
		static NSString *cellIdentifier = @"SentTableViewCell";
		SentTableViewCell *cell = (SentTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		cell.tag = indexPath.row;
		
        if (message.messageType == Text) {
            cell.textMessageContainer.hidden = NO;
            cell.imageMessageContainer.hidden = YES;
            cell.imageMsgBottomContraint.active = NO;
            cell.lbMessageText.text = message.text;
            cell.lbTextMsgTime.text = [DateUtils getTimeFromTimestamp:message.sentTimestamp];
            [ThemeManager setTheme:cell.textMessageContainer];
        } else if (message.messageType == Image) {
            cell.textMessageContainer.hidden = YES;
            cell.imageMessageContainer.hidden = NO;
            if (!cell.imageMsgBottomContraint.active) {
                cell.imageMsgBottomContraint = [NSLayoutConstraint
                                                 constraintWithItem:cell.imageMessageContainer
                                                 attribute:NSLayoutAttributeBottom
                                                 relatedBy:NSLayoutRelationEqual
                                                 toItem:cell.contentView
                                                 attribute:NSLayoutAttributeBottom
                                                 multiplier:1.0f
                                                 constant:0.f];
                
                [cell.contentView addConstraint:cell.imageMsgBottomContraint];
            }
            [FirebaseManager showImageWithUrl:cell.ivMessageImage imagePath:message.imageUrl];
            [cell.imageMessageContainer setBackgroundColor:[UIColor clearColor]];
            cell.lbImageMsgTime.textColor = [ThemeManager getCurrentThemeColor];
            
            cell.ivMessageImage.tag = indexPath.row;
            cell.ivMessageImage.userInteractionEnabled = YES;
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showImageMessage:)];
            [cell.ivMessageImage addGestureRecognizer:recognizer];
        }

		if (message.isFirstOfDay || indexPath.row == 0) {
			cell.lbDate.layer.cornerRadius = cell.lbDate.frame.size.height / 2;
			cell.lbDate.clipsToBounds = YES;
			cell.lbDate.text = [NSString stringWithFormat:@"   %@   ", [DateUtils getGapOfDateWithString:message.sentTimestamp]];

            if (message.messageType == Text)
                cell.messageTopConstraint.constant = 70.0f;
            else if (message.messageType == Image)
                cell.imageMsgTopConstraint.constant = 70.0f;
            
			[cell.lbDate setHidden:NO];
            [ThemeManager setTheme:cell.lbDate];
		} else {
            if (message.messageType == Text)
                cell.messageTopConstraint.constant = 10.0f;
            else if (message.messageType == Image)
                cell.imageMsgTopConstraint.constant = 10.0f;
			
			[cell.lbDate setHidden:YES];
		}		
		
		if (!message.isRead) {
			if (message.isNotified)
				cell.lbReadStatus.text = @"Notified again!";
			else
				cell.lbReadStatus.text = @"unread";
						
            cell.messageBottomContraint.constant = 42.0f;
			cell.lbReadStatus.hidden = NO;
            
		} else {
            cell.imageMsgBottomContraint.constant = 10.0f;
			cell.lbReadStatus.hidden = YES;			
		}
		
		UILongPressGestureRecognizer *recognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
		[cell addGestureRecognizer:recognizer];
		
		return cell;
	}
	
	return nil;
}

- (void)updateContentInsetForTableView:(UITableView *)tableView animated:(BOOL)animated {
	NSUInteger lastRow = [self tableView:tableView numberOfRowsInSection:0];
	
	NSUInteger lastIndex = lastRow > 0 ? lastRow - 1 : 0;
	
	NSIndexPath *lastIndexPath = [NSIndexPath indexPathForItem:lastIndex inSection:0];
	CGRect lastCellFrame = [self.tbMessages rectForRowAtIndexPath:lastIndexPath];
	
	// top inset = table view height - top position of last cell - last cell height
	CGFloat topInset = MAX(CGRectGetHeight(self.tbMessages.frame) - lastCellFrame.origin.y - CGRectGetHeight(lastCellFrame), 0);
	
	// What about this way? (Did not work when tested)
	// CGFloat topInset = MAX(CGRectGetHeight(self.tableView.frame) - self.tableView.contentSize.height, 0);
	
	NSLog(@"top inset: %f", topInset);
	
	UIEdgeInsets contentInset = tableView.contentInset;
	contentInset.top = topInset;
	//NSLog(@"inset: %f, %f : %f, %f", contentInset.top, contentInset.bottom, contentInset.left, contentInset.right);
	
	//NSLog(@"table height: %f", CGRectGetHeight(self.tbMessages.frame));
	
	UIViewAnimationOptions options = UIViewAnimationOptionBeginFromCurrentState;
	[UIView animateWithDuration:animated ? 0.25 : 0.0 delay:0.0 options:options animations:^{
		//self.markerHeightConstraint.constant = ABS(topInset);
		tableView.contentInset = contentInset;
	} completion:^(BOOL finished) {
	}];
}

#pragma mark - TextView delegate
- (void)textViewDidChange:(UITextView *)textView {
	NSLog(@"textViewDidBeginEditing:");
	if (textView.text.length > 0)
		[self.btnSend setTitleColor:[ThemeManager getCurrentThemeColor] forState:UIControlStateNormal];
	else
		[self.btnSend setTitleColor:NORMAL_TEXT_GREY_COLOR forState:UIControlStateNormal];
}

#pragma mark - Menu controller

- (void)longPress:(UILongPressGestureRecognizer *)recognizer {
	NSInteger index = recognizer.view.tag;
	Message* msg = [messageArray objectAtIndex:index];
	if ([msg.senderId isEqualToString:currentUserId])
		return;
	
	if (recognizer.state == UIGestureRecognizerStateBegan) {
		SentTableViewCell *cell = (SentTableViewCell *)recognizer.view;
		[cell becomeFirstResponder];
		
		showedMenuIndex = cell.tag;
		UIMenuController *menu = [UIMenuController sharedMenuController];
		UIMenuItem *nudge = [[UIMenuItem alloc] initWithTitle:@"● Nudge" action:@selector(nudgeMessage:)];
		UIMenuItem *edit = [[UIMenuItem alloc] initWithTitle:@"Edit" action:@selector(editMessage:)];
		UIMenuItem *delete = [[UIMenuItem alloc] initWithTitle:@"Delete" action:@selector(deleteMessage:)];
		if (!msg.isRead)
			[menu setMenuItems:[NSArray arrayWithObjects:nudge, edit, delete, nil]];
		else
			[menu setMenuItems:[NSArray arrayWithObjects:edit, delete, nil]];
		
		[menu setTargetRect:cell.frame inView:cell.superview];
		[menu setMenuVisible:YES animated:YES];
		[[UIMenuController sharedMenuController] update];
	}
}

- (void)showImageMessage:(UILongPressGestureRecognizer *)recognizer {
    NSInteger index = recognizer.view.tag;
    Message* msg = [messageArray objectAtIndex:index];
    PhotoViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PhotoViewController"];
    [vc setPhotoUrl:msg.imageUrl];
    [self presentViewController:vc animated:YES completion:nil];
}

- (void)showMenu:(UIButton *)sender {
	NSArray *menuItems = nil;
	NSString* myId = [FirebaseManager sharedManager].loginedUser.key;
	if (blockUser != nil && [blockUser.Ids containsString:myId]) {
		menuItems = @[
		  [KxMenuItem menuItem:@"Go to map"
						 image:[UIImage imageNamed:@"map"]
						target:self
						action:@selector(handleGoToMap:)],
		  
		  [KxMenuItem menuItem:@"Add contact"
						 image:[UIImage imageNamed:@"addContact"]
						target:self
						action:@selector(handleAddContact:)],
		  
		  [KxMenuItem menuItem:@"Unblock user"
						 image:[UIImage imageNamed:@"blockContact"]
						target:self
						action:@selector(handleUnblockUser:)],
		  ];
	} else {
		menuItems = @[
		  [KxMenuItem menuItem:@"Go to map"
						 image:[UIImage imageNamed:@"map"]
						target:self
						action:@selector(handleGoToMap:)],
		  
		  [KxMenuItem menuItem:@"Add contact"
						 image:[UIImage imageNamed:@"addContact"]
						target:self
						action:@selector(handleAddContact:)],
		  
		  [KxMenuItem menuItem:@"Block user"
						 image:[UIImage imageNamed:@"blockContact"]
						target:self
						action:@selector(handleBlockUser:)],
		  ];
	}
	
	[KxMenu setTitleFont:[UIFont fontWithName:@"Karla-Regular" size:16]];
	[KxMenu setTintColor:[UIColor whiteColor]];
	[KxMenu showMenuInView:self.view fromRect:sender.frame menuItems:menuItems];
}

- (void)keyboardWillShow:(NSNotification *)notification {
	CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
	self.bottomContraint.constant = keyboardSize.height;
	[UIView animateWithDuration:0.25 animations:^{
		[self.view layoutIfNeeded];
		[self reloadMessages];
	}];
}

- (void)keyboardWillHide:(NSNotification *)notification {
	self.bottomContraint.constant = 0;
	[UIView animateWithDuration:0.25 animations:^{
		[self.view layoutIfNeeded];
	}];
}

- (IBAction)onBack:(id)sender {
	[self dismissViewControllerAnimated:NO completion:nil];
}

- (void)handleGoToMap:(id)sender {
    if (!isFromProfile)
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_GOTO_MAP object:self];
	
    [self dismissViewControllerAnimated:NO completion:^(void) {
        if (isFromProfile)
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_GOTO_MAP object:self];
    }];
}

- (void)handleAddContact:(id)sender {
	if (![BaseDataManager isDuplicatedContact:currentUserId]) {
		[[FirebaseManager sharedManager] addContact:currentUserId];
	} else
		[self.view makeToast:@"This contact has already added!"];
}

- (void)handleBlockUser:(id)sender {
    if (messageArray == nil || messageArray.count == 0) {
        [AlertViewManager showAlertView:@"Note!" msg:@"It's not allowed because you haven't any conversation yet." parent:self];
        
        return;
    }
    
	NSString* myId = [FirebaseManager sharedManager].loginedUser.key;
	NSString* blockIds = @"";
	if (blockUser != nil && blockUser.Ids.length > 0)
		blockIds = [NSString stringWithFormat:@"%@:%@", blockUser.Ids, myId];
	else
		blockIds = myId;
	
	[[FirebaseManager sharedManager] doBlockUser:currentUserId blockIds:blockIds];
}

- (void)handleUnblockUser:(id)sender {
	NSString* blockIds = @"";
	NSString* myId = [FirebaseManager sharedManager].loginedUser.key;
	if (blockUser != nil && blockUser.Ids.length > 0) {
		if ([blockUser.Ids containsString:@":"]) {
			NSArray *ids = [blockUser.Ids componentsSeparatedByString:@":"];
			if ([ids[0] isEqualToString:myId])
				blockIds = ids[1];
			else
				blockIds = ids[0];
		}
	} else
		return;
	
	[[FirebaseManager sharedManager] doUnBlockUser:currentUserId blockIds:blockIds];
}

#pragma mark IBAction
- (IBAction)onTouchedPlus:(id)sender {
	[self showMenu:sender];
}

- (IBAction)onSendMessage:(id)sender {
    [self sendMessage:Text fileUrl:nil];
}

- (IBAction)onSendImage:(id)sender {
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:@"Take a Photo" preferredStyle:UIAlertControllerStyleActionSheet];
    
    //Cancel
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [actionSheet dismissViewControllerAnimated:YES completion:nil];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Use Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self takePhoto];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Photo Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self selectPhoto];
    }]];
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
}

- (IBAction)onUserProfile:(id)sender {
    UserProfileViewController* svc = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileViewController"];

    NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
    [dict setValue:[NSNumber numberWithBool:YES] forKey:@"isFromChat"];
    [dict setValue:currentUserId forKey:@"userId"];
    
    [svc setBaseData:dict];
    
    isGoToProfile = YES;
    [self presentViewController:svc animated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
