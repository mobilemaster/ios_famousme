//
//  InviteViewController.h
//  FamousMe
//
//  Created by Shine Man on 12/11/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InviteViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *btnInviteEmail;
@property (weak, nonatomic) IBOutlet UIButton *btnInviteSMS;
@property (weak, nonatomic) IBOutlet UIButton *btnInviteFacebook;
@property (weak, nonatomic) IBOutlet UIButton *btnInviteTwitter;
@property (weak, nonatomic) IBOutlet UIButton *btnPostLinkedIn;
@property (weak, nonatomic) IBOutlet UIButton *btnShare;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbDetails;

@end
