//
//  InviteViewController.m
//  FamousMe
//
//  Created by Shine Man on 12/11/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "InviteViewController.h"
@import TwitterKit;

@interface InviteViewController ()

@end

@implementation InviteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	
	self.btnInviteEmail.layer.cornerRadius = 8.0f;
	[self.btnInviteEmail setClipsToBounds:YES];
	
	self.btnInviteSMS.layer.cornerRadius = 8.0f;
	[self.btnInviteSMS setClipsToBounds:YES];
	
	self.btnInviteFacebook.layer.cornerRadius = 8.0f;
	[self.btnInviteFacebook setClipsToBounds:YES];

	self.btnInviteTwitter.layer.cornerRadius = 8.0f;
	[self.btnInviteTwitter setClipsToBounds:YES];

	self.btnPostLinkedIn.layer.cornerRadius = 8.0f;
	[self.btnPostLinkedIn setClipsToBounds:YES];

	self.btnShare.layer.cornerRadius = 8.0f;
	self.btnShare.layer.borderWidth = 1.0f;
	self.btnShare.layer.borderColor = MAIN_COLOR.CGColor;
	[self.btnShare setClipsToBounds:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if ([UIScreen mainScreen].bounds.size.height < 500) {
        [self.lbTitle setFont: [self.lbTitle.font fontWithSize: 16]];
        [self.lbDetails setFont: [self.lbDetails.font fontWithSize: 14]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark IBAction.
- (IBAction)onInviteByEmail:(id)sender {
}

- (IBAction)onInviteBySMS:(id)sender {
}

- (IBAction)onInviteFacebookFriends:(id)sender {
}

- (IBAction)onInviteTwitterContacts:(id)sender {
	// Check if current session has users logged in
	if ([[Twitter sharedInstance].sessionStore hasLoggedInUsers]) {
		TWTRComposerViewController *composer = [TWTRComposerViewController emptyComposer];
		[self presentViewController:composer animated:YES completion:nil];
	} else {
		[[Twitter sharedInstance] logInWithCompletion:^(TWTRSession *session, NSError *error) {
			if (session) {
				TWTRComposerViewController *composer = [TWTRComposerViewController emptyComposer];
				[self presentViewController:composer animated:YES completion:nil];
			} else {
				UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"No Twitter Accounts Available" message:@"You must log in before presenting a composer." preferredStyle:UIAlertControllerStyleAlert];
				[self presentViewController:alert animated:YES completion:nil];
			}
		}];
	}
}

- (IBAction)onPostShareLinkedin:(id)sender {
}

- (IBAction)onPostShare:(id)sender {
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
