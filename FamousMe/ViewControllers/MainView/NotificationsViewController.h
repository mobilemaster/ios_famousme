//
//  NotificationsViewController.h
//  FamousMe
//
//  Created by Shine Man on 11/22/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Notification.h"

@interface NotificationsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tbNotifications;
@property (weak, nonatomic) IBOutlet UIView *headerView;

- (void)addNotification:(Notification*)notification;
@end
