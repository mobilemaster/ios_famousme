//
//  NotificationsViewController.m
//  FamousMe
//
//  Created by Shine Man on 11/22/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "NotificationsViewController.h"
#import "NotificationTableViewCell.h"
#import "ChatViewController.h"
#import "PhotoViewController.h"

#import "DateUtils.h"

@interface NotificationsViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic) NSMutableArray* notificationsArray;
@end

@implementation NotificationsViewController
@synthesize notificationsArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	
	_tbNotifications.delegate = self;
	_tbNotifications.dataSource = self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self applyTheme];
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	
	[self checkNewNotifications];
	[self.tbNotifications reloadData];
	[[self.tabBarController.tabBar.items objectAtIndex:2] setBadgeValue:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)applyTheme {
    [ThemeManager setThemeWithChildViews:_headerView];
}

/**
 * Check if the notifications are new or not
 */
- (void)checkNewNotifications {
	for (Notification* notification in notificationsArray) {
		if (!notification.isRead) {
			[[FirebaseManager sharedManager] updateNotificationReadStatus:notification.key];
		}
	}
	
	[[BaseDataManager sharedManager] setNewNotifications:0];
}


/**
 * Add a new notification
 */
- (void)addNotification:(Notification*)notification {
	if (notificationsArray == nil)
		notificationsArray = [NSMutableArray array];
	
	if (![self isDuplicatedNotification:notification]) {
		[notificationsArray insertObject:notification atIndex:0];
		[self setUserProfilesToNotification:notification];
	} else {
		NSString* userId = [FirebaseManager sharedManager].loginedUser.key;
		[[FirebaseManager sharedManager] removeNotification:userId notificationKey:notification.key];
	}
}

/**
 * Check the duplicated notification.
 */
- (BOOL)isDuplicatedNotification:(Notification*)newNotification {
	for (Notification* notification in notificationsArray) {
		if (notification.notificationType == newNotification.notificationType &&
			[notification.userId isEqualToString:newNotification.userId]) {
			notification.timestamp = newNotification.timestamp;
			return YES;
		}
	}
	
	return NO;
}

/**
 * Set user profiles to the discovery which was added.
 */
- (void)setUserProfilesToNotification:(Notification*)notification {
	[[FirebaseManager sharedManager] getUser:notification.userId completion:^(User* user, NSError* error) {
		notification.userProfile = user.userProfile;
		[self.tbNotifications reloadData];
	}];
}

- (void)handleContactPhotoLongPress:(UILongPressGestureRecognizer*)recognizer {
	Notification* notification = [notificationsArray objectAtIndex:recognizer.view.tag];
	PhotoViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PhotoViewController"];
	[vc setPhotoUrl:notification.userProfile.photoUrl];
	[self presentViewController:vc animated:YES completion:nil];
}


#pragma mark
- (IBAction)onClearAll:(id)sender {
	[notificationsArray removeAllObjects];
	NSString* userId = [FirebaseManager sharedManager].loginedUser.key;
	[[FirebaseManager sharedManager] removeAllNotification:userId];
	[self.tbNotifications reloadData];
}

#pragma mark - UITableViewDelegate & DataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 80;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex {
	return notificationsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *cellIdentifier = @"NotificationTableViewCell";
	NotificationTableViewCell *cell = (NotificationTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	cell.ivProfilePhoto.layer.cornerRadius = cell.ivProfilePhoto.frame.size.height / 2;
	[cell.ivProfilePhoto setUserInteractionEnabled:YES];
	cell.ivProfilePhoto.clipsToBounds = YES;
	
	Notification* notification = [notificationsArray objectAtIndex:indexPath.row];
	[FirebaseManager showProfilePhoto:cell.ivProfilePhoto userProfile:notification.userProfile];
	UILongPressGestureRecognizer* photoLongPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleContactPhotoLongPress:)];
	[cell.ivProfilePhoto addGestureRecognizer:photoLongPress];
	cell.ivProfilePhoto.tag = indexPath.row;
	
	cell.lbUserName.text = notification.userProfile.username;
	cell.lbProfession.text = notification.userProfile.profession;
    if (notification.timestamp > 0)
        cell.lbTime.text = [DateUtils getGapOfTimesWithString:notification.timestamp];
		
	if (notification.notificationType == NotificationViewedProfile)
		cell.lbDescription.text = @"viewed your profile";
	else if (notification.notificationType == NotificationImportedContact)
		cell.lbDescription.text = @"Added you to contact";
	
    cell.lbDescription.textColor = [ThemeManager getCurrentThemeColor];
    
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	ChatViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatViewController"];
	
	Notification* notification = [notificationsArray objectAtIndex:indexPath.row];
	NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
	[dict setValue:[NSNumber numberWithBool:NO] forKey:@"isFromChat"];
	[dict setValue:@(YES) forKey:@"shouldCheckUpdate"];
	[dict setValue:notification.userId forKey:@"userId"];
	[dict setValue:[NSNumber numberWithBool:notification.userProfile.isValid] forKey:@"isValid"];
	[dict setValue:notification.userProfile.profession forKey:@"profession"];
	[dict setValue:notification.userProfile.username forKey:@"username"];
	[dict setValue:notification.userProfile.photoUrl forKey:@"photoUrl"];
	
	[vc setBaseData:dict];
	
	[self presentViewController:vc animated:NO completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
