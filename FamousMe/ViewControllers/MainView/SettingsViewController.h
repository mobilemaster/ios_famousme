//
//  SettingsViewController.h
//  FamousMe
//
//  Created by Shine Man on 11/22/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaddingTextField.h"
#import "BEMCheckBox.h"

@interface SettingsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIScrollView *scrScrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *headerView;

@property (weak, nonatomic) IBOutlet UIView *currentThemeView;

@property (weak, nonatomic) IBOutlet UISwitch *swAnonymousMode;
@property (weak, nonatomic) IBOutlet PaddingTextField *tfDiscoveryGender;
@property (weak, nonatomic) IBOutlet UISegmentedControl *scDiscoveryMethod;
@property (weak, nonatomic) IBOutlet PaddingTextField *tfOnlineFilter;

@property (weak, nonatomic) IBOutlet PaddingTextField *tfServiceFilter;

@property (weak, nonatomic) IBOutlet UILabel *lbDiscoveryTitle;
@property (weak, nonatomic) IBOutlet PaddingTextField *tfDiscoveryMethod;

@property (weak, nonatomic) IBOutlet PaddingTextField *tfAge;
@property (weak, nonatomic) IBOutlet PaddingTextField *tfNationality;

@property (weak, nonatomic) IBOutlet UIButton *btnInterests;

@property (weak, nonatomic) IBOutlet UISwitch *swPushNotification;

@property (weak, nonatomic) IBOutlet UISwitch *swNotifications;
@property (weak, nonatomic) IBOutlet UISwitch *swShowPhoneNumber;
@property (weak, nonatomic) IBOutlet UISwitch *swCanSeeAge;

@property (weak, nonatomic) IBOutlet UIView *vgSelectInterests;
@property (weak, nonatomic) IBOutlet UITableView *tbInterestsList;
@property (weak, nonatomic) IBOutlet UIButton *btnInterestsOk;

@end
