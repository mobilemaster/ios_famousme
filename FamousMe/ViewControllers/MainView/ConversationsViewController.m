//
//  ConversationsViewController.m
//  FamousMe
//
//  Created by Shine Man on 11/22/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "ConversationsViewController.h"
#import "DiscoveryTableViewCell.h"
#import "ConversationTableViewCell.h"
#import "ContactTableViewCell.h"
#import "ChatViewController.h"
#import "UserProfileViewController.h"
#import "PhotoViewController.h"

#import "User.h"
#import "Contact.h"
#import "DateUtils.h"
#import "WelcomeMessageView.h"

#import <FirebaseStorageUI/FirebaseStorageUI.h>

@interface ConversationsViewController ()<UITableViewDelegate, UITableViewDataSource, UISearchDisplayDelegate, UISearchResultsUpdating> {
	
	int countOfUnreadMessages;
	UIRefreshControl* refreshControl;
	UISearchController* conversationSearchController;
	UISearchController* discoverySearchController;
	UISearchController* contactSearchController;
	
	WelcomeMessageView* welcomeMsgView;
    //NSMutableDictionary *cellHeightsDictionary;
	
	BOOL isConversationSearchMode;
    BOOL isDiscoverySearchMode;
    BOOL isContactSearchMode;
    
    //For Theme
    TopTabBar * ttbCurrentTab;
}

@property (strong, nonatomic) NSMutableArray* conversationsArray;
@property (strong, nonatomic) NSMutableArray* discoveriesArray;
@property (strong, nonatomic) NSMutableArray* contactsArray;

@property (strong, nonatomic) NSMutableArray* searchConversationsArray;
@property (strong, nonatomic) NSMutableArray* searchDiscoveriesArray;
@property (strong, nonatomic) NSMutableArray* searchContactsArray;
@end

@implementation ConversationsViewController
@synthesize conversationsArray;
@synthesize discoveriesArray;
@synthesize contactsArray;
@synthesize searchConversationsArray;
@synthesize searchDiscoveriesArray;
@synthesize searchContactsArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	conversationsArray = [NSMutableArray array];
	discoveriesArray = [NSMutableArray array];
	contactsArray = [NSMutableArray array];
	
	searchConversationsArray = [NSMutableArray array];
	searchDiscoveriesArray = [NSMutableArray array];
	searchContactsArray = [NSMutableArray array];
	countOfUnreadMessages = 0;
	
	_tbConversations.delegate = self;
	_tbConversations.dataSource = self;
	conversationSearchController = [[UISearchController alloc] initWithSearchResultsController:nil];
	[conversationSearchController.searchBar sizeToFit];
	conversationSearchController.searchResultsUpdater = self;
	conversationSearchController.dimsBackgroundDuringPresentation = NO;
	conversationSearchController.searchBar.placeholder = @"Search";
	conversationSearchController.searchBar.tag = 1001;
	conversationSearchController.searchResultsUpdater = self;
	_tbConversations.tableHeaderView = conversationSearchController.searchBar;
	
	discoverySearchController = [[UISearchController alloc] initWithSearchResultsController:nil];
	[discoverySearchController.searchBar sizeToFit];
	discoverySearchController.searchResultsUpdater = self;
	discoverySearchController.dimsBackgroundDuringPresentation = NO;
	discoverySearchController.searchBar.placeholder = @"Search";
	discoverySearchController.searchBar.tag = 1002;
	discoverySearchController.searchResultsUpdater = self;

	_tbDiscoveries.tableHeaderView = discoverySearchController.searchBar;

	contactSearchController = [[UISearchController alloc] initWithSearchResultsController:nil];
	[contactSearchController.searchBar sizeToFit];
	contactSearchController.searchResultsUpdater = self;
	contactSearchController.dimsBackgroundDuringPresentation = NO;
	contactSearchController.searchBar.placeholder = @"Search";
	contactSearchController.searchResultsUpdater = self;
	contactSearchController.searchBar.tag = 1003;
	_tbContacts.tableHeaderView = contactSearchController.searchBar;
	self.definesPresentationContext = YES;

	_tbContacts.delegate = self;
	_tbContacts.dataSource = self;
	_tbDiscoveries.dataSource = self;
	_tbDiscoveries.delegate = self;
    
    self.btnGotoMap.layer.cornerRadius = 8.0f;
    [self.btnGotoMap setClipsToBounds:YES];
    
	[self initialize];
	[self getConversations];
	//[self getDiscoveries];
	[self getContacts];
	[self performSelector:@selector(showWelcomeBackMessage)
			   withObject:self
			   afterDelay:3.0f];
	
    isConversationSearchMode = NO;
    isDiscoverySearchMode = NO;
	isContactSearchMode = NO;
    
    [_tbConversations setContentOffset:CGPointMake(0.0, _tbConversations.tableHeaderView.frame.size.height) animated:YES];
    [_tbDiscoveries setContentOffset:CGPointMake(0.0, _tbDiscoveries.tableHeaderView.frame.size.height) animated:YES];
    [_tbContacts setContentOffset:CGPointMake(0.0, _tbContacts.tableHeaderView.frame.size.height) animated:YES];
    [self.searchDisplayController setActive:NO animated:YES];
 }


- (void)viewWillAppear:(BOOL)animated {
	[super viewDidAppear:animated];
    
    [self applyTheme];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)applyTheme {
    [ThemeManager setThemeWithChildViews:_headerView];
    [ThemeManager setTheme:_btnGotoMap];
    
    if (ttbCurrentTab)
        [ttbCurrentTab setSelected:YES];
    
    //Set theme of TabBar
    [self.tabBarController.tabBar setTintColor:[ThemeManager getCurrentThemeColor]];
}

- (void)showWelcomeBackMessage {
    BOOL showWelcomeMessage = [[NSUserDefaults standardUserDefaults] boolForKey:@"isHaveAccount"];
    if (!showWelcomeMessage)
        return;

	welcomeMsgView = [[[NSBundle mainBundle] loadNibNamed:@"WelcomeMessageView" owner:self options:nil] objectAtIndex:0];
	
	CGRect bounds = [UIScreen mainScreen].bounds;
	welcomeMsgView.frame = CGRectMake(0, -90, bounds.size.width, 90);
    [ThemeManager setThemeWithChildViews:welcomeMsgView];
	[self.view addSubview:welcomeMsgView];
	
	if (countOfUnreadMessages > 0)
		welcomeMsgView.lbNewMessage.text = [NSString stringWithFormat:@"%d new messages", countOfUnreadMessages];
	else
		welcomeMsgView.lbNewMessage.hidden = YES;
	
	User* user = [FirebaseManager sharedManager].loginedUser;
	welcomeMsgView.lbWelcomeTitle.text = [NSString stringWithFormat:@"Welcome back %@", user.userProfile.username];
	
	[UIView animateWithDuration:1.0f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^(void) {
		welcomeMsgView.frame = CGRectMake(0, 0, bounds.size.width, 90);
	}  completion:^(BOOL finished) {
		[self dismissWelcomeBackMessage];
	}];
	
	[[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isHaveAccount"];
}

- (void)dismissWelcomeBackMessage {
	[UIView animateWithDuration:1.0f delay:3.0f options:UIViewAnimationOptionCurveEaseInOut animations:^(void) {
		welcomeMsgView.frame = CGRectMake(0, -90, welcomeMsgView.frame.size.width, 90);
	}  completion:^(BOOL finished) {
		[welcomeMsgView removeFromSuperview];
	}];
}

- (void)initialize {
	_vPhotoFrame.layer.cornerRadius = _vPhotoFrame.frame.size.height/2;
	_ivProfilePhoto.layer.cornerRadius = _ivProfilePhoto.frame.size.height/2;
	_ivProfilePhoto.clipsToBounds = YES;
	
	UITapGestureRecognizer* photoTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleProfilePhotoTap:)];
	[self.vPhotoFrame addGestureRecognizer:photoTap];
	
	UILongPressGestureRecognizer* photoLongPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleProfilePhotoLongPress:)];
	[self.vPhotoFrame addGestureRecognizer:photoLongPress];
	
	UITapGestureRecognizer* conversationsTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleConversationsTap:)];
	[self.ttbConversations addGestureRecognizer:conversationsTap];
	
	UITapGestureRecognizer* discoveriesTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDiscoveriesTap:)];
	[self.ttbDiscoveries addGestureRecognizer:discoveriesTap];
	
	UITapGestureRecognizer* contactsTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleContactsTap:)];
	[self.ttbContacts addGestureRecognizer:contactsTap];
	
	[self handleConversationsTap:nil];
}

- (void)setUserProfile {
	User* user = [FirebaseManager sharedManager].loginedUser;
	self.lbName.text = user.userProfile.username;
	if (user.userProfile.avatarUrl.length == 0 &&
        user.userProfile.photoUrl.length == 0)
		return;
	
    [FirebaseManager showProfilePhoto:self.ivProfilePhoto userProfile:user.userProfile];
}

- (void)reloadConversations {
    if (conversationsArray.count > 0)
        self.vgNoConversation.hidden = YES;
    else
        self.vgNoConversation.hidden = NO;
    
    [[BaseDataManager sharedManager] setConversationCount:(int)conversationsArray.count];
    
	[self.ttbConversations setCountString:[NSString stringWithFormat:@"%d", (int)conversationsArray.count]];
	[self.tbConversations reloadData];
}

- (void)getConversations {
	[[FirebaseManager sharedManager] getConversations:^(Conversation* conversation, BOOL isChanged, NSError* error) {
		if (conversation == nil)
			return;
		
		if (!isChanged) {
			if (!conversation.isRead && ![conversation.lastMessage containsString:@"You:"])
				countOfUnreadMessages++;
			
			[conversationsArray insertObject:conversation atIndex:0];
			[self reloadConversations];
		} else {
			for (Conversation* item in conversationsArray) {
				if ([conversation.conversationId isEqualToString:item.conversationId]) {
					conversation.userProfile = item.userProfile;
					conversation.conversationId = item.conversationId;
					[conversationsArray removeObject:item];
					[conversationsArray insertObject:conversation atIndex:0];
					break;
				}
			}
			
			[self reloadConversations];
		}
	}];
}

- (void)refreshDiscoveries {
    [discoveriesArray removeAllObjects];
    [self reloadDiscoveries];
    //[self getDiscoveries];
}

/**
 * Add discovery with user that got from country filter.[26/03/2018]
 */
- (void)addDiscovery:(User*)user currentLocation:(CLLocation*)currentLocation {
    CLLocation* userLocation = nil;
    if (user.position)
        userLocation = [[CLLocation alloc] initWithLatitude:user.position.latitude longitude:user.position.longitude];
    
    [self addDiscovery:user userLocation:userLocation currentLocation:currentLocation];
}

- (void)addDiscovery:(User*)user userLocation:(CLLocation*)userLocation
                    currentLocation:(CLLocation*)currentLocation {
    Discovery* discovery = [[Discovery alloc] init];
    discovery.userKey = user.key;
    if (userLocation)
        discovery.distance = [userLocation distanceFromLocation:currentLocation];
    else
        discovery.distance = -1;
    
    discovery.timestamp = [[NSDate date] timeIntervalSince1970] * 1000;
    
    if (user.userProfile.isValid) {
        discovery.userProfile = user.userProfile;
        discovery.status = user.status;
        [discoveriesArray addObject:discovery];
        
        [self sortDiscoveryArray];
        [self reloadDiscoveries];
    }    
}

- (void)sortDiscoveryArray {
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"distance"
                                                 ascending:YES];
    NSArray *sortedArray = [discoveriesArray sortedArrayUsingDescriptors:@[sortDescriptor]];
    
    [discoveriesArray removeAllObjects];
    discoveriesArray = [NSMutableArray arrayWithArray:sortedArray];
}

- (void)removeAllDiscoveries {
    [discoveriesArray removeAllObjects];
    [self.tbDiscoveries reloadData];
}

- (void)reloadDiscoveries {
	[self.ttbDiscoveries setCountString:[NSString stringWithFormat:@"%d", (int)discoveriesArray.count]];
	[self.tbDiscoveries reloadData];
}

- (void)getContacts {
	[[FirebaseManager sharedManager] getContacts:^(Contact* contact, NSError* error) {
		if (contact == nil)
			return;
		
		[contactsArray insertObject:contact atIndex:0];
		if (![BaseDataManager isDuplicatedContact:contact.userId])
			[[BaseDataManager sharedManager] addContact:contact];
		
		[self reloadContacts];
	}];
}

- (void)reloadContacts {
	[self.ttbContacts setCountString:[NSString stringWithFormat:@"%d", (int)contactsArray.count]];
	[self.tbContacts reloadData];
}

/**
 * If the user profile is updated, reset the user profile with the data.
 * @param data changed data notification.
 */
- (void)updateUserProfile:(NSNotification*)data {
	NSMutableDictionary* userInfo = (NSMutableDictionary*)[data object];
	
	BOOL isUpdated = [[userInfo objectForKey:@"isUpdated"] boolValue];
	if (!isUpdated)
		return;
	
	NSString* userKey = [userInfo objectForKey:@"userKey"];
	NSString* username = [userInfo objectForKey:@"username"];
	NSString* photoUrl = [userInfo objectForKey:@"photoUrl"];
    NSString* avatarUrl = [userInfo objectForKey:@"avatarUrl"];
	NSString* profession = [userInfo objectForKey:@"profession"];
	
	for (Discovery* discovery in discoveriesArray) {
		if ([discovery.userKey isEqualToString:userKey]) {
			discovery.userProfile.username = username;
			discovery.userProfile.photoUrl = photoUrl;
            discovery.userProfile.avatarUrl = avatarUrl;
			discovery.userProfile.profession = profession;
			break;
		}
	}
	
	[self.tbDiscoveries reloadData];
	
	for (Conversation* conversation in conversationsArray) {
		if ([conversation.userId isEqualToString:userKey]) {
			conversation.userProfile.username = username;
			conversation.userProfile.photoUrl = photoUrl;
            conversation.userProfile.avatarUrl = avatarUrl;
			conversation.userProfile.profession = profession;
			break;
		}
	}
	
	[self.tbConversations reloadData];
	
	for (Contact* contact in contactsArray) {
		if ([contact.userId isEqualToString:userKey]) {
			contact.userProfile.username = username;
			contact.userProfile.photoUrl = photoUrl;
            contact.userProfile.avatarUrl = avatarUrl;
			contact.userProfile.profession = profession;
			break;
		}
	}
	
	[self.tbContacts reloadData];

	[[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_USERINFO_CHANGED object:nil];
}

- (void)handleProfilePhotoTap:(UITapGestureRecognizer *)recognizer {
	[self performSegueWithIdentifier:@"ConversationToMyProfile" sender:self];
}

- (void)handleProfilePhotoLongPress:(UILongPressGestureRecognizer*)recognizer {
	User* user = [FirebaseManager sharedManager].loginedUser;
	PhotoViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PhotoViewController"];
	[vc setPhotoUrl:user.userProfile.photoUrl];
	[self presentViewController:vc animated:YES completion:nil];
}

- (void)handleConversationLongPress:(UILongPressGestureRecognizer*)recognizer {
	Conversation* conversation = [conversationsArray objectAtIndex:recognizer.view.tag];
	PhotoViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PhotoViewController"];
	[vc setPhotoUrl:conversation.userProfile.photoUrl];
	[self presentViewController:vc animated:YES completion:nil];
}

- (void)handleDiscoveryPhotoLongPress:(UILongPressGestureRecognizer*)recognizer {
	Discovery* discovery = [discoveriesArray objectAtIndex:recognizer.view.tag];
	PhotoViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PhotoViewController"];
	[vc setPhotoUrl:discovery.userProfile.photoUrl];
	[self presentViewController:vc animated:YES completion:nil];
}

- (void)handleContactPhotoLongPress:(UILongPressGestureRecognizer*)recognizer {
	Contact* contact = [contactsArray objectAtIndex:recognizer.view.tag];
	PhotoViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PhotoViewController"];
	[vc setPhotoUrl:contact.userProfile.photoUrl];
	[self presentViewController:vc animated:YES completion:nil];
}

- (IBAction)onTouchGotoMap:(id)sender {
    [self.tabBarController setSelectedIndex:1];
}

#pragma mark - UITableViewDelegate & DataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 80;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex {
    if (tableView == _tbConversations) {
        if (isConversationSearchMode)
            return searchConversationsArray.count;
        else
            return conversationsArray.count;
    } else if (tableView == _tbDiscoveries) {
        if (isDiscoverySearchMode)
            return searchDiscoveriesArray.count;
        else
            return discoveriesArray.count;
    } else if (tableView == _tbContacts) {
        if (isContactSearchMode)
            return searchContactsArray.count;
        else
            return contactsArray.count;
    }
    
	return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	if (tableView == _tbConversations) {
		static NSString * convCellIdentifier = @"ConversationTableViewCell";
		ConversationTableViewCell* conversationCell = (ConversationTableViewCell*)[tableView dequeueReusableCellWithIdentifier:convCellIdentifier];
		
		conversationCell.ivProfilePhoto.layer.cornerRadius = conversationCell.ivProfilePhoto.frame.size.height/2;
		[conversationCell.ivProfilePhoto setClipsToBounds:YES];
		conversationCell.selectionStyle = UITableViewCellSelectionStyleNone;
		
        Conversation* conversation = nil;
        if (isConversationSearchMode)
            conversation = [searchConversationsArray objectAtIndex:indexPath.row];
        else
            conversation = [conversationsArray objectAtIndex:indexPath.row];
        
		if (!conversation.isRead && ![conversation.lastMessage containsString:@"You:"]) {
			conversationCell.lbLastMessage.textColor = [UIColor blackColor];
			conversationCell.backgroundColor = LIGHT_DARK_BACKGROUND_COLOR;
		} else {
			conversationCell.lbLastMessage.textColor = NORMAL_TEXT_GREY_COLOR;
			conversationCell.backgroundColor = [UIColor whiteColor];
		}
        
		[FirebaseManager showProfilePhoto:conversationCell.ivProfilePhoto userProfile:conversation.userProfile];
		UILongPressGestureRecognizer* photoLongPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleConversationLongPress:)];
		[conversationCell.ivProfilePhoto addGestureRecognizer:photoLongPress];
		conversationCell.ivProfilePhoto.tag = indexPath.row;
		
		conversationCell.lbUserName.text = conversation.userProfile.username;
        if (conversation.timestamp > 0)
            conversationCell.lbTime.text = [DateUtils getGapOfTimesWithString:conversation.timestamp];

		conversationCell.lbProfession.text = conversation.userProfile.profession;
		if (conversation.lastMessage.length > 0)
			conversationCell.lbLastMessage.text = conversation.lastMessage;
		else
			conversationCell.lbLastMessage.text = @"";
		
		return conversationCell;
	} else if (tableView == _tbDiscoveries) {
		static NSString *disCellIdentifier = @"DiscoveryTableViewCell";
		DiscoveryTableViewCell* discoveryCell = (DiscoveryTableViewCell*)[_tbDiscoveries dequeueReusableCellWithIdentifier:disCellIdentifier];
		discoveryCell.ivProfilePhoto.layer.cornerRadius = discoveryCell.ivProfilePhoto.frame.size.height/2;
		[discoveryCell.ivProfilePhoto setClipsToBounds:YES];
		discoveryCell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        Discovery* discovery = nil;
        if (isDiscoverySearchMode)
            discovery = [searchDiscoveriesArray objectAtIndex:indexPath.row];
        else
            discovery = [discoveriesArray objectAtIndex:indexPath.row];
        
		[FirebaseManager showProfilePhoto:discoveryCell.ivProfilePhoto userProfile:discovery.userProfile];
		UILongPressGestureRecognizer* photoLongPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleDiscoveryPhotoLongPress:)];
		[discoveryCell.ivProfilePhoto addGestureRecognizer:photoLongPress];
		discoveryCell.ivProfilePhoto.tag = indexPath.row;
		
		discoveryCell.lbUserName.text = discovery.userProfile.username;
		discoveryCell.lbProfession.text = discovery.userProfile.profession;
        
        if (discovery.status.isOnline) {
            discoveryCell.lbTime.text = @"online now";
            discoveryCell.lbTime.textColor = MAIN_COLOR;
        }
        else {
            discoveryCell.lbTime.textColor = CONTENT_TEXT_COLOR;
            discoveryCell.lbTime.text = [DateUtils getGapOfTimesWithString:discovery.status.timestamp];
        }

        if (discovery.distance >= 0)
            discoveryCell.lbDistance.text = [BaseDataManager getDistanceString:discovery.distance];
        else
            discoveryCell.lbDistance.text = @"";
        
        
		return discoveryCell;
		
	} else if (tableView == _tbContacts) {
		static NSString *conCellIdentifier = @"ContactTableViewCell";
		ContactTableViewCell* contactCell = (ContactTableViewCell*)[tableView dequeueReusableCellWithIdentifier:conCellIdentifier];
		contactCell.selectionStyle = UITableViewCellSelectionStyleNone;
		
		contactCell.ivProfilePhoto.layer.cornerRadius = contactCell.ivProfilePhoto.frame.size.height/2;
		contactCell.ivProfilePhoto.clipsToBounds = YES;
		
        Contact* contact = nil;
        if (isContactSearchMode)
            contact = [searchContactsArray objectAtIndex:indexPath.row];
        else
            contact = [contactsArray objectAtIndex:indexPath.row];
        
        [FirebaseManager showProfilePhoto:contactCell.ivProfilePhoto userProfile:contact.userProfile];
		[contactCell.ivProfilePhoto setUserInteractionEnabled:YES];
		UILongPressGestureRecognizer* photoLongPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleContactPhotoLongPress:)];
		[contactCell.ivProfilePhoto addGestureRecognizer:photoLongPress];
		contactCell.ivProfilePhoto.tag = indexPath.row;
		
		contactCell.lbUserName.text = contact.userProfile.username;
		contactCell.lbProfession.text = contact.userProfile.profession;
		
		return contactCell;
	}
	
	return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	if (tableView == _tbConversations) {
		ChatViewController* svc = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatViewController"];

		Conversation* conversation = nil;
		if (isConversationSearchMode)
			conversation = [searchConversationsArray objectAtIndex:indexPath.row];
		else
			conversation = [conversationsArray objectAtIndex:indexPath.row];
		 
		NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
		[dict setValue:@YES forKey:@"shouldCheckUpdate"];
		[dict setValue:conversation.userId forKey:@"userId"];
		[dict setValue:[NSNumber numberWithBool:conversation.userProfile.isValid] forKey:@"isValid"];
		[dict setValue:conversation.userProfile.profession forKey:@"profession"];
		[dict setValue:conversation.userProfile.username forKey:@"username"];
		[dict setValue:conversation.userProfile.photoUrl forKey:@"photoUrl"];
		
        NSLog(@"User Name is %@", conversation.userProfile.username);
		[svc setBaseData:dict];
		
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUserProfile:) name:NOTIFICATION_USERINFO_CHANGED object:nil];
        
		[self presentViewController:svc animated:YES completion:nil];
	} else if (tableView == _tbDiscoveries) {
		UserProfileViewController* svc = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileViewController"];
		
		Discovery* discovery = nil;
		if (isDiscoverySearchMode)
			discovery = [searchDiscoveriesArray objectAtIndex:indexPath.row];
		else
			discovery = [discoveriesArray objectAtIndex:indexPath.row];
		
		//NSString* timeGap = [DateUtils getGapOfTimesWithString:discovery.timestamp];
		NSString* distance = [BaseDataManager getDistanceString:discovery.distance];
		//NSString* status = [NSString stringWithFormat:@"%@", distance];
		
		[[FirebaseManager sharedManager] sendNotification:discovery.userKey notificationType:NotificationViewedProfile];
		
		NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
		[dict setValue:[NSNumber numberWithBool:NO] forKey:@"isFromChat"];
		[dict setValue:@(YES) forKey:@"shouldCheckUpdate"];
		[dict setValue:distance forKey:@"status"];
		[dict setValue:discovery.userKey forKey:@"userId"];
		[dict setValue:[NSNumber numberWithBool:discovery.userProfile.isValid] forKey:@"isValid"];
		[dict setValue:discovery.userProfile.profession forKey:@"profession"];
		[dict setValue:discovery.userProfile.username forKey:@"username"];
		[dict setValue:discovery.userProfile.photoUrl forKey:@"photoUrl"];
        [dict setValue:discovery.userProfile.avatarUrl forKey:@"avatarUrl"];
		
		[svc setBaseData:dict];
		
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUserProfile:) name:NOTIFICATION_USERINFO_CHANGED object:nil];
		
		[self presentViewController:svc animated:YES completion:nil];
		
		//[self performSegueWithIdentifier:@"DiscoveryToUserProfile" sender:self];
	} else if (tableView == _tbContacts) {
		ChatViewController* svc = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatViewController"];
		
		Contact* contact = nil;
		if (isContactSearchMode)
			contact = [searchContactsArray objectAtIndex:indexPath.row];
		else
			contact = [contactsArray objectAtIndex:indexPath.row];
			
		NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
		[dict setValue:contact.userId forKey:@"userId"];
		[dict setValue:[NSNumber numberWithBool:contact.userProfile.isValid] forKey:@"isValid"];
		[dict setValue:contact.userProfile.username forKey:@"username"];
		
		[svc setBaseData:dict];

		[self presentViewController:svc animated:YES completion:nil];
	}
}

#pragma mark - UISearchResultsUpdating

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
	NSString *searchText = [searchController.searchBar text];
	
	if (searchController.searchBar.tag == 1001) {
		[searchConversationsArray removeAllObjects];
		if (searchText.length > 0) {
			for (Conversation *conversation in conversationsArray) {
				if ([conversation.userProfile.username containsString:searchText] ||
					[conversation.userProfile.profession containsString:searchText])
					
					[searchConversationsArray addObject:conversation];
			}
			
			isConversationSearchMode = YES;
		} else {
			isConversationSearchMode = NO;
		}

		[self.tbConversations reloadData];
	} else if (searchController.searchBar.tag == 1002) {
		[searchDiscoveriesArray removeAllObjects];
		if (searchText.length > 0) {
			for (Discovery *discovery in discoveriesArray) {
				if ([discovery.userProfile.username containsString:searchText] ||
					[discovery.userProfile.profession containsString:searchText])
					
					[searchDiscoveriesArray addObject:discovery];
			}
			
			isDiscoverySearchMode = YES;
		} else {
			isDiscoverySearchMode = NO;
		}
		
		[self.tbDiscoveries reloadData];
	} else if (searchController.searchBar.tag == 1003) {
		[searchContactsArray removeAllObjects];
		if (searchText.length > 0) {
			for (Contact *contact in contactsArray) {
				if ([contact.userProfile.username containsString:searchText] ||
					[contact.userProfile.profession containsString:searchText])
					
					[searchContactsArray addObject:contact];
			}
			
			isContactSearchMode = YES;
		} else {
			isContactSearchMode = NO;
		}	
		
		[self.tbContacts reloadData];
	}
}

#pragma mark - TopTabBar

- (void)handleConversationsTap:(UITapGestureRecognizer *)recognizer {
	_vgConversationsGroup.hidden = NO;
	_vgDiscoveriesGroup.hidden = YES;
	_vgContactsGroup.hidden = YES;
	
	[_ttbConversations setSelected:YES];
	[_ttbDiscoveries setSelected:NO];
	[_ttbContacts setSelected:NO];
    
    conversationSearchController.searchBar.hidden = NO;
    discoverySearchController.searchBar.hidden = YES;
    contactSearchController.searchBar.hidden = YES;
    [discoverySearchController.searchBar resignFirstResponder];
    [contactSearchController.searchBar resignFirstResponder];
    
    ttbCurrentTab = _ttbConversations;
}

- (void)handleDiscoveriesTap:(UITapGestureRecognizer *)recognizer {
	_vgDiscoveriesGroup.hidden = NO;
	_vgConversationsGroup.hidden = YES;
	_vgContactsGroup.hidden = YES;
	
	[_ttbDiscoveries setSelected:YES];
	[_ttbConversations setSelected:NO];
	[_ttbContacts setSelected:NO];
    
    conversationSearchController.searchBar.hidden = YES;
    discoverySearchController.searchBar.hidden = NO;
    contactSearchController.searchBar.hidden = YES;
    [conversationSearchController.searchBar resignFirstResponder];
    [contactSearchController.searchBar resignFirstResponder];
    
    ttbCurrentTab = _ttbDiscoveries;
}

- (void)handleContactsTap:(UITapGestureRecognizer *)recognizer {
	_vgContactsGroup.hidden = NO;
	_vgConversationsGroup.hidden = YES;
	_vgDiscoveriesGroup.hidden = YES;
	
	[_ttbContacts setSelected:YES];
	[_ttbConversations setSelected:NO];
	[_ttbDiscoveries setSelected:NO];
    
    conversationSearchController.searchBar.hidden = YES;
    discoverySearchController.searchBar.hidden = YES;
    contactSearchController.searchBar.hidden = NO;
    [conversationSearchController.searchBar resignFirstResponder];
    [contactSearchController.searchBar resignFirstResponder];
    
    ttbCurrentTab = _ttbContacts;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
