//
//  MapViewController.h
//  FamousMe
//
//  Created by Shine Man on 11/22/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>

@interface MapViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *btnAction;
@property (weak, nonatomic) IBOutlet UIView *headerView;

@property (weak, nonatomic) IBOutlet UIView *vgMapLoadingView;

@property (weak, nonatomic) IBOutlet UIView *vgLocationList;
@property (weak, nonatomic) IBOutlet UITableView *tbLocations;
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UIView *vgViewProfile;
@property (weak, nonatomic) IBOutlet UIImageView *ivUserPhoto;
@property (weak, nonatomic) IBOutlet UILabel *lbUserName;
@property (weak, nonatomic) IBOutlet UILabel *lbProfession;
@property (weak, nonatomic) IBOutlet UILabel *lbOnlineStatus;
@property (weak, nonatomic) IBOutlet UILabel *lbDistanceStatus;
@property (weak, nonatomic) IBOutlet UIButton *btnViewProfile;
@property (weak, nonatomic) IBOutlet UIView *vgNobody;
@property (weak, nonatomic) IBOutlet UIButton *btnInviteFriends;
@property (weak, nonatomic) IBOutlet UIImageView *ivLoading;
@property (weak, nonatomic) IBOutlet UIImageView *ivNoLoading;

- (void)updateUserLocation:(CLLocation*)location;
- (void)refreshMapLocation;
- (void)addNearUser:(User*)user userLocation:(CLLocation*)userLocation;
@end
