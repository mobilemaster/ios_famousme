//
//  ConversationsViewController.h
//  FamousMe
//
//  Created by Shine Man on 11/22/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TopTabBar.h"

@interface ConversationsViewController : UIViewController
//Header
@property (weak, nonatomic) IBOutlet UILabel *lbName;
@property (weak, nonatomic) IBOutlet UIView *vPhotoFrame;
@property (weak, nonatomic) IBOutlet UIImageView *ivProfilePhoto;
@property (weak, nonatomic) IBOutlet UIView *headerView;

@property (weak, nonatomic) IBOutlet UIView *vgConversationsGroup;
@property (weak, nonatomic) IBOutlet TopTabBar *ttbConversations;
@property (weak, nonatomic) IBOutlet UITableView *tbConversations;

@property (weak, nonatomic) IBOutlet UIView *vgDiscoveriesGroup;
@property (weak, nonatomic) IBOutlet TopTabBar *ttbDiscoveries;
@property (weak, nonatomic) IBOutlet UITableView *tbDiscoveries;

@property (weak, nonatomic) IBOutlet UIView *vgContactsGroup;
@property (weak, nonatomic) IBOutlet TopTabBar *ttbContacts;
@property (weak, nonatomic) IBOutlet UITableView *tbContacts;
@property (weak, nonatomic) IBOutlet UIView *vgNoConversation;
@property (weak, nonatomic) IBOutlet UIButton *btnGotoMap;

- (void)setUserProfile;
- (void)refreshDiscoveries;
- (void)removeAllDiscoveries;
- (void)addDiscovery:(User*)user currentLocation:(CLLocation*)currentLocation;
- (void)addDiscovery:(User*)user userLocation:(CLLocation*)userLocation currentLocation:(CLLocation*)currentLocation;
@end
