//
//  MainViewController.m
//  FamousMe
//
//  Created by Shine Man on 11/22/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "MainViewController.h"
#import "ConversationsViewController.h"
#import "NotificationsViewController.h"
#import "MapViewController.h"
#import "SettingsViewController.h"
#import "LogoViewController.h"
#import "NudgeToastView.h"
#import "MBProgressHUD.h"

#define  MAX_GET_DISCOVERIES_SKIP_TIME              100

@interface MainViewController () <CLLocationManagerDelegate> {
	ConversationsViewController* conversationViewController;
	NotificationsViewController* notificationViewController;
	MapViewController* mapViewController;
	SettingsViewController* settingsViewController;
	
    CLLocation *currentLocation;
	NudgeToastView* nudgeToastView;
	NSTimer* updateLocationTimer;
	
	BOOL isGotUserInfo;
	BOOL isChangedRules;
	
	BOOL isStoppedLocationManager;
	
	BOOL shouldGoToMap;
    
    NSUInteger gotDiscoveriesTimes;
    NSUInteger contryFilterTimes; //only once time.
    
    BOOL isLogout;
    
    MBProgressHUD* progressHUD;
}

@property (strong, nonatomic) CLLocationManager *locationManager;;
@end

@implementation MainViewController
@synthesize locationManager;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	isGotUserInfo = NO;
	isChangedRules = NO;
    gotDiscoveriesTimes = 0;
    contryFilterTimes = 0;
    isLogout = NO;
	
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
	[self getUserInfo];
    
	isStoppedLocationManager = YES;
	locationManager = [[CLLocationManager alloc] init];
	//[self getAllDiscoveries];
	
	shouldGoToMap = NO;
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goToMap:) name:NOTIFICATION_GOTO_MAP object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(runningExit:) name:NOTIFICATION_LOG_OUT object:nil];
			
	locationManager.delegate = self;
	locationManager.desiredAccuracy = kCLLocationAccuracyBest;
	//locationManager.distanceFilter = 50;
	
	[locationManager requestAlwaysAuthorization];
    dispatch_async(dispatch_get_main_queue(), ^{
        //Your main thread code goes in here
        [self checkDiscoverableMap];
    });
    
    [[FirebaseManager sharedManager] updateUserToken];
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	
	conversationViewController = (ConversationsViewController*)[self.viewControllers objectAtIndex:0] ;
	mapViewController = (MapViewController*)[self.viewControllers objectAtIndex:1];
	notificationViewController = (NotificationsViewController*)[self.viewControllers objectAtIndex:2];
	settingsViewController = (SettingsViewController*)[self.viewControllers objectAtIndex:3];
	
	if (shouldGoToMap) {
		[self setSelectedViewController:mapViewController];
		shouldGoToMap = NO;
	}
	
    //I'm not sure why mornigingNudgeMessages function is calling here.
    //If it would be called in viewDidLoad, what happened?
    if (!isLogout) {
        [self monitoringNudgeMessages];
        
        if (!updateLocationTimer)
            [self startLocationTimer];
    } else
        isLogout = NO;
}

- (void)viewDidDisappear:(BOOL)animated {
	[updateLocationTimer invalidate];
	updateLocationTimer = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)checkDiscoverableMap {
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Want to show your location on the map?"
                                 message:@"If you choose to show that, other users can find you on the map and you will get much more focus!"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action) {
                                                   //Do Some action here
                                                   [self setDiscoverableMap:YES];
                                               }];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       [self setDiscoverableMap:NO];
                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                   }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)setDiscoverableMap:(BOOL)discoverableMap {
    NSDictionary* dicoveryRulesDic = @{ @"discoverableMap": [NSNumber numberWithBool:discoverableMap]
                                        };
    [[FirebaseManager sharedManager] updateDiscoveryRules:dicoveryRulesDic completion:^(NSError *error) {
        if (error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //Your main thread code goes in here
                [AlertViewManager showAlertView:@"Error!" msg:error.localizedDescription parent:self];
            });
        }
    }];
}

- (void)getUserInfo {
    [[FirebaseManager sharedManager] updateUserStatus];
	[[FirebaseManager sharedManager] getCurrentUser:^(User *user, NSError *error) {
		if (!error) {
			[conversationViewController setUserProfile];

            if (!user.userProfile.isValid) {
                [self runningExit:nil];
                return;
            }
            
            if (!isGotUserInfo) {
                //[[FirebaseManager sharedManager] updateUserStatus];
                isGotUserInfo = YES;
                
                [self startLocationUpdate];
                [self monitoringDiscoveryRules];
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            }            
			
			if (user.discoveryRules.notifications) {
				[self getNotifications];
			} else
				[self stopNotifications];
        } else {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [AlertViewManager showAlertView:@"Error!" msg:error.localizedDescription parent:self];
        }
	}];
}

/**
 * Monitoring Nudge Messages.
 */
- (void)monitoringNudgeMessages {
	[[FirebaseManager sharedManager] getNudgeMessage:^(NudgeMessage* message, NSError* error) {
		if (error) {
			[AlertViewManager showAlertView:@"Error" msg:error.localizedDescription parent:self];
			return;
		}
		
		if (message == nil)
			return;
		
		dispatch_async(dispatch_get_main_queue(), ^{
			//Your main thread code goes in here
			[self showNudgeToast:message];
		});
	
	}];
}

- (void)showNudgeToast:(NudgeMessage*)message {
	nudgeToastView = [[[NSBundle mainBundle] loadNibNamed:@"NudgeToastView" owner:self options:nil] objectAtIndex:0];
	
	CGRect bounds = [UIScreen mainScreen].bounds;
	nudgeToastView.frame = CGRectMake(0, -90, bounds.size.width, 90);
	[self.view addSubview:nudgeToastView];
	
	nudgeToastView.lbTitle.text = message.title;
	nudgeToastView.lbNewMessage.text = message.message;
    [ThemeManager setThemeWithChildViews:nudgeToastView];
    
    [FirebaseManager showImageWithUrl:nudgeToastView.ivProfileView imagePath:message.photoUrl];

	[UIView animateWithDuration:1.0f delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^(void) {
		nudgeToastView.frame = CGRectMake(0, 0, bounds.size.width, 90);
	}  completion:^(BOOL finished) {
        
	}];
    
    [[FirebaseManager sharedManager] removeNudgeMessage:message.key];
    [self dismissNudgeMessage];
}

- (void)dismissNudgeMessage {
	[UIView animateWithDuration:1.0f delay:3.0 options:UIViewAnimationOptionCurveEaseInOut animations:^(void) {
        nudgeToastView.frame = CGRectMake(0, -90, nudgeToastView.frame.size.width, 90);
	}  completion:^(BOOL finished) {
        [nudgeToastView removeFromSuperview];
	}];
}

- (void)getNotifications {
	[[FirebaseManager sharedManager] getNotifications:^(Notification *notification, BOOL isChanged, NSError *error) {
		if (!error) {
			if (!isChanged) {
				[notificationViewController addNotification:notification];
				
				if (!notification.isRead) {
					//Current is not Notifications page!
					if ([self selectedIndex] != 2) {
						[[BaseDataManager sharedManager] addNewNotification];
						//Set Badge!
						int newNotifications = [[BaseDataManager sharedManager] getNewNotifications];
						[[self.tabBar.items objectAtIndex:2] setBadgeValue:[NSString stringWithFormat:@"%d", newNotifications]];
					}					
				}
			}
		}
	}];
}

- (void)stopNotifications {
	[[FirebaseManager sharedManager] stopNotification];
}

//- (void)getAllDiscoveries {
//    [[FirebaseManager sharedManager] getAllDiscoveries:^(NSError *error) {
//        if (!error) {
//            [self startLocationUpdate];
//            [self monitoringDiscoveryRules];
//        }
//    }];
//}

- (void)monitoringDiscoveryRules {
	[[FirebaseManager sharedManager] getDiscoveryRules:^(DiscoveryRules* discoveryRules, NSError *error) {
		if (discoveryRules) {
			if (isChangedRules) {
                gotDiscoveriesTimes = 0;
                contryFilterTimes = 0;
                if (conversationViewController)
                    [conversationViewController refreshDiscoveries];
                
                if (mapViewController)
                    [mapViewController refreshMapLocation];
				
				[self restartLocationUpdate];
			}
			
			isChangedRules = YES;
		}
	}];
}

- (void)startLocationTimer {
	updateLocationTimer = [NSTimer scheduledTimerWithTimeInterval:UPDATE_INTERVAL_IN_SECONDS
									 target:self
								   selector:@selector(startLocationUpdate)
								   userInfo:nil
									repeats:YES];
}

- (void)startLocationUpdate {
	[locationManager startUpdatingLocation];
	isStoppedLocationManager = NO;
}

- (void)stopLocationUpdate {
	[locationManager stopUpdatingLocation];
	isStoppedLocationManager = YES;
}

- (void)restartLocationUpdate {
	if (!isStoppedLocationManager)	
		[self stopLocationUpdate];
	
	[self startLocationUpdate];
}

- (void)goToMap:(NSNotification*)data {
	shouldGoToMap = YES;
}

- (void)runningExit:(NSNotification*)data {
    isLogout = YES;
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"completedRegister"];
    
	[[BaseDataManager sharedManager] removeAllData];
	[[FirebaseManager sharedManager] removeAllObservers];
	
	[self stopLocationUpdate];
	[self stopNotifications];
    
    [updateLocationTimer invalidate];
    updateLocationTimer = nil;
    
    locationManager = nil;
	
	LogoViewController* svc = [self.storyboard instantiateViewControllerWithIdentifier:@"LogoViewController"];
	[[UIApplication sharedApplication].keyWindow setRootViewController:svc];
}

- (void)getNearUsers {
    NSLog(@"------------Get Near Users--------------");
    if (conversationViewController && gotDiscoveriesTimes == 0)
        [conversationViewController removeAllDiscoveries];
    
    if (mapViewController)
        [mapViewController refreshMapLocation];    
    
    User* me = [FirebaseManager sharedManager].loginedUser;
    
    if (gotDiscoveriesTimes > MAX_GET_DISCOVERIES_SKIP_TIME)
        gotDiscoveriesTimes = 0; //reset.
    
    gotDiscoveriesTimes++;
    
    double rangeRadius = NEAR_FINDING_RADIUS;
    //First time, get all discoveries according to discovery rules.
    //However, it wouldn't observe anymore because of performance.
    //After then, getting near users on map within 1km.
    if (gotDiscoveriesTimes == 1 && me.discoveryRules.discoveryMode == Range) {
        rangeRadius = [BaseDataManager getDiscoveryRangeValue];
    }
    
    [[FirebaseManager sharedManager] getNearUsers:currentLocation radius:rangeRadius
                                       completion:^(User* user, CLLocation* userLocation, NSError* error)
     {
         if (error) {
             [AlertViewManager showAlertView:@"Error!" msg:error.localizedDescription parent:self];
             return;
         }
         
         if (!user) return;
         
         if (![BaseDataManager checkValidUser:user])
             return;

         if (me.discoveryRules.discoveryMode == Range) {
             if (gotDiscoveriesTimes == 1 && conversationViewController)
                 [conversationViewController addDiscovery:user userLocation:userLocation currentLocation:currentLocation];
         }
         
         if (mapViewController)
             [mapViewController addNearUser:user userLocation:userLocation];
     }];
}

/**
 * Get discoveries with user that got by country filter. [26/03/2018]
 */
- (void)getDiscoveriesByCountryFilter {
    if (contryFilterTimes > 0)
        return;
    
    if (conversationViewController)
        [conversationViewController removeAllDiscoveries];
   
    contryFilterTimes++;
    if (contryFilterTimes > MAX_GET_DISCOVERIES_SKIP_TIME)
        contryFilterTimes = 0; //reset.
    
    [[FirebaseManager sharedManager] getUsersByCountryFilter:^(FIRDataSnapshot *snapshot, NSError *error) {
        if (error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //Your main thread code goes in here
                [AlertViewManager showAlertView:@"Error!" msg:error.localizedDescription parent:self];
            });
        }
        
        for (FIRDataSnapshot *userSnapshot in snapshot.children) {
            User* user = [[User alloc] initWithDictionary:userSnapshot.value];
            user.key = userSnapshot.key;
            if (!user)
                continue;

            if (![BaseDataManager checkValidUser:user])
                continue;
            
            User* me = [FirebaseManager sharedManager].loginedUser;
            if ([user.key isEqualToString:me.key])
                continue;
            
            if (conversationViewController)
                [conversationViewController addDiscovery:user currentLocation:currentLocation];
        }
    }];
}

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
	NSLog(@"didFailWithError: %@", error);
    
    [AlertViewManager showAlertView:@"Error" msg:@"Failed to Get Your Location" parent:self];
}

- (void)locationManager:(CLLocationManager *)manager
						didUpdateToLocation:(CLLocation *)newLocation
		   				fromLocation:(CLLocation *)oldLocation {
	
	if (!isStoppedLocationManager) {        
        currentLocation = newLocation;
        
        [FirebaseManager sharedManager].currentLocation = newLocation;
        
        User* me = [FirebaseManager sharedManager].loginedUser;
        [self getNearUsers];
        
        if (me.discoveryRules.discoveryMode == Country) {
            [self getDiscoveriesByCountryFilter];
        }
        
        if (mapViewController)
            [mapViewController updateUserLocation:newLocation];
        
		[self stopLocationUpdate];
	}
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
