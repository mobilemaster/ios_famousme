//
//  SettingsViewController.m
//  FamousMe
//
//  Created by Shine Man on 11/22/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "SettingsViewController.h"
#import "InterestTableViewCell.h"
#import "APContact.h"
#import "APAddressBook.h"
#import "User.h"
#import "MBProgressHUD.h"

@import MRCountryPicker;

#define DISCOVERY_GENDER_TAG                     1001
#define PICKER_GENDER_TAG                        2001

#define DISCOVERY_AGE_TAG                        1002
#define PICKER_AGE_TAG                           2002

#define DISCOVERY_NATIONALITY_TAG                1003
#define PICKER_NATIONALITY_TAG                   2003

#define DISCOVERY_METHOD_TAG                     1004
#define PICKER_METHOD_TAG                        2004

#define DISCOVERY_ONLINE_TAG                     1005
#define PICKER_ONLINE_TAG                        2005

#define DISCOVERY_SERVICE_TAG                    1006
#define PICKER_SERVICE_TAG                       2006

#define PICKER_COUNTRY_TAG                       2007
@import Toast;

@interface SettingsViewController ()<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, BEMCheckBoxDelegate, MRCountryPickerDelegate> {
	UIPickerView* agePicker;
	UIPickerView* genderPicker;
    UIPickerView* rangePicker;
    UIPickerView* onlinePicker;
    UIPickerView* servicePicker;
	UIPickerView* nationalityPicker;
    MRCountryPicker *countryPicker;
	
	NSArray* genderArray;
    NSArray* rangeArray;
	NSArray* ageArray;
	NSMutableArray* nationalityArray;
	NSArray* interestArray;
	NSMutableArray* selectedInterestArray;
    
    NSArray* onlineArray;
    NSArray* serviceArray;
    
    NSString* tempDialCode;
    
    ThemeType currentTheme;
}

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	
	self.tbInterestsList.delegate = self;
	self.tbInterestsList.dataSource = self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self setUI];
}

- (void)viewDidLayoutSubviews {
	[super viewDidLayoutSubviews];
	
	/*_btnDiscoveryGender.imageEdgeInsets = UIEdgeInsetsMake(0, _btnDiscoveryGender.frame.size.width - _btnDiscoveryGender.imageView.frame.origin.x, 0, 20);
	_btnAge.imageEdgeInsets = UIEdgeInsetsMake(0, _btnAge.frame.size.width - _btnAge.imageView.frame.origin.x, 0, 20);
	_btnNationality.imageEdgeInsets = UIEdgeInsetsMake(0, _btnNationality.frame.size.width - _btnNationality.imageView.frame.origin.x, 0, 20);*/
	/*_btnInterests.imageEdgeInsets = UIEdgeInsetsMake(0, _btnInterests.frame.size.width - _btnInterests.imageView.frame.origin.x, 0, 20);*/
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setUI {
	User* user = [FirebaseManager sharedManager].loginedUser;
    if (user == nil)
        return;
    
    currentTheme = [ThemeManager getThemeType];
    _currentThemeView.backgroundColor = [ThemeManager getCurrentThemeColor];
    
	genderPicker = [[UIPickerView alloc] init];
	genderPicker.delegate = self;
	genderPicker.dataSource = self;
	genderPicker.tag = PICKER_GENDER_TAG;
	self.tfDiscoveryGender.inputView = genderPicker;
	self.tfDiscoveryGender.delegate = self;
	self.tfDiscoveryGender.tag = DISCOVERY_GENDER_TAG;
	genderArray = @[@"All", @"Male", @"Female"];
	self.tfDiscoveryGender.text = [genderArray objectAtIndex:user.discoveryRules.discoverGender];
	
	agePicker = [[UIPickerView alloc] init];
	agePicker.delegate = self;
	agePicker.dataSource = self;
	agePicker.tag = PICKER_AGE_TAG;
	self.tfAge.inputView = agePicker;
	self.tfAge.delegate = self;
	self.tfAge.tag = DISCOVERY_AGE_TAG;
	
	ageArray = @[@"All", @"18 ~ 29", @"30 ~ 39", @"40 ~ 49", @"50 ~ 59", @"60 ~"];
	self.tfAge.text = [ageArray objectAtIndex:user.discoveryRules.ageRange];

    //Range
    rangePicker = [[UIPickerView alloc] init];
    rangePicker.delegate = self;
    rangePicker.dataSource = self;
    rangePicker.tag = PICKER_METHOD_TAG;
    self.tfDiscoveryMethod.inputView = rangePicker;
    self.tfDiscoveryMethod.delegate = self;
    self.tfDiscoveryMethod.tag = DISCOVERY_METHOD_TAG;
    rangeArray = @[@"No Limit", @"1km", @"5km", @"100km", @"1000km"];
    self.tfDiscoveryMethod.text = [rangeArray objectAtIndex:user.discoveryRules.discoveryRange];
    
    countryPicker = [[MRCountryPicker alloc] init];
    countryPicker.countryPickerDelegate = self;
    countryPicker.showPhoneNumbers = NO;
    countryPicker.tag = PICKER_COUNTRY_TAG;
    
    [self.scDiscoveryMethod setSelectedSegmentIndex:user.discoveryRules.discoveryMode];
    [self setDiscoveryMode];

    //Online
    onlinePicker = [[UIPickerView alloc] init];
    onlinePicker.delegate = self;
    onlinePicker.dataSource = self;
    onlinePicker.tag = PICKER_ONLINE_TAG;
    self.tfOnlineFilter.inputView = onlinePicker;
    self.tfOnlineFilter.delegate = self;
    self.tfOnlineFilter.tag = DISCOVERY_ONLINE_TAG;
    onlineArray = [BaseDataManager getOnlineFilterList];
    self.tfOnlineFilter.text = [onlineArray objectAtIndex:user.discoveryRules.onlineFilter];

    //Service
    servicePicker = [[UIPickerView alloc] init];
    servicePicker.delegate = self;
    servicePicker.dataSource = self;
    servicePicker.tag = PICKER_SERVICE_TAG;
    self.tfServiceFilter.inputView = servicePicker;
    self.tfServiceFilter.delegate = self;
    self.tfServiceFilter.tag = DISCOVERY_SERVICE_TAG;
    serviceArray = [BaseDataManager getServiceList];
    self.tfServiceFilter.text = [serviceArray objectAtIndex:user.discoveryRules.serviceFilter];
    
	nationalityPicker = [[UIPickerView alloc] init];
	nationalityPicker.delegate = self;
	nationalityPicker.dataSource = self;
	nationalityPicker.tag = PICKER_NATIONALITY_TAG;
	self.tfNationality.inputView = nationalityPicker;
	self.tfNationality.delegate = self;
	self.tfNationality.tag = DISCOVERY_NATIONALITY_TAG;
	nationalityArray = [NSMutableArray arrayWithArray:[BaseDataManager getNationalityList]];
	self.tfNationality.text = user.discoveryRules.discoverNationality;
	[nationalityArray insertObject:@"All" atIndex:0];
	
	interestArray = [BaseDataManager getInterestList];
	
	[self.swAnonymousMode setOn:user.discoveryRules.isAnonymousMode];
	//[self.swDiscoverableMap setOn:user.discoveryRules.discoverableMap];
    [self.swPushNotification setOn:user.discoveryRules.pushNotification];
	[self.swNotifications setOn:user.discoveryRules.notifications];
	[self.swShowPhoneNumber setOn:user.discoveryRules.showPhoneNumber];
	[self.swCanSeeAge setOn:user.discoveryRules.canSeeMyAge];
	
	NSArray* interestArray = [BaseDataManager getInterestList];
	if ([user.discoveryRules.discoveryInterest isEqualToString:@"All"])
		selectedInterestArray = [NSMutableArray arrayWithArray:interestArray];
	else
		selectedInterestArray = [NSMutableArray arrayWithArray:[user.discoveryRules.discoveryInterest componentsSeparatedByString:@"/"]];
	
	self.btnInterestsOk.layer.cornerRadius = 0.5f;
    
    if (user.discoveryRules.countryFilter > 0)
        tempDialCode = user.discoveryRules.countryFilter;
    else if (user.countryCode > 0)
        tempDialCode = user.countryCode;
    else
        tempDialCode = @"+1";
    
    [self applyTheme];
}

- (void)applyTheme {
    [ThemeManager setThemeWithChildViews:_headerView];
    [_scDiscoveryMethod setTintColor:[ThemeManager getCurrentThemeColor]];
    [ThemeManager setTheme:_btnInterestsOk];
    
    //Set theme of TabBar
    [self.tabBarController.tabBar setTintColor:[ThemeManager getCurrentThemeColor]];
}

- (void)saveDiscoveryRules {
	[MBProgressHUD showHUDAddedTo:self.view animated:YES];
	if (selectedInterestArray.count < 3) {
		[AlertViewManager showAlertView:nil msg:@"You should select the more interests than 3." parent:self];
		return;
	}
	
	NSString* interestData = nil;
	for (int i = 0; i < selectedInterestArray.count; i++) {
		NSString* interest = selectedInterestArray[i];
		if (!interestData)
			interestData = interest;
		else
			interestData = [NSString stringWithFormat:@"%@/%@", interestData, interest];
	}
	
	NSInteger genderIndex = [genderPicker selectedRowInComponent:0];
	NSInteger ageIndex = [agePicker selectedRowInComponent:0];
    NSInteger rangeIndex = [rangePicker selectedRowInComponent:0];
    NSInteger onlineIndex = [onlinePicker selectedRowInComponent:0];
    NSInteger serviceIndex = [servicePicker selectedRowInComponent:0];

	NSDictionary* dicoveryRulesDic = @{ @"isAnonymousMode" : [NSNumber numberWithBool:[self.swAnonymousMode isOn]] ,
                                        @"discoverGender"  : [NSNumber numberWithInteger:genderIndex],
                                        @"ageRange"     : [NSNumber numberWithInteger:ageIndex],
                                        @"discoveryMode"     : [NSNumber numberWithInteger:self.scDiscoveryMethod.selectedSegmentIndex],
                                        @"discoveryRange"     : [NSNumber numberWithInteger:rangeIndex],
                                        @"countryFilter"    : tempDialCode,
                                        @"onlineFilter"     : [NSNumber numberWithInteger:onlineIndex],
                                        @"serviceFilter"     : [NSNumber numberWithInteger:serviceIndex],
                                        @"discoverNationality": self.tfNationality.text,
                                        @"discoveryInterest": interestData,
                                        //@"discoverableMap" :  [NSNumber numberWithBool:[self.swDiscoverableMap isOn]],
                                        @"pushNotifications"  :  [NSNumber numberWithBool:[self.swPushNotification isOn]],
                                        @"notifications"	 :  [NSNumber numberWithBool:[self.swNotifications isOn]],
                                        @"showPhoneNumber" :  [NSNumber numberWithBool:[self.swShowPhoneNumber isOn]],
                                        @"canSeeMyAge"	 :  [NSNumber numberWithBool:[self.swCanSeeAge isOn]]
									 };
	
	[[FirebaseManager sharedManager] updateDiscoveryRules:dicoveryRulesDic completion:^(NSError *error) {
		if (error) {
			[AlertViewManager showAlertView:@"Error!" msg:error.localizedDescription parent:self];
		} else {
//			if (![self.swDiscoverableMap isOn])
//				[[FirebaseManager sharedManager] removeLocation:user.key];
			[MBProgressHUD hideHUDForView:self.view animated:YES];
			[AlertViewManager showAlertView:@"Success!" msg:@"Saved successfully!" parent:self];
		}
	}];
}

- (void)importContact {
	APAddressBook *addressBook = [[APAddressBook alloc] init];
	addressBook.fieldsMask = APContactFieldPhonesOnly;
	// don't forget to show some activity
	[addressBook loadContacts:^(NSArray <APContact *> *contacts, NSError *error) {
		if (!error) {
			// do something with contacts array
			for (APContact* contact in contacts) {
				for (APPhone *phone in contact.phones) {
					[self importContactsToServer:phone.number];
				}
			}
		}
		else {
			// show error
			[AlertViewManager showAlertView:@"Error" msg:error.localizedDescription parent:self];
		}
	}];
}

/**
 * Query with phoneNumber and add the user to contacts.
 * @param phoneNumber the phoneNumber whose have to be looked for...
 */
- (void)importContactsToServer:(NSString*)phoneNumber {
	if (phoneNumber.length == 0)
		return;
	
	phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
	phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
	phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
	phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@")" withString:@""];

	User* user = [FirebaseManager sharedManager].loginedUser;
	if (![phoneNumber containsString:@"+"]) {
		NSString *firstLetter = [phoneNumber substringToIndex:1];
		if ([firstLetter isEqualToString:@"0"])
			phoneNumber = [phoneNumber substringWithRange:NSMakeRange(1, [phoneNumber length] - 1)];
		
		phoneNumber = [NSString stringWithFormat:@"%@%@", user.countryCode,phoneNumber];
	}
	
	//Check own phone number
	if ([phoneNumber isEqualToString:user.phoneNumber])
		return;
	
	[[FirebaseManager sharedManager] importContact:phoneNumber completion:^(BOOL isImported, NSError* error) {
		if (error) {
			[AlertViewManager  showAlertView:@"Error" msg:error.localizedDescription parent:self];
			return;
		}
		
		if (isImported)
			[self.view makeToast:@"A user has been added to your contacts!"];
	}];
}

/**
 * Set discovery mode: Range or Country.
 */
- (void)setDiscoveryMode {
    NSInteger selectedSegment = self.scDiscoveryMethod.selectedSegmentIndex;
    User* user = [FirebaseManager sharedManager].loginedUser;
    if (selectedSegment == Range) {
        //toggle the correct view to be visible
        self.lbDiscoveryTitle.text = @"Range";
        self.tfDiscoveryMethod.inputView = rangePicker;
        self.tfDiscoveryMethod.text = [rangeArray objectAtIndex:user.discoveryRules.discoveryRange];
        
    } else {
        //toggle the correct view to be visible
        self.lbDiscoveryTitle.text = @"Country";
        self.tfDiscoveryMethod.inputView = countryPicker;
        if (user.discoveryRules.countryFilter.length > 0) {
            self.tfDiscoveryMethod.text = [BaseDataManager getCountryNameByCode:user.discoveryRules.countryFilter];
        } else {
            if (user.countryCode.length > 0)
                self.tfDiscoveryMethod.text = [BaseDataManager getCountryNameByCode:user.countryCode];
            else
                self.tfDiscoveryMethod.text = @"";
        }
    }
}

#pragma mark IBAction
- (IBAction)onSave:(id)sender {
	[self saveDiscoveryRules];
}

- (IBAction)onImport:(id)sender {
	[self importContact];
}

- (IBAction)onSelectedInterests:(id)sender {
	self.vgSelectInterests.hidden = YES;
}

- (IBAction)onSelectInterests:(id)sender {
	self.vgSelectInterests.hidden = NO;
}

- (IBAction)onChangedDiscoveryMode:(id)sender {
    [self setDiscoveryMode];
}

- (IBAction)onChangeTheme:(id)sender {
    UIButton* btnColor = (UIButton*)sender;

    currentTheme = btnColor.tag;
    _currentThemeView.backgroundColor = [ThemeManager getThemeColor:currentTheme];
}

- (IBAction)onApplyTheme:(id)sender {
    [ThemeManager setThemeType:currentTheme];
    [self applyTheme];
}

#pragma mark - MRCountryPickerDelegate
- (void)countryPhoneCodePicker:(MRCountryPicker *)picker didSelectCountryWithName:(NSString *)name
                   countryCode:(NSString *)countryCode
                     phoneCode:(NSString *)phoneCode flag:(UIImage *)flag {
    
    tempDialCode = phoneCode;
    self.tfDiscoveryMethod.text = [BaseDataManager getCountryNameByCode:phoneCode];
}


#pragma mark - UIPickerViewDelegate & UIPickerViewDataSource
// The number of columns of data
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
	return 1;
}

// The number of rows of data
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
	if (pickerView.tag == PICKER_GENDER_TAG)
		return genderArray.count;
	else if (pickerView.tag == PICKER_AGE_TAG)
		return ageArray.count;
	else if (pickerView.tag == PICKER_NATIONALITY_TAG)
		return nationalityArray.count;
    else if (pickerView.tag == PICKER_METHOD_TAG)
        return rangeArray.count;
    else if (pickerView.tag == PICKER_ONLINE_TAG)
        return onlineArray.count;
    else if (pickerView.tag == PICKER_SERVICE_TAG)
        return serviceArray.count;
    
	return 0;
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
	if (pickerView.tag == PICKER_GENDER_TAG)
		return genderArray[row];
	else if (pickerView.tag == PICKER_AGE_TAG)
		return ageArray[row];
	else if (pickerView.tag == PICKER_NATIONALITY_TAG)
		return nationalityArray[row];
    else if (pickerView.tag == PICKER_METHOD_TAG)
        return rangeArray[row];
    else if (pickerView.tag == PICKER_ONLINE_TAG)
        return onlineArray[row];
    else if (pickerView.tag == PICKER_SERVICE_TAG)
        return serviceArray[row];
    else if (pickerView.tag == PICKER_COUNTRY_TAG)
        return [BaseDataManager getCountryNameByCode:tempDialCode];
    
	return nil;
}


#pragma mark - UITableViewDelegate & DataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 50;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex {
	return interestArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *cellIdentifier = @"InterestTableViewCell";
	InterestTableViewCell *cell = (InterestTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	NSString* interest = [interestArray objectAtIndex:indexPath.row];
	cell.lbName.text = interest;
	
	BOOL isSelected = NO;
	for (NSString* selectedInterest in selectedInterestArray) {
		if ([interest isEqualToString:selectedInterest]) {
			isSelected = YES;
			break;
		}
	}
    
    cell.chCheckBox.onFillColor = [ThemeManager getCurrentThemeColor];
    cell.chCheckBox.onTintColor = [ThemeManager getCurrentThemeColor];

	[cell.chCheckBox setTag:indexPath.row];
	[cell.chCheckBox setOn:isSelected];
	cell.chCheckBox.delegate = self;
	
	return cell;
}

#pragma mark - BEMCheckBoxDelegate
- (void)didTapCheckBox:(BEMCheckBox*)checkBox {
	NSInteger index = checkBox.tag;
	NSString* interestName = [interestArray objectAtIndex:index];
	if (checkBox.on)
		[selectedInterestArray addObject:interestName];
	else
		[selectedInterestArray removeObject:interestName];
}

#pragma mark - UITextFieldDelegate
- (void)textFieldDidEndEditing:(UITextField *)textField {
	switch (textField.tag) {
		case DISCOVERY_GENDER_TAG: {
			NSInteger row = [genderPicker selectedRowInComponent:0];
			self.tfDiscoveryGender.text = [genderArray objectAtIndex:row];
			[self.tfDiscoveryGender resignFirstResponder];
		}
			break;
		case DISCOVERY_AGE_TAG: {
			NSInteger row = [agePicker selectedRowInComponent:0];
			self.tfAge.text = [ageArray objectAtIndex:row];
			[self.tfAge resignFirstResponder];
		}
			break;
		case DISCOVERY_NATIONALITY_TAG:{
			NSInteger row = [nationalityPicker selectedRowInComponent:0];
			self.tfNationality.text = [nationalityArray objectAtIndex:row];
			[self.tfNationality resignFirstResponder];
		}
			break;
        case DISCOVERY_METHOD_TAG:{
            if (self.scDiscoveryMethod.selectedSegmentIndex == Range) {
                NSInteger row = [rangePicker selectedRowInComponent:0];
                self.tfDiscoveryMethod.text = [rangeArray objectAtIndex:row];
                [self.tfDiscoveryMethod resignFirstResponder];
            }
        }
            break;
        case DISCOVERY_ONLINE_TAG:{
            NSInteger row = [onlinePicker selectedRowInComponent:0];
            self.tfOnlineFilter.text = [onlineArray objectAtIndex:row];
            [self.tfOnlineFilter resignFirstResponder];
        }
            break;
        case DISCOVERY_SERVICE_TAG:{
            NSInteger row = [servicePicker selectedRowInComponent:0];
            self.tfServiceFilter.text = [serviceArray objectAtIndex:row];
            [self.tfServiceFilter resignFirstResponder];
        }
            break;
		default:
			break;
	}
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
