//
//  MapViewController.m
//  FamousMe
//
//  Created by Shine Man on 11/22/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "MapViewController.h"
#import "MapTableViewCell.h"
#import "ChatViewController.h"
#import "GeoFire.h"
#import "MapLocationItem.h"
#import "DateUtils.h"
#import "UserProfileViewController.h"
#import "InviteViewController.h"

@import Pulsator;

@interface MapViewController ()<UITableViewDelegate, UITableViewDataSource, GMSMapViewDelegate> {
	CLLocation *currentLocation;
	
	NSInteger currentUserIndex;
}

@property (strong, nonatomic) NSMutableArray* nearValidUserArray;
@property (assign, nonatomic) BOOL isMapView;
@end

@implementation MapViewController
@synthesize nearValidUserArray;
@synthesize isMapView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	_tbLocations.delegate = self;
	_tbLocations.dataSource = self;
	
	self.ivUserPhoto.layer.cornerRadius = self.ivUserPhoto.frame.size.height / 2;
	self.ivUserPhoto.clipsToBounds = YES;
	
	currentUserIndex = 0;
    if (nearValidUserArray == nil)
        nearValidUserArray = [NSMutableArray array];
    
	isMapView = NO;
	self.mapView.delegate = self;
	[self onChangeAction:nil];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
    
	[self applyTheme];
    
	_vgMapLoadingView.hidden = NO;
	[self performSelector:@selector(hideMapLoadingView)
				withObject:self
				afterDelay:3.0f];
}

- (void)loadView {
	[super loadView];
}

- (void)applyTheme {
    [ThemeManager setThemeWithChildViews:_headerView];
    [ThemeManager setTheme:_btnInviteFriends];
    [ThemeManager setTheme:_btnViewProfile];
    
    _ivLoading.image = [ThemeManager getMapLoadingIcon];
    _ivNoLoading.image = [ThemeManager getMapLoadingIcon];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)hideMapLoadingView {
	_vgMapLoadingView.hidden = YES;
	
	[self updateUserLocation:currentLocation];
}

- (void)updateUserLocation:(CLLocation*)location {
	currentLocation = location;
	
	User* user = [FirebaseManager sharedManager].loginedUser;
	if (user.discoveryRules.discoverableMap) {
		[[FirebaseManager sharedManager] setUserLocation:user.key location:location];
	}
	
	[self.mapView clear];
	// Create a GMSCameraPosition that tells the map to display the
	GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:currentLocation.coordinate.latitude
															longitude:currentLocation.coordinate.longitude
																 zoom:15];
	self.mapView.myLocationEnabled = NO;
	[self.mapView animateToCameraPosition:camera];
	
	// Creates a marker in the center of the map.
	GMSMarker *marker = [[GMSMarker alloc] init];
	marker.position = CLLocationCoordinate2DMake(currentLocation.coordinate.latitude, currentLocation.coordinate.longitude);
	//marker.title = @"My location";
    marker.groundAnchor = CGPointMake(0.5, 0.5);
	marker.map = self.mapView;    
	
	//Draw Circle
//    CLLocationCoordinate2D circleCenter = CLLocationCoordinate2DMake(currentLocation.coordinate.latitude, currentLocation.coordinate.longitude);
//    GMSCircle *circle = [GMSCircle circleWithPosition:circleCenter radius:1000];
//    circle.strokeColor = [UIColor whiteColor];
//    circle.strokeWidth = 2;
//    circle.map = self.mapView;
    
	[self.mapView setMinZoom:15 maxZoom:30];
    
    //Pulse Ring
    CGFloat pulseRadius = [UIScreen mainScreen].bounds.size.width - 50;
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, pulseRadius, pulseRadius)];
    view.layer.cornerRadius = pulseRadius/2;
    view.layer.borderWidth = 2.0f;
    view.layer.borderColor = MAIN_COLOR.CGColor;
    view.backgroundColor = PULSE_BACKGROUND_COLOR;
    UIImageView* markerView = [[UIImageView alloc] init];
    markerView.frame = CGRectMake(0, 0, 50, 50);
    markerView.center = view.center;
    markerView.layer.cornerRadius = 25;
    [markerView setClipsToBounds:YES];
    markerView.layer.borderWidth = 2.0f;
    markerView.layer.borderColor = MAIN_COLOR.CGColor;
    
    User* me = [FirebaseManager sharedManager].loginedUser;
    [FirebaseManager showImageWithUrl:markerView imagePath:me.userProfile.avatarUrl];
    [view addSubview:markerView];
    
    Pulsator* pulsator = [[Pulsator alloc] init];
    pulsator.position = view.center;
    pulsator.backgroundColor = PULSE_COLOR.CGColor;
    pulsator.radius = pulseRadius/2;
    
    [pulsator start];

    marker.iconView = view;
    [view.layer.superlayer insertSublayer:pulsator below:view.layer];
    
	//[self getNearUsers];
    //Add users
    for (int i = 0; i < nearValidUserArray.count; i++) {
        MapLocationItem* item = [nearValidUserArray objectAtIndex:i];
        if (!item.user.discoveryRules.isAnonymousMode)
            [self addMarkers:item.location index:i userName:item.user.userProfile.username];
    }
}

- (void)refreshMapLocation {
    [nearValidUserArray removeAllObjects];
    [self.tbLocations reloadData];
    
    [self.mapView clear];
}

- (void)addNearUser:(User*)user userLocation:(CLLocation*)userLocation {
    if (nearValidUserArray == nil)
        nearValidUserArray = [NSMutableArray array];
    
    MapLocationItem* item = [[MapLocationItem alloc] init];    
    [item setData:user userLocation:userLocation currentLocation:currentLocation];
    
    //Only the users who located in 1km radius should be shown.
    if (item.distance > NEAR_FINDING_RADIUS * 1000)
        return;
    
    [nearValidUserArray addObject:item];
    [self.tbLocations reloadData];
    
    if (!item.user.discoveryRules.isAnonymousMode)
        [self addMarkers:userLocation index:nearValidUserArray.count - 1 userName:user.userProfile.username];
}

#if 0
- (void)getNearUsers {
	[[FirebaseManager sharedManager] getNearUsers:currentLocation
									   completion:^(User* user, CLLocation* userLocation, NSError* error)
    {
		if (error) {
			[AlertViewManager showAlertView:@"Error!" msg:error.localizedDescription parent:self];
			return;
		}
		
		if (!user) return;

        if (!user.userProfile.isValid) return	;
		
		if (![BaseDataManager checkValidUser:user])
			return;

		NSInteger index = [self getIndexOfNearUser:user.key];
		if (index >= 0) {
			MapLocationItem *item = [nearValidUserArray objectAtIndex:index];
			[item setData:user userLocation:userLocation currentLocation:currentLocation];
			
			[self.tbLocations reloadData];
			[self updateDiscoveriesData:item];
			
			if (!item.user.discoveryRules.isAnonymousMode)
				[self addMarkers:userLocation index:index userName:user.userProfile.username];
		} else {
			MapLocationItem* item = [[MapLocationItem alloc] init];
			[item setData:user userLocation:userLocation currentLocation:currentLocation];
			[nearValidUserArray insertObject:item atIndex:0];
			
			[self.tbLocations reloadData];
			[self updateDiscoveriesData:item];
			
			if (!item.user.discoveryRules.isAnonymousMode)
				[self addMarkers:userLocation index:nearValidUserArray.count-1 userName:user.userProfile.username];
		}
	}];
}

- (NSInteger)getIndexOfNearUser:(NSString*)userKey {
	for (NSInteger i = 0; i < nearValidUserArray.count; i++) {
		MapLocationItem* item = [nearValidUserArray objectAtIndex:i];
		if ([userKey isEqualToString:item.user.key])
			return i;
	}
	
	return -1;
}

- (void)updateDiscoveriesData:(MapLocationItem*)mapLocationItem {
	NSMutableDictionary *discoveryDic = [[NSMutableDictionary alloc] init];
	[discoveryDic setValue:[NSNumber numberWithDouble:mapLocationItem.distance] forKey:@"distance"];
	
	if (![BaseDataManager isDuplicatedDiscovery:mapLocationItem.user.key])
		[discoveryDic setValue:[FIRServerValue timestamp] forKey:@"timestamp"];
	
	[[FirebaseManager sharedManager] updateDiscovery:discoveryDic userId:mapLocationItem.user.key completion:nil];
}
#endif

- (void)addMarkers:(CLLocation*)userLocation index:(NSInteger)index userName:(NSString*)userName {
	GMSMarker *marker = [[GMSMarker alloc] init];
	marker.position = CLLocationCoordinate2DMake(userLocation.coordinate.latitude, userLocation.coordinate.longitude);
	marker.title = userName;
	UIImage* image = [UIImage imageNamed:@"famousme"];
	
	UIImageView* markerView = [[UIImageView alloc] initWithImage:image];
	markerView.frame = CGRectMake(0, 0, 50, 50);
	
	marker.iconView = markerView;
	[marker setUserData:[NSNumber numberWithInteger:index]];
	marker.map = self.mapView;
}

#pragma mark - IBAction
- (IBAction)onViewProfile:(id)sender {
	MapLocationItem* item = [nearValidUserArray objectAtIndex:currentUserIndex];
	NSString* timeGap = [DateUtils getGapOfTimesWithString:item.user.status.timestamp];
	NSString* distance = [BaseDataManager getDistanceString:item.distance];
	NSString* status = [NSString stringWithFormat:@"%@ - %@", timeGap, distance];
	
	//Go to user profile page
	self.vgViewProfile.hidden = YES;
	
	[[FirebaseManager sharedManager] sendNotification:item.user.key notificationType:NotificationViewedProfile];
	
	NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
	[dict setValue:@NO forKey:@"isFromChat"];
	[dict setValue:@NO forKey:@"shouldCheckUpdate"];
	[dict setValue:status forKey:@"status"];
	[dict setValue:item.user.key forKey:@"userId"];
	
	UserProfileViewController* svc = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileViewController"];
	[svc setBaseData:dict];
	
	[self presentViewController:svc animated:YES completion:nil];
}

- (IBAction)onInviteFriends:(id)sender {
	InviteViewController* svc = [self.storyboard instantiateViewControllerWithIdentifier:@"InviteViewController"];
	
	[self presentViewController:svc animated:YES completion:nil];
}

- (IBAction)onChangeAction:(id)sender {
	isMapView = !isMapView;
	if (isMapView) {
		_vgLocationList.hidden = YES;
		[_btnAction setImage:[UIImage imageNamed:@"listSimpleLineIcons"] forState:UIControlStateNormal];
	} else {
		_vgLocationList.hidden = NO;
		[_btnAction setImage:[UIImage imageNamed:@"iconMap"] forState:UIControlStateNormal];
	}
}

#pragma mark Marker Click
- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker {
	NSInteger indexOfUser = [marker.userData integerValue];
	MapLocationItem* mapItem = [nearValidUserArray objectAtIndex:indexOfUser];
	
	currentUserIndex = indexOfUser;
	
	self.vgViewProfile.hidden = NO;
	
	[FirebaseManager showImageWithUrl:self.ivUserPhoto imagePath:mapItem.user.userProfile.photoUrl];
	self.lbUserName.text = mapItem.user.userProfile.username;
	self.lbProfession.text = mapItem.user.userProfile.profession;
	
	if (mapItem.user.status.isOnline) {
		self.lbOnlineStatus.text = @"online now";
		self.lbOnlineStatus.textColor = MAIN_COLOR;
	} else {
		self.lbOnlineStatus.text = [DateUtils getGapOfTimesWithString:mapItem.user.status.timestamp];
		self.lbOnlineStatus.textColor = CONTENT_TEXT_COLOR;
	}

	self.lbDistanceStatus.text = [BaseDataManager getDistanceString:mapItem.distance];
}

#pragma mark - UITableViewDelegate & DataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 80;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex {
	return nearValidUserArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *cellIdentifier = @"MapTableViewCell";
	MapTableViewCell *cell = (MapTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];	
	cell.ivProfilePhoto.layer.cornerRadius = cell.ivProfilePhoto.frame.size.height/2;
	[cell.ivProfilePhoto setClipsToBounds:YES];
	
	MapLocationItem* item = [nearValidUserArray objectAtIndex:indexPath.row];
	//[FirebaseManager showProfilePhoto:cell.ivProfilePhoto imagePath:item.user.userProfile.photoUrl];
    [FirebaseManager showProfilePhoto:cell.ivProfilePhoto userProfile:item.user.userProfile];
	
	cell.lbUserName.text = item.user.userProfile.username;
	cell.lbProfession.text = item.user.userProfile.profession;
	cell.lbTime.text = [DateUtils getGapOfTimesWithString:item.user.status.timestamp];
	cell.lbDistance.text = [BaseDataManager getDistanceString:(int)item.distance];
	
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	ChatViewController* svc = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatViewController"];

    MapLocationItem* item = [nearValidUserArray objectAtIndex:indexPath.row];
	NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
	
	[dict setValue:item.user.key forKey:@"userId"];
	[dict setValue:[NSNumber numberWithBool:item.user.userProfile.isValid] forKey:@"isValid"];
	[dict setValue:item.user.userProfile.username forKey:@"username"];
	
	[svc setBaseData:dict];
	[self presentViewController:svc animated:YES completion:nil];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
