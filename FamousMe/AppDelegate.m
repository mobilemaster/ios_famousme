//
//  AppDelegate.m
//  FamousMe
//
//  Created by Shine Man on 11/14/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "AppDelegate.h"
@import GoogleMaps;
@import GooglePlaces;
@import IQKeyboardManager;

#import "LogoViewController.h"
#import "MainViewController.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
@import TwitterKit;
@import GoogleSignIn;

#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
@import UserNotifications;
#endif

// Implement UNUserNotificationCenterDelegate to receive display notification via APNS for devices
// running iOS 10 and above.
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
@interface AppDelegate () <UNUserNotificationCenterDelegate>
@end
#endif

// Copied from Apple's header in case it is missing in some cases (e.g. pre-Xcode 8 builds).
#ifndef NSFoundationVersionNumber_iOS_9_x_Max
#define NSFoundationVersionNumber_iOS_9_x_Max 1299
#endif

@interface AppDelegate () {
	NSString *InstanceID;
}

@end

@implementation AppDelegate

NSString *const kGCMMessageIDKey = @"gcm.message_id";

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
	// Override point for customization after application launch.
	[GMSServices provideAPIKey:@"AIzaSyDGuDZXBZrmqt_7H7WYCkfHfpgv15yNcd4"];
	[GMSPlacesClient provideAPIKey:@"AIzaSyDGuDZXBZrmqt_7H7WYCkfHfpgv15yNcd4"];
	
	[IQKeyboardManager sharedManager].enable = YES;
	
	[FIRApp configure];
    //Google SignIn
    [GIDSignIn sharedInstance].clientID = [FIRApp defaultApp].options.clientID;    
    
	//[FIRDatabase database].persistenceEnabled = YES;
    //[FIRDatabase setLoggingEnabled:YES];
	//[[[FIRDatabase database] reference] keepSynced:YES];
	
    //Facebook sign in
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    //Twitter sign in
	[[Twitter sharedInstance] startWithConsumerKey:@"9JR71cAdSb8g18AEfEjCeyLcx" consumerSecret:@"yXBJCRancl0gmK1MBgghcH9lhUfDu7YZ3a9VUin42ZMb466W3r"];
	
	self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
	UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
	
	if([[NSUserDefaults standardUserDefaults] boolForKey:@"completedRegister"]) {
		// Show the dashboard
		self.window.rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"MainViewController"];
	} else {
		// Login
		self.window.rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"LogoViewController"];
	}
	
	[self.window makeKeyAndVisible];
	
	//ToolTip Page Control
	UIPageControl *pageControl = [UIPageControl appearance];
	//pageControl.transform = CGAffineTransformMakeScale(2.0f, 2.0f);	
	pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
	pageControl.currentPageIndicatorTintColor = MAIN_COLOR;
	pageControl.backgroundColor = [UIColor colorWithRed:250.0f/255.0f green:250.0f/255.0f blue:250.0f/255.0f alpha:1.0];
	

	// Register for remote notifications. This shows a permission dialog on first run, to
	// show the dialog at a more appropriate time move this registration accordingly.
	if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_7_1) {
		// iOS 7.1 or earlier. Disable the deprecation warnings.
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
		UIRemoteNotificationType allNotificationTypes =
		(UIRemoteNotificationTypeSound |
		 UIRemoteNotificationTypeAlert |
		 UIRemoteNotificationTypeBadge);
		[application registerForRemoteNotificationTypes:allNotificationTypes];
#pragma clang diagnostic pop
	} else {
		// iOS 8 or later
		// [START register_for_notifications]
		if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
			UIUserNotificationType allNotificationTypes =
			(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
			UIUserNotificationSettings *settings =
			[UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
			[application registerUserNotificationSettings:settings];
		} else {
			// iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
			// For iOS 10 display notification (sent via APNS)
			[UNUserNotificationCenter currentNotificationCenter].delegate = self;
			UNAuthorizationOptions authOptions =
			UNAuthorizationOptionAlert
			| UNAuthorizationOptionSound
			| UNAuthorizationOptionBadge;
			[[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
			}];
#endif
		}
		
		[application registerForRemoteNotifications];
		// [END register_for_notifications]
	}
	
	// [START set_messaging_delegate]
	[FIRMessaging messaging].delegate = self;
	[[FIRMessaging messaging] setShouldEstablishDirectChannel:YES];
	// [END set_messaging_delegate]
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:) name:kFIRInstanceIDTokenRefreshNotification object:nil];
		
	return YES;
}

#define TWITTER_SCHEME @"twitterkit-9JR71cAdSb8g18AEfEjCeyLcx"
#define FACEBOOK_SCHEME  @"fb347912062360535"
#define GOOGLE_SCHEME    @"com.googleusercontent.apps.207560281480-fje8dcadhqe7vvpplm07sl5r7g5o0fgi"
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options {

    if ([[url scheme] isEqualToString:FACEBOOK_SCHEME])
        return [[FBSDKApplicationDelegate sharedInstance] application:app
                                                              openURL:url                                                    sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                           annotation:nil];
    if ([[url scheme] isEqualToString:TWITTER_SCHEME])
        return [[Twitter sharedInstance] application:app openURL:url options:options];
    
    if ([[url scheme] isEqualToString:GOOGLE_SCHEME])
        return [[GIDSignIn sharedInstance] handleURL:url
                                   sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                          annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
    
    return NO;
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}

- (void)tokenRefreshNotification:(NSNotification *)notification
{
	InstanceID = [NSString stringWithFormat:@"%@", [notification object]];
	
	[self connectToFcm];
}

- (void)connectToFcm
{
	[[FIRMessaging messaging] setShouldEstablishDirectChannel:YES];
}


- (void)applicationWillResignActive:(UIApplication *)application {
	// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
	// Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
	// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
	// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
	// Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}

- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
	// Pass device token to auth.
	//[[FIRAuth auth] setAPNSToken:deviceToken type:FIRAuthAPNSTokenTypeProd];
	
	// Further handling of the device token if needed by the app.
	[[FIRAuth auth] setAPNSToken:deviceToken type:FIRAuthAPNSTokenTypeUnknown];
    [FIRMessaging messaging].APNSToken = deviceToken;
	
}

// [START receive_message]
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
	// If you are receiving a notification message while your app is in the background,
	// this callback will not be fired till the user taps on the notification launching the application.
	// TODO: Handle data of notification
	
	// With swizzling disabled you must let Messaging know about the message, for Analytics
	// [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
	
	// Print message ID.
	if (userInfo[kGCMMessageIDKey]) {
		NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
	}
	
	// Print full message.
	NSLog(@"%@", userInfo);
    NSLog(@"%@", userInfo[@"notification"]);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
	// If you are receiving a notification message while your app is in the background,
	// this callback will not be fired till the user taps on the notification launching the application.
	// TODO: Handle data of notification
	
	// Pass notification to auth and check if they can handle it.
	if ([[FIRAuth auth] canHandleNotification:userInfo]) {
		completionHandler(UIBackgroundFetchResultNoData);
		return;
	}
	// Print message ID.
	if (userInfo[kGCMMessageIDKey]) {
		NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
	}
	
	// Print full message.
	NSLog(@"%@", userInfo);
    NSLog(@"%@", userInfo[@"alert"]);
	
	completionHandler(UIBackgroundFetchResultNewData);
}

- (void)applicationReceivedRemoteMessage:(FIRMessagingRemoteMessage *)remoteMessage {
	NSLog(@"FCM registration token: %@", @"-----------");
}

// [END receive_message]

// [START refresh_token]
- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken {
	NSLog(@"FCM registration token: %@", fcmToken);
	
	// TODO: If necessary send token to application server.
	// Note: This callback is fired at each app startup and whenever a new token is generated.
}
// [END refresh_token]

// [START ios_10_data_message]
// Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
// To enable direct data messages, you can set [Messaging messaging].shouldEstablishDirectChannel to YES.
- (void)messaging:(FIRMessaging *)messaging didReceiveMessage:(FIRMessagingRemoteMessage *)remoteMessage {
	NSLog(@"Received data message: %@", remoteMessage.appData);
}
// [END ios_10_data_message]

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
	NSLog(@"Unable to register for remote notifications: %@", error);
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
	// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBSDKAppEvents activateApp];
}


- (void)applicationWillTerminate:(UIApplication *)application {
	// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
