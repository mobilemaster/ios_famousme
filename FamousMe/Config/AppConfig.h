//
//  AppConfig.h
//  FamousMe
//
//  Created by Shine Man on 11/23/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#ifndef AppConfig_h
#define AppConfig_h

#define PROFILE_AVATAR_SIZE                 120.0f

#define MAIN_COLOR							[UIColor colorWithRed:18/225.0f green:177/255.0f blue:171/255.0f alpha:1.0f]
#define SALMON_COLOR                        [UIColor colorWithRed:249/225.0f green:104/255.0f blue:85/255.0f alpha:1.0f]
#define MEDIUM_GREEN_COLOR                  [UIColor colorWithRed:18/225.0f green:177/255.0f blue:171/255.0f alpha:1.0f]
#define HOT_PINK_COLOR                      [UIColor colorWithRed:236/225.0f green:121/255.0f blue:164/255.0f alpha:1.0f]
#define BLUE_COLOR                          [UIColor colorWithRed:53/225.0f green:118/255.0f blue:212/255.0f alpha:1.0f]

#define DARK_INDIGO_COLOR                   [UIColor colorWithRed:20/225.0f green:7/255.0f blue:16/255.0f alpha:1.0f]
#define DARK_SALMON_COLOR                   [UIColor colorWithRed:236/225.0f green:82/255.0f blue:82/255.0f alpha:1.0f]
#define BLUE_VIOLET_COLOR                   [UIColor colorWithRed:97/225.0f green:10/255.0f blue:151/255.0f alpha:1.0f]
#define SLATE_GRAY_COLOR                    [UIColor colorWithRed:41/225.0f green:47/255.0f blue:59/255.0f alpha:1.0f]
#define ROYAL_BLUE_COLOR                    [UIColor colorWithRed:26/225.0f green:40/255.0f blue:101/255.0f alpha:1.0f]

#define NORMAL_TEXT_GREY_COLOR				[UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.5f]
#define CONTENT_TEXT_COLOR					[UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.8f]
#define LIGHT_DARK_BACKGROUND_COLOR			[UIColor colorWithRed:230/255.0f green:237/255.0f blue:237/255.0f alpha:1.0f]
#define GENERAL_LIGHT_GREY_COLOR			[UIColor colorWithRed:211/255.0f green:219/255.0f blue:219/255.0f alpha:1.0f]
#define MESSAGE_NOTICE_COLOR				[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:0.5f]
#define GENERAL_YELLOW_COLOR				[UIColor colorWithRed:255/255.0f green:189/255.0f blue:18/255.0f alpha:1.0f]
#define PROGRESS_BACKGROUND_COLOR			[UIColor colorWithRed:127/255.0f green:127/255.0f blue:127/255.0f alpha:1.0f]
#define PULSE_BACKGROUND_COLOR              [UIColor colorWithRed:18/225.0f green:177/255.0f blue:171/255.0f alpha:0.15f]
#define PULSE_COLOR                         [UIColor colorWithRed:18/225.0f green:220/255.0f blue:190/255.0f alpha:1.0f]

#define AGE_RANGE_ARRAY					 	@[@"All", @"18 ~ 29", @"30 ~ 39", @"40 ~ 49", @"50 ~ 59", @"60 ~"]

#define NOTIFICATION_USERINFO_CHANGED 		        @"notification_userinfo_changed"
#define NOTIFICATION_GOTO_MAP 				        @"notification_goto_map"
#define NOTIFICATION_LOG_OUT 				        @"notification_log_out"


#define ACHIEVEMENT_START_CONVERSATION				@"start_conversation"
#define ACHIEVEMENT_USE_APP_TWO_WEEKS	  			@"use_app_2_weeks"
#define ACHIEVEMENT_IMPORT_CONTACTS 				@"import_contacts"

#define READ_MESSAGES_COUNT							25

#define FCM_AUTH_KEY								@"key=AIzaSyBdcYThNB7WCqqbkIxzHsZ9imBJ-aP9xd0"
#define FCM_SERVER_URL								@"https://fcm.googleapis.com/fcm/send"

#define NEAR_FINDING_RADIUS                         1
#define DISCOVERY_FINDING_RADIUS                    20000
#define UPDATE_INTERVAL_IN_SECONDS                  600
//Enum
typedef NS_ENUM(NSUInteger, DiscoveryMode) {
    Range,
    Country
};

typedef NS_ENUM(NSUInteger, AchievementType) {
	AchievementStartConverstation,
	AchievementUseAppFor2Weeks,
	AchievementImportContact
};

typedef NS_ENUM(NSUInteger, NotificationType) {
	NotificationViewedProfile = 1,
	NotificationImportedContact = 2
};

typedef NS_ENUM(NSUInteger, OnlineFilter) {
    NO_TIME_LIMIT,
    A_DAY,
    A_WEEK,
    A_MONTH
};

typedef NS_ENUM(NSUInteger, ThemeType) {
    SalmonColor,
    MediumGreenColor,
    BlueColor,
    HotPinkColor,
    DarkIndigoColor,
    DarkSalmonColor,
    BlueVioletColor,
    SlateGrayColor,
    RoyalBlueColor    
};

typedef NS_ENUM(NSUInteger, MessageType) {
    Text,
    Image
};

#endif /* AppConfig_h */
