//
//  Discovery.m
//  FamousMe
//
//  Created by Shine Man on 12/13/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "Discovery.h"

@implementation Discovery

- (id)initWithDictionary:(NSDictionary *)dictionary {
	self = [super init];
	if (self) {
		//self.userKey  = [dictionary objectForKey:@"userKey"];
		self.timestamp  = [[dictionary objectForKey:@"timestamp"] doubleValue];
		self.distance = [[dictionary objectForKey:@"distance"] doubleValue];
		self.lastMessage   = [dictionary objectForKey:@"lastMessage"];
	}
	
	return self;
}

- (NSDictionary *)dictionaryValue {
	return @{ @"timestamp"     : [NSNumber numberWithDouble:self.timestamp],
			  @"distance"      : [NSNumber numberWithDouble:self.distance],
			  @"lastMessage"     : self.lastMessage,
			  };
}

@end
