//
//  Social.h
//  FamousMe
//
//  Created by Shine Man on 12/6/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Social : NSObject

@property (nonatomic) NSString *website;
@property (nonatomic) NSString *facebook;
@property (nonatomic) NSString *snapchat;
@property (nonatomic) NSString *linkedin;
@property (nonatomic) NSString *twitter;
@property (nonatomic) NSString *instagram;
@property (nonatomic) NSString *youtube;

- (id)initWithDictionary:(NSDictionary *)dictionary;
- (NSDictionary *)dictionaryValue;
@end
