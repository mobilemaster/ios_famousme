//
//  BlockUser.m
//  FamousMe
//
//  Created by Shine Man on 12/13/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "BlockUser.h"

@implementation BlockUser

- (id)init {
	self = [super init];
	if (self) {
		self.Ids = @"";
	}
	
	return self;
}

- (id)initWithDictionary:(NSDictionary *)dictionary {
	self = [super init];
	if (self) {
		self.Ids = [dictionary objectForKey:@"Ids"];
	}
	
	return self;
}

- (NSDictionary *)dictionaryValue {
	return @{ @"Ids"     : self.Ids,
			  };
}

@end
