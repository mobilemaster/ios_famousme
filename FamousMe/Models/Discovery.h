//
//  Discovery.h
//  FamousMe
//
//  Created by Shine Man on 12/13/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserProfile.h"
#import "Status.h"

@interface Discovery : NSObject

@property (nonatomic) NSString *userKey;

@property (nonatomic) double timestamp;
@property (nonatomic) double distance;
@property (nonatomic) NSString *lastMessage;

@property (nonatomic) UserProfile *userProfile;

//Exclude firebase data - [2018.03.12]
@property (nonatomic) Status *status;

- (id)initWithDictionary:(NSDictionary *)dictionary;
- (NSDictionary *)dictionaryValue;
@end
