//
//  Achievement.h
//  FamousMe
//
//  Created by Shine Man on 12/22/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Achievement : NSObject

@property (nonatomic) NSString *type;
@property (nonatomic) NSString *name;

@property (nonatomic) double timeStamp;
@property (nonatomic) int value;


- (id)initWithDictionary:(NSDictionary *)dictionary;
- (NSDictionary *)dictionaryValue;
@end
