//
//  Notification.h
//  FamousMe
//
//  Created by Shine Man on 12/13/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserProfile.h"

@interface Notification : NSObject

@property (nonatomic) NSString *key;
@property (nonatomic) NSString *userId;

@property (nonatomic) UserProfile *userProfile;

@property (nonatomic) double timestamp;
@property (nonatomic) int notificationType;
@property (nonatomic) BOOL isRead;

- (id)initWithDictionary:(NSDictionary *)dictionary;
- (NSDictionary *)dictionaryValue;
@end
