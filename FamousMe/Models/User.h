//
//  User.h
//  FamousMe
//
//  Created by Shine Man on 12/6/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserProfile.h"
#import "Social.h"
#import "DiscoveryRules.h"
#import "Status.h"
#import "Position.h"

@interface User : NSObject

@property (nonatomic) NSString *key;
@property (nonatomic) UserProfile *userProfile;

@property (nonatomic) int gender;
@property (nonatomic) NSString *logInSocialWith;
@property (nonatomic) NSString *countryCode;
@property (nonatomic) NSString *phoneNumber;
@property (nonatomic) NSString *dateOfBirth;
@property (nonatomic) NSString *nationality;
@property (nonatomic) NSString *emailAddress;
@property (nonatomic) NSString *interests;
//Service
@property (nonatomic) NSString *serviceTitle;
@property (nonatomic) NSString *serviceDetails;

//Position
@property (nonatomic) Position *position;

//Social
@property (nonatomic) Social *social;

//Upgrade
@property (nonatomic) BOOL isUpgraded;
@property (nonatomic) BOOL isAddedSocial;
//Discoveries rules.
@property (nonatomic) DiscoveryRules *discoveryRules;
@property (nonatomic) Status *status;
@property (nonatomic) NSString *token;

- (id)initWithDictionary:(NSDictionary *)dictionary;
- (NSDictionary *)dictionaryValue;
@end
