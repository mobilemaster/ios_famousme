//
//  Social.m
//  FamousMe
//
//  Created by Shine Man on 12/6/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "Social.h"

@implementation Social

- (id)init {
	self = [super init];
	if (self) {
		self.website  = @"";
		self.facebook = @"";
		self.snapchat = @"";
		self.linkedin = @"";
		self.twitter  = @"";
		self.instagram = @"";
		self.youtube  = @"";

	}
	
	return self;
}

- (id)initWithDictionary:(NSDictionary *)dictionary {
	self = [super init];
	if (self) {
		self.website  = [dictionary objectForKey:@"website"];
		self.facebook = [dictionary objectForKey:@"facebook"];
		self.snapchat = [dictionary objectForKey:@"snapchat"];
		self.linkedin = [dictionary objectForKey:@"linkedin"];
		self.twitter  = [dictionary objectForKey:@"twitter"];
		self.instagram = [dictionary objectForKey:@"instagram"];
		self.youtube  = [dictionary objectForKey:@"youtube"];
	}
	
	return self;
}

- (NSDictionary *)dictionaryValue {
	return @{ @"website"     : self.website,
			  @"facebook"     : self.facebook,
			  @"snapchat"     : self.snapchat,
			  @"linkedin"     : self.linkedin,
			  @"twitter"      : self.twitter,
			  @"instagram"    : self.instagram,
			  @"youtube"	  : self.youtube,
			  };
}

@end
