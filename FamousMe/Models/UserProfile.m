//
//  UserProfile.m
//  FamousMe
//
//  Created by Shine Man on 12/6/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "UserProfile.h"

@implementation UserProfile

- (id)initWithDictionary:(NSDictionary *)dictionary {
	self = [super init];
	if (self) {
		self.username  = [dictionary objectForKey:@"username"];
		self.photoUrl   = [dictionary objectForKey:@"photoUrl"];
        self.avatarUrl  = [dictionary objectForKey:@"avatarUrl"];
		self.profession    = [dictionary objectForKey:@"profession"];
		self.isValid       = [[dictionary objectForKey:@"isValid"] boolValue];
		self.deletedTimestamp  = [[dictionary objectForKey:@"deletedTimestamp"] doubleValue];
	}
	
	return self;
}

- (NSDictionary *)dictionaryValue {
	return @{ @"username"     : self.username,
			 @"photoUrl"      : self.photoUrl,
             @"avatarUrl"     : self.avatarUrl,
			 @"profession"    : self.profession,
			 @"isValid" :  [NSNumber numberWithBool:self.isValid],
			 @"deletedTimestamp"     : [NSNumber numberWithDouble:self.deletedTimestamp],
			 };
}

@end
