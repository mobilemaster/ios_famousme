//
//  NudgeMessage.h
//  FamousMe
//
//  Created by Shine Man on 12/25/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NudgeMessage : NSObject

@property (nonatomic) NSString *key;
@property (nonatomic) NSString *title;
@property (nonatomic) NSString *photoUrl;
@property (nonatomic) NSString *message;
@property (nonatomic) NSString *senderId;

- (id)initWithDictionary:(NSDictionary *)dictionary;
- (NSDictionary *)dictionaryValue;
@end
