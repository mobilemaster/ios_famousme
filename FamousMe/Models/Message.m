//
//  Message.m
//  FamousMe
//
//  Created by Shine Man on 12/13/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "Message.h"

@implementation Message

- (id)initWithDictionary:(NSDictionary *)dictionary {
	self = [super init];
	if (self) {
		self.messageId   = [dictionary objectForKey:@"messageId"];
		self.senderId  = [dictionary objectForKey:@"senderId"];
		self.receiverId  = [dictionary objectForKey:@"receiverId"];
		self.sentTimestamp  = [[dictionary objectForKey:@"sentTimestamp"] doubleValue];
		self.text   = [dictionary objectForKey:@"text"];
		self.isRead       = [[dictionary objectForKey:@"isRead"] boolValue];
		self.readTimestamp  = [[dictionary objectForKey:@"readTimestamp"] doubleValue];
		self.isFirstOfDay       = [[dictionary objectForKey:@"isFirstOfDay"] boolValue];
		self.isNotified       = [[dictionary objectForKey:@"isNotified"] boolValue];
        if ([dictionary objectForKey:@"imageUrl"]) {
            self.imageUrl = [dictionary objectForKey:@"imageUrl"];
        }
        
        if ([dictionary objectForKey:@"messageType"]) {
            self.messageType = [[dictionary objectForKey:@"messageType"] integerValue];
        } else
            self.messageType = Text;
	}
	
	return self;
}

- (NSDictionary *)dictionaryValue {
	return @{ @"messageId"      : self.messageId,
			  @"senderId"     : self.senderId,
			  @"receiverId"     : self.receiverId,
			  @"sentTimestamp"     : [NSNumber numberWithDouble:self.sentTimestamp],
			  @"text"     : self.text,
			  @"isRead" :  [NSNumber numberWithBool:self.isRead],
			  @"readTimestamp"     : [NSNumber numberWithDouble:self.readTimestamp],
			  @"isFirstOfDay" :  [NSNumber numberWithBool:self.isFirstOfDay],
			  @"isNotified" :  [NSNumber numberWithBool:self.isNotified],
              @"imageUrl"     : self.imageUrl,
              @"messageType" :  [NSNumber numberWithInteger:self.messageType],
			  };
}

- (void)setData:(Message*)newData {
	self.messageId = newData.messageId;
	self.senderId = newData.senderId;
	self.receiverId = newData.receiverId;
	self.sentTimestamp = newData.sentTimestamp;
	self.text = newData.text;
	self.isRead = newData.isRead;
	self.isNotified = newData.isNotified;
	self.readTimestamp = newData.readTimestamp;
    self.imageUrl = newData.imageUrl;
    self.messageType = newData.messageType;
}
@end
