//
//  MapLocationItem.m
//  FamousMe
//
//  Created by Shine Man on 12/15/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "MapLocationItem.h"

@implementation MapLocationItem

- (void)setData:(User*)user userLocation:(CLLocation*)userLocation
					currentLocation:(CLLocation*)currentLocation {
	self.user = user;
	self.location = userLocation;
	
	self.distance = [userLocation distanceFromLocation:currentLocation];
}
@end
