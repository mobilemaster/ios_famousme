//
//  Contact.m
//  FamousMe
//
//  Created by Shine Man on 12/18/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "Contact.h"

@implementation Contact

- (id)initWithDictionary:(NSDictionary *)dictionary {
	self = [super init];
	if (self) {
		self.userId   = [dictionary objectForKey:@"userId"];
		self.contactKey  = [dictionary objectForKey:@"contactKey"];
	}
	
	return self;
}

- (NSDictionary *)dictionaryValue {
	return @{ @"userId"      : self.userId,
			  @"contactKey"     : self.contactKey,
			  };
}

@end
