//
//  MapLocationItem.h
//  FamousMe
//
//  Created by Shine Man on 12/15/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Position : NSObject
@property (nonatomic) double latitude;
@property (nonatomic) double longitude;

- (id)initWithDictionary:(NSDictionary *)dictionary;
- (NSDictionary *)dictionaryValue;
- (void)setData:(double)latitude longitude:(double)longitude;
@end
