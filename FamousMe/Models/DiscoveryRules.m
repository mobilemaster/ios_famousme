//
//  DiscoveryRules.m
//  FamousMe
//
//  Created by Shine Man on 12/6/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "DiscoveryRules.h"

@implementation DiscoveryRules

- (id)init {
	self = [super init];
	if (self) {
		self.isAnonymousMode = NO;
		self.discoverGender = 0;
		self.ageRange = 0;
        self.discoveryRange = 0;
        self.countryFilter = @"";
        self.discoveryMode = 0;
        self.onlineFilter = 0;
        self.serviceFilter = 0;        
		self.discoverNationality = @"All";
		self.discoveryInterest = @"All";
		self.discoverableMap = YES;
		self.notifications = YES;
        self.pushNotification = YES;
		self.showPhoneNumber = NO;
		self.canSeeMyAge = NO;
	}
	
	return self;
}

- (id)initWithDictionary:(NSDictionary *)dictionary {
	self = [super init];
	if (self) {
		self.isAnonymousMode = [[dictionary objectForKey:@"isAnonymousMode"] boolValue];
		self.discoverGender = [[dictionary objectForKey:@"discoverGender"] intValue];
		self.ageRange = [[dictionary objectForKey:@"ageRange"] intValue];
        if ([dictionary objectForKey:@"discoveryMode"])
            self.discoveryMode = [[dictionary objectForKey:@"discoveryMode"] intValue];
        else
            self.discoveryMode = 0;

        if ([dictionary objectForKey:@"countryFilter"])
            self.countryFilter = [dictionary objectForKey:@"countryFilter"];
        
        if ([dictionary objectForKey:@"discoveryRange"])
            self.discoveryRange = [[dictionary objectForKey:@"discoveryRange"] intValue];
        else
            self.discoveryRange = 0;
        
        if ([dictionary objectForKey:@"onlineFilter"])
            self.onlineFilter = [[dictionary objectForKey:@"onlineFilter"] intValue];
        else
            self.onlineFilter = 0;
        
        if ([dictionary objectForKey:@"serviceFilter"])
            self.serviceFilter = [[dictionary objectForKey:@"serviceFilter"] intValue];
        else
            self.serviceFilter = 0;
        
		self.discoverNationality = [dictionary objectForKey:@"discoverNationality"];
		self.discoveryInterest = [dictionary objectForKey:@"discoveryInterest"];
		self.discoverableMap = [[dictionary objectForKey:@"discoverableMap"] boolValue];
		self.notifications = [[dictionary objectForKey:@"notifications"] boolValue];
        if ([dictionary objectForKey:@"pushNotification"])
            self.pushNotification = [[dictionary objectForKey:@"pushNotification"] boolValue];
        else
            self.pushNotification = YES;
            
		self.showPhoneNumber = [[dictionary objectForKey:@"showPhoneNumber"] boolValue];
		self.canSeeMyAge = [[dictionary objectForKey:@"canSeeMyAge"] boolValue];
	}
	
	return self;
}

- (NSDictionary *)dictionaryValue {
	return @{ @"isAnonymousMode" :  [NSNumber numberWithBool:self.isAnonymousMode],
			  @"discoverGender"  : [NSNumber numberWithInt:self.discoverGender],
			  @"ageRange"     : [NSNumber numberWithInt:self.ageRange],
              @"discoveryMode"     : [NSNumber numberWithInt:self.discoveryMode],
              @"countryFilter"     : self.countryFilter,
              @"discoveryRange"     : [NSNumber numberWithInt:self.discoveryRange],
              @"onlineFilter"     : [NSNumber numberWithInt:self.onlineFilter],
              @"serviceFilter"     : [NSNumber numberWithInt:self.serviceFilter],
			  @"discoverNationality": self.discoverNationality,
			  @"discoveryInterest": self.discoveryInterest,
			  @"discoverableMap" :  [NSNumber numberWithBool:self.discoverableMap],
              @"pushNotification":  [NSNumber numberWithBool:self.pushNotification],
			  @"notifications"	 :  [NSNumber numberWithBool:self.notifications],
			  @"showPhoneNumber" :  [NSNumber numberWithBool:self.showPhoneNumber],
			  @"canSeeMyAge"	 :  [NSNumber numberWithBool:self.canSeeMyAge]
			  };
}

@end
