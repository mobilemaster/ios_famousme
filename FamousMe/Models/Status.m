//
//  Status.m
//  FamousMe
//
//  Created by Shine Man on 12/6/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "Status.h"

@implementation Status

- (id)initWithDictionary:(NSDictionary *)dictionary {
	self = [super init];
	if (self) {
		self.isOnline   = [[dictionary objectForKey:@"isOnline"] boolValue];
		self.timestamp  = [[dictionary objectForKey:@"timestamp"] doubleValue];
	}
	
	return self;
}

- (NSDictionary *)dictionaryValue {
	return @{ @"isOnline" :  [NSNumber numberWithBool:self.isOnline],
			  @"timestamp"     : [NSNumber numberWithDouble:self.timestamp],
			  };
}

@end
