//
//  User.m
//  FamousMe
//
//  Created by Shine Man on 12/6/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "User.h"

@implementation User

- (id)init {
	self = [super init];
	if (self) {
		self.userProfile = [[UserProfile alloc] init];
		self.social = [[Social alloc] init];
		self.status = [[Status alloc] init];
	}
	
	return self;
}

- (id)initWithDictionary:(NSDictionary *)dictionary {
	self = [super init];
	if (self) {
		//self.key = [dictionary objectForKey:@"key"];
		self.userProfile  = [[UserProfile alloc] initWithDictionary:[dictionary objectForKey:@"userProfile"]];
		self.gender  = [[dictionary objectForKey:@"gender"] intValue];
		self.countryCode = [dictionary objectForKey:@"countryCode"];
		self.phoneNumber = [dictionary objectForKey:@"phoneNumber"];
		self.dateOfBirth = [dictionary objectForKey:@"dateOfBirth"];
		self.nationality = [dictionary objectForKey:@"nationality"];
		self.emailAddress = [dictionary objectForKey:@"emailAddress"];
		self.interests    = [dictionary objectForKey:@"interests"];
		self.serviceTitle = [dictionary objectForKey:@"serviceTitle"];
		self.serviceDetails = [dictionary objectForKey:@"serviceDetails"];
		self.social = [[Social alloc] initWithDictionary:[dictionary objectForKey:@"social"]];
        if ([dictionary objectForKey:@"position"]) {
            self.position = [[Position alloc] initWithDictionary:[dictionary objectForKey:@"position"]];
        }
        
        if ([dictionary objectForKey:@"logInSocialWith"]) {
            self.logInSocialWith = [dictionary objectForKey:@"logInSocialWith"];
        }
        
		self.isUpgraded = [[dictionary objectForKey:@"isUpgraded"] boolValue];
		self.isAddedSocial  = [[dictionary objectForKey:@"isAddedSocial"] boolValue];
		self.discoveryRules = [[DiscoveryRules alloc] initWithDictionary:[dictionary objectForKey:@"discoveryRules"]];
		self.status = [[Status alloc] initWithDictionary:[dictionary objectForKey:@"status"]];
		self.token = [dictionary objectForKey:@"token"];
	}
	
	return self;
}

- (NSDictionary *)dictionaryValue {
	return @{ //@"key"     : self.key,
			  @"userProfile" : [self.userProfile dictionaryValue],
			  @"gender"      :  [NSNumber numberWithInt:self.gender],
              @"logInSocialWith" : self.logInSocialWith == nil? @"": self.logInSocialWith,
			  @"countryCode" : self.countryCode,
			  @"phoneNumber" : self.phoneNumber,
			  @"dateOfBirth" : self.dateOfBirth,
			  @"nationality" : self.nationality,
			  @"emailAddress": self.emailAddress,
			  @"interests"     : self.interests,
			  @"serviceTitle"  : self.serviceTitle,
			  @"serviceDetails"  : self.serviceDetails,
			  @"social" 		 : [self.social dictionaryValue],
			  @"isUpgraded"	     : [NSNumber numberWithBool:self.isUpgraded],
			  @"isAddedSocial"   : [NSNumber numberWithBool:self.isAddedSocial],
			  @"discoveryRules"  : [self.discoveryRules dictionaryValue],
			  //@"status" 		 : [self.status dictionaryValue],
			  //@"token"			 : self.token,
			  };
}

@end
