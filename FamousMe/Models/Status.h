//
//  Status.h
//  FamousMe
//
//  Created by Shine Man on 12/6/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Status : NSObject

@property (nonatomic) BOOL isOnline;
@property (nonatomic) double timestamp;

- (id)initWithDictionary:(NSDictionary *)dictionary;
- (NSDictionary *)dictionaryValue;

@end
