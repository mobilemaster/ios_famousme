//
//  UserProfile.h
//  FamousMe
//
//  Created by Shine Man on 12/6/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserProfile : NSObject

@property (nonatomic) NSString *username;
@property (nonatomic) NSString *photoUrl;
@property (nonatomic) NSString *avatarUrl;
@property (nonatomic) NSString *profession;

@property (nonatomic) BOOL isValid;
@property (nonatomic) double deletedTimestamp;


- (id)initWithDictionary:(NSDictionary *)dictionary;
- (NSDictionary *)dictionaryValue;

@end
