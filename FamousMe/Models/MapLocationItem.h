//
//  MapLocationItem.h
//  FamousMe
//
//  Created by Shine Man on 12/15/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

@interface MapLocationItem : NSObject
@property (nonatomic) User *user;
@property (nonatomic) CLLocation* location;
@property (nonatomic) CLLocationDistance distance;

- (void)setData:(User*)user userLocation:(CLLocation*)userLocation currentLocation:(CLLocation*)currentLocation;
@end
