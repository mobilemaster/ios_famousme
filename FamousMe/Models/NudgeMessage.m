//
//  NudgeMessage.m
//  FamousMe
//
//  Created by Shine Man on 12/25/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "NudgeMessage.h"

@implementation NudgeMessage

- (id)initWithDictionary:(NSDictionary *)dictionary {
	self = [super init];
	if (self) {
		self.key   = [dictionary objectForKey:@"key"];
		self.title  = [dictionary objectForKey:@"title"];
		self.photoUrl  = [dictionary objectForKey:@"photoUrl"];
		self.message  = [dictionary objectForKey:@"message"];
		self.senderId  = [dictionary objectForKey:@"senderId"];
	}
	
	return self;
}

- (NSDictionary *)dictionaryValue {
	return @{ @"key"      : self.key,
			  @"title"     : self.title,
			  @"photoUrl"    : self.photoUrl,
			  @"message"     : self.message,
			  @"senderId"    : self.senderId,
			  };
}
@end
