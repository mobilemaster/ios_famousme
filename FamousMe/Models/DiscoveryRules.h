//
//  DiscoveryRules.h
//  FamousMe
//
//  Created by Shine Man on 12/6/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DiscoveryRules : NSObject
@property (nonatomic) BOOL isAnonymousMode;
@property (nonatomic) int discoverGender;
@property (nonatomic) int ageRange;
@property (nonatomic) int discoveryMode; //Range or Country
@property (nonatomic) NSString *countryFilter;
@property (nonatomic) int onlineFilter;
@property (nonatomic) int serviceFilter;

@property (nonatomic) int discoveryRange;
@property (nonatomic) NSString *discoverNationality;
@property (nonatomic) NSString *discoveryInterest;

@property (nonatomic) BOOL discoverableMap;
@property (nonatomic) BOOL pushNotification;
@property (nonatomic) BOOL notifications;
@property (nonatomic) BOOL showPhoneNumber;
@property (nonatomic) BOOL canSeeMyAge;

- (id)initWithDictionary:(NSDictionary *)dictionary;
- (NSDictionary *)dictionaryValue;
@end
