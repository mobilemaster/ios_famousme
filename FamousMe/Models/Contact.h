//
//  Contact.h
//  FamousMe
//
//  Created by Shine Man on 12/18/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Contact : NSObject

@property (nonatomic) NSString *contactKey;
@property (nonatomic) NSString *userId;

@property (nonatomic) UserProfile *userProfile;

- (id)initWithDictionary:(NSDictionary *)dictionary;
- (NSDictionary *)dictionaryValue;

@end
