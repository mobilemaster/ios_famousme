//
//  Notification.m
//  FamousMe
//
//  Created by Shine Man on 12/13/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "Notification.h"

@implementation Notification
- (id)initWithDictionary:(NSDictionary *)dictionary {
	self = [super init];
	if (self) {
		self.userId   = [dictionary objectForKey:@"userId"];
		self.timestamp  = [[dictionary objectForKey:@"timestamp"] doubleValue];
		self.notificationType = [[dictionary objectForKey:@"notificationType"] intValue];
		self.isRead       = [[dictionary objectForKey:@"isRead"] boolValue];
	}
	
	return self;
}

- (NSDictionary *)dictionaryValue {
	return @{ //@"key"     : self.key,
			  @"userId"      : self.userId,
			  @"timestamp"     : [NSNumber numberWithDouble:self.timestamp],
			  @"notificationType"   : [NSNumber numberWithInt:self.notificationType],
			  @"isRead" :  [NSNumber numberWithBool:self.isRead],
			  };
}

@end
