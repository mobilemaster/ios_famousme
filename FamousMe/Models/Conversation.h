//
//  Conversation.h
//  FamousMe
//
//  Created by Shine Man on 12/13/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BlockUser.h"

@interface Conversation : NSObject

@property (nonatomic) NSString *userId;
@property (nonatomic) NSString *conversationId;
@property (nonatomic) double timestamp;
@property (nonatomic) NSString *lastMessage;
@property (nonatomic) BlockUser *blockUser;
@property (nonatomic) BOOL isRead;

@property (nonatomic) UserProfile *userProfile;

- (id)init;
- (id)initWithDictionary:(NSDictionary *)dictionary;
- (NSDictionary *)dictionaryValue;
@end
