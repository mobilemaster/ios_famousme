//
//  Message.h
//  FamousMe
//
//  Created by Shine Man on 12/13/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Message : NSObject

@property (nonatomic) NSString *messageId;
@property (nonatomic) NSString *senderId;
@property (nonatomic) NSString *receiverId;
@property (nonatomic) double sentTimestamp;
@property (nonatomic) NSString *text;
@property (nonatomic) BOOL isRead;
@property (nonatomic) double readTimestamp;
@property (nonatomic) BOOL isFirstOfDay;
@property (nonatomic) BOOL isNotified;
@property (nonatomic) NSString *imageUrl;
@property (nonatomic) MessageType messageType;


- (id)initWithDictionary:(NSDictionary *)dictionary;
- (NSDictionary *)dictionaryValue;
- (void)setData:(Message*)newData;
@end
