//
//  Achievement.m
//  FamousMe
//
//  Created by Shine Man on 12/22/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "Achievement.h"

@implementation Achievement
- (id)initWithDictionary:(NSDictionary *)dictionary {
	self = [super init];
	if (self) {
		self.type  = [dictionary objectForKey:@"type"];
		self.name  = [dictionary objectForKey:@"name"];
		self.timeStamp  = [[dictionary objectForKey:@"timestamp"] doubleValue];
		self.value  = [[dictionary objectForKey:@"value"] intValue];
	}
	
	return self;
}

- (NSDictionary *)dictionaryValue {
	return @{ @"type"     : self.type,
			  @"name"      : self.name,
			  @"timestamp"    : [NSNumber numberWithDouble:self.timeStamp],
			  @"value" :  [NSNumber numberWithInt:self.value],			  
			  };
}

@end
