//
//  Conversation.m
//  FamousMe
//
//  Created by Shine Man on 12/13/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "Conversation.h"

@implementation Conversation

- (id)init{
	self = [super init];
	if (self) {
		self.timestamp = 0;
		self.blockUser = [[BlockUser alloc] init];
		self.isRead = NO;
	}
	
	return self;
}

- (id)initWithDictionary:(NSDictionary *)dictionary {
	self = [super init];
	if (self) {
		self.userId   = [dictionary objectForKey:@"userId"];
		self.conversationId  = [dictionary objectForKey:@"conversationId"];
		self.timestamp  = [[dictionary objectForKey:@"timestamp"] doubleValue];
		self.lastMessage   = [dictionary objectForKey:@"lastMessage"];
		self.blockUser    = [[BlockUser alloc] initWithDictionary:[dictionary objectForKey:@"blockUser"]];
		self.isRead       = [[dictionary objectForKey:@"isRead"] boolValue];
	}
	
	return self;
}

- (NSDictionary *)dictionaryValue {
	return @{ @"userId"      : self.userId,
			  @"conversationId"     : self.conversationId,
			  @"timestamp"     : [NSNumber numberWithDouble:self.timestamp],
			  @"lastMessage"     : self.lastMessage,
			  @"blockUser" : [self.blockUser dictionaryValue],
			  @"isRead" :  [NSNumber numberWithBool:self.isRead],
			  };
}


@end
