//
//  MapLocationItem.m
//  FamousMe
//
//  Created by Shine Man on 12/15/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import "Position.h"

@implementation Position

- (id)initWithDictionary:(NSDictionary *)dictionary {
    self = [super init];
    if (self) {
        self.latitude   = [[dictionary objectForKey:@"latitude"] doubleValue];
        self.longitude  = [[dictionary objectForKey:@"longitude"] doubleValue];
    }
    
    return self;
}

- (NSDictionary *)dictionaryValue {
    return @{ @"latitude" :  [NSNumber numberWithDouble:self.latitude],
              @"longitude"     : [NSNumber numberWithDouble:self.longitude],
              };
}

- (void)setData:(double)latitude longitude:(double)longitude {
	self.latitude = latitude;
	self.longitude = longitude;
}
@end
