//
//  AppDelegate.h
//  FamousMe
//
//  Created by Shine Man on 11/14/17.
//  Copyright © 2017 Shine Man. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface AppDelegate : UIResponder <UIApplicationDelegate, FIRMessagingDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

